<script type="text/javascript">
    var base_dir = "{$base_dir_ssl}";
    var id_product = {$id_product};
    var request_sellerquestion_uri = "{$request_sellerquestion_uri}";
    var request_sellerquestion_p = {$request_sellerquestion_p};
    var request_sellerquestion_n = {$request_sellerquestion_n};
    var ispostbacksellerquestion = {if $ispostbacksellerquestion}true{else}false{/if};
</script>

<script type="text/javascript">
    
    var agilesellerquestionsform_open = {if isset($post_errors) && count($post_errors)>0}1{else}0{/if};
	//tootgle  seller questions form
	
	function toogleSellerQuestionForm()
	{ldelim}
	    if(agilesellerquestionsform_open == 1)
    	{ldelim}
    		alert ("1");
    		$('#sendSellerQuestion').slideUp('fast');
	    	$('input#addSellerQuestionButton').fadeIn('slow');
	    	agilesellerquestionsform_open = 0;
    	{rdelim}
	    else
    	{ldelim}
    		
    		$('#sendSellerQuestion').slideDown('fast');
	    	$('input#addSellerQuestionButton').fadeOut('slow');
	    	agilesellerquestionsform_open = 1;
    	{rdelim}
	{rdelim}
	
	
    function validateSellerQuestionForm() {ldelim}
        if (trim($('input#sellercustomer').val()).length < 1) {ldelim}
            alert("{l s='please input your name' mod='agilesellermessenger'}");
            return false;
        {rdelim}
        if (trim($('textarea#yourquestion').val()).length < 1) {ldelim}
            alert("{l s='Please enter your question or comment' mod='agilesellermessenger'}");
            return false;
        {rdelim}

        return ret;
    {rdelim}
	
	$(document).ready(function(){
		
		$('#sendSellerQuestion').slideDown('fast');
		$('input#addSellerQuestionButton').fadeOut('slow');
		agilesellerquestionsform_open = 1;
	
	});
	
	
	
</script>

<div id="cuadroContacto">
            {if isset($post_errors) && $post_errors}
	            <div class="error">
		            <p>
		            	{if $post_errors|@count > 1}
		            		{l s='There are' mod='agilesellermessenger'}
		            	{else}
		            		{l s='There is' mod='agilesellermessenger'}
		            	{/if} 
		            	
		            	{$post_errors|@count} 
		            	
		            	{if $post_errors|@count > 1}
		            		{l s='errors' mod='agilesellermessenger'}
		            	{else}
		            		{l s='error' mod='agilesellermessenger'}
		            	{/if} 
		            	:</p>
		            <ol>
		            {foreach from=$post_errors key=k item=error}
			            <li>{$error}</li>
		            {/foreach}
		            </ol>
	            </div>
            {/if}
            
            {if $postSellerQuestionSuccess}
    			<div style="color:green;border:solid 1px green;padding:10px 10px 10px 10px;background-color:#e0e0e0;">
    			    <b><font color="green">{$postSellerQuestionSuccess}</font></b>
                </div>
            {/if}
             
            
    {if $logged == true OR !$login_required_sellerquestion}
    
    <!--<a id="addSellerQuestionButton" style="cursor:pointer;" onclick="javascript:toogleSellerQuestionForm()">{*l s='Click here to open or close the "Ask Seller A Question" form.' mod='agilesellermessenger'*}</a>--><br /><br />
    
    <form style="display:{if isset($post_errors) && count($post_errors)>0}{else}none{/if};" action="{$request_sellerquestion_uri}&p={$request_sellerquestion_p}&n={$request_sellerquestion_n}&ispostbacksellerquestion=1" method="post" onsubmit="return validateSellerQuestionForm()" class="std" enctype="multipart/form-data" id="sendSellerQuestion">
		
		<input type="hidden" name="id_seller" value="{$sellerinfo->id_seller}">
	    
	    <fieldset>
	    	
	    	<h7>Contacta con nosotros</h7>
	    	
	    	<div id="columnaContactoIzquierda">
	    		
	    		<p id="tituloContacto">Tus datos de contacto</p>
	    	
	    		<hr>
	    	
	    		<!--
	    		<div id="imagenCorreo">
	    		
	    			<img src="{*$base_dir*}img/correo.png">
	    			
	    		</div>
	    		-->
	    		
	    		<div id="datosColumnaIzquierda">
	    		
	    			<p>Nombre:</p>
	    			<p>Email:</p>		
	    			<p>Verificaci&oacute;n de texto&nbsp;<img src="{$base_dir_ssl}modules/agilesellermessenger/antispamimage.php" /></p>
	    			<p>Tu pregunta:</p>
	    			
	    		</div>
	    		
	    		<div id="datosColumnaDerecha">
	    		
	    			<p><input type="text" name="customer_name" id="customer_name" value="{$customer_name}" /></p>	
	    			<p><input type="text" name="customer_email" size="80" id="customer_email" value="{$customer_email}" /></p>
	    			<p><input type="text" name="anti_spam_code_sellermessage" value="" /></p>
	    			<p><textarea cols="80" rows="5" name="customer_message" id="customer_message">{if isset($post_errors) && $post_errors}{$post_message|escape:'html':'UTF-8'}{/if}</textarea></p>
	    		
	    		</div>
	    		
	    		<p class="submit">
			    	<input class="buscarAvanzadaButton3" name="submitSellerQuestion" value="Enviar" type="submit" />
		    	</p>
	    		
					    		
	    	</div>
	    	
	    	<div id="liniaSeparadoraVertial"></div>
	    		
	    		<div id="datosEmpresa2">
			
					{foreach from=$empresa item=itemempresa}
				
						<a href="{$base_dir}index.php?controller=listadoclientes&id_cliente={$itemempresa.id_seller}">
						
							<img src="{$base_dir}img/as/{$itemempresa.id_seller}.jpg" />
						
						</a>
						
						
						<img id="maletin" src="{$base_dir}img/maletin.png">
						
						<p id="nombreEmpresa1"><a href="{$base_dir}index.php?controller=listadoclientes&id_cliente={$itemempresa.id_seller}">{$itemempresa.company}</a></p>
						<p id="vendedor1">Vendedor de {$categoria}</p>
				
						
								
					<hr id="hr2">
					
					<p id="nProductos1">El n&uacute;mero total de productos son:
					
						{foreach from=$totalProductos item=itemtotalProductos}
						
							{$itemtotalProductos.nProductos}
						
						{/foreach}
					
					</p>
				
					<p id="todosProductos1"><a href="{$base_dir}index.php?controller=listadoclientes&id_cliente={$itemempresa.id_seller}">Ver todos los productos</a></p>
					
					{/foreach}
						
				</div>	
	    	
	    	
	    	
	    	{*$customer_name*}
		        
	    </fieldset>
    </form>
    {else}
	    <p class="align_center">{l s='Only registered users can post a new question.' mod='agilesellermessenger'}</p>
    {/if}	    
    {if $sellerquestions AND count($sellerquestions)>0}
        {l s='Questions Asked' mod='agilesellermessenger'}(total:{$sellerquestions_nb})
        {include file="$tpl_dir./pagination.tpl"}   
	    <div class="clear table_block">
	    {foreach from=$sellerquestions item=sellerquestion}
		    {if $sellerquestion.message}
              <div itemscope itemtype="http://data-vocabulary.org/Review" style="margin:10px;">
                <span>{$sellerquestion.from_name|truncate:20:'...':true|escape:'html':'UTF-8'}</span>
                <span>{dateFormat date=$sellerquestion.date_add|escape:'html':'UTF-8' full=0}</span><br />
                <br />
                <span>{$sellerquestion.message|truncate:380:'...':true|escape:'html':'UTF-8'|nl2br}</span>
                {if $sellerquestion.attshname1}
                 <a href="{$base_dir_ssl}modules/agilesellermessenger/get-att.php?&filename={$sellerquestion.attpsname1}&id_seller={$sellerquestion.id_seller}">{$sellerquestion.attshname1|truncate:20:'...':true|escape:'html':'UTF-8'}</a>&nbsp&nbsp
                {/if}
                {if $sellerquestion.attshname2}
                  <a href="{$base_dir_ssl}modules/agilesellermessenger/get-att.php?&filename={$sellerquestion.attpsname2}&id_seller={$sellerquestion.id_seller}">{$sellerquestion.attshname2|truncate:20:'...':true|escape:'html':'UTF-8'}</a>&nbsp&nbsp
                {/if}
                {if $sellerquestion.attshname3}
                  <a href="{$base_dir_ssl}modules/agilesellermessenger/get-att.php?&filename={$sellerquestion.attpsname3}&id_seller={$sellerquestion.id_seller}">{$sellerquestion.attshname3|truncate:20:'...':true|escape:'html':'UTF-8'}</a>
                {/if}
              </div>
             <hr style="border:dotted 1px gray" />
		    {/if}
	    {/foreach}
	    </div>
    {else}
	    
    {/if}

</div>
