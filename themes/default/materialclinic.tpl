{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


<script>

	$(document).ready(function(){
		
		$("#center_column").css("width","77%");
		$("#center_column").css("marginRight","0px");
		$("#center_column").css("height","1050px");
		$("#center_column").css('border','1px solid #82bc00');
		$("#center_column").css('backgroundColor','#ebf4d6');
	
	});

</script>

{capture name=path}Dosa{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>Quienes somos</h1>

<div id="contenedorReciclinic">
	
	<h3>Distribuciones Dosa Automoci&oacute;n, S.L., es una empresa de nueva creaci&oacute;n, fundada por profesionales con una larga experiencia en el sector del recambio del autom&oacute;vil.</h3>
	
	<p>Nuestros clientes son los principales <span id="business">recambistas espa&ntilde;oles.</span></p>
	
	<p>Nuestra filosof&iacute;a se divide en dos simples puntos:</p>
	
	<ol>
	
		<li>Voluntad de dar el mejor servicio al cliente.</li>
		<li>Constante mejora de nuestra gesti&oacute;n para ofrecer los precios m&aacute;s competitivos.</li>
	
	</ol>
	
	
	
	<div id="left">
		<div id="map"></div>
	</div>
	
	<div id="right">
		
		<h3>DOSA AUTOMOCI&Oacute;N, S.L.</h3>
		<p>Avda. Mare de D&eacute;u de N&uacute;ria, 23B , Pol. Ind. Can Calder&oacute;n</p> 
		<p>08830 Sant Boi de Llobregat, Barcelona, Espa&ntilde;a</p> 
		<p>CIF B66276460</p>
		<!--<p>&nbsp;</p>-->
		<p>Tel. +34 936.309.223 / +34 936.400.129</p>
		<p>Fax. +34 936 409 964</p> 
		<!--<p>&nbsp;</p>-->
		<p>email:&nbsp;<a href="mailto:info@dosa.com">info@dosa.com</a></p>
		<p>website:&nbsp;<a href="http://www.2automocion.com">http://www.2automocion.com</a></p>
		<!--<p>&nbsp;</p>-->
		<!--
		<p>Inscrita en el Registro Mercantil de Barcelona, T. 44266, F.451448, S. 8, Fecha de inscripción  2014-04-14, fechaPublicacion 2014-04-22 
		<p>213515. Fecha inscripci&oacute;n 18/04/2000.</p>
		-->
		
		
		
	</div>
	
	<br id="clear">
	
	
	
	
</div>

<script type="text/javascript">
		
		var roadStyle = {
			strokeColor: "#FFFF00",
			strokeWeight: 7,
			strokeOpacity: 0.75
		};
		
		var addressStyle = {
			icon: "img/marker-house.png"
		};
		
		var parcelStyle = {
			strokeColor: "#FF7800",
			strokeOpacity: 1,
			strokeWeight: 2,
			fillColor: "#46461F",
			fillOpacity: 0.25
		};
		
		var map;
		currentFeature_or_Features = null;
		
		var geojson_Point = {
			"type": "Point",
			"coordinates": [41.361832, 2.103009]
		};
		
		var infowindow = new google.maps.InfoWindow();
		
		function init(){
			
			map = new google.maps.Map(document.getElementById('map'),{
				zoom: 17,
				center: new google.maps.LatLng(41.503761, 2.344225),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			
			
		}
		
		function clearMap(){
			
			if (!currentFeature_or_Features)
				return;
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(null);
						}
					}
					else{
						currentFeature_or_Features[i].setMap(null);
					}
				}
			}else{
				currentFeature_or_Features.setMap(null);
			}
			if (infowindow.getMap()){
				infowindow.close();
			}
		}
		
		function showFeature(geojson, style){
			
			clearMap();
			
			currentFeature_or_Features = new GeoJSON(geojson, style || null);
			
			if (currentFeature_or_Features.type && currentFeature_or_Features.type == "Error"){
				document.getElementById("put_geojson_string_here").value = currentFeature_or_Features.message;
				return;
			}
			
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(map);
							if(currentFeature_or_Features[i][j].geojsonProperties) {
								setInfoWindow(currentFeature_or_Features[i][j]);
							}
						}
					}
					else{
						currentFeature_or_Features[i].setMap(map);
					}
					if (currentFeature_or_Features[i].geojsonProperties) {
						setInfoWindow(currentFeature_or_Features[i]);
					}
				}
			}else{
				currentFeature_or_Features.setMap(map)
				if (currentFeature_or_Features.geojsonProperties) {
					setInfoWindow(currentFeature_or_Features);
				}
			}
			
			document.getElementById("put_geojson_string_here").value = JSON.stringify(geojson);
		}
		
		function rawGeoJSON(){
			clearMap();
			currentFeature_or_Features = new GeoJSON(JSON.parse(document.getElementById("put_geojson_string_here").value));
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(map);
							if(currentFeature_or_Features[i][j].geojsonProperties) {
								setInfoWindow(currentFeature_or_Features[i][j]);
							}
						}
					}
					else{
						currentFeature_or_Features[i].setMap(map);
					}
					if (currentFeature_or_Features[i].geojsonProperties) {
						setInfoWindow(currentFeature_or_Features[i]);
					}
				}
			}else{
				currentFeature_or_Features.setMap(map);
				if (currentFeature_or_Features.geojsonProperties) {
					setInfoWindow(currentFeature_or_Features);
				}
			}
		}
		
		function setInfoWindow (feature) {
			google.maps.event.addListener(feature, "click", function(event) {
				var content = "<div id='infoBox'><strong>GeoJSON Feature Properties</strong><br />";
				for (var j in this.geojsonProperties) {
					content += j + ": " + this.geojsonProperties[j] + "<br />";
				}
				content += "</div>";
				infowindow.setContent(content);
				infowindow.setPosition(event.latLng);
				infowindow.open(map);
			});
		}
		
		init();
		showFeature(geojson_Point);
		
</script>
