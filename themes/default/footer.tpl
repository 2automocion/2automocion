{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

		{if !$content_only}
				</div>

<!-- Right -->

				{foreach from=$paginasNoColumnas key=k item=pagina}
					
					{if $page_name == $k && $pagina == 2 || $page_name == $k && $pagina == 3}
				
						<div id="right_column" class="column grid_2 omega">
							{$HOOK_RIGHT_COLUMN}
						</div>
				
					{/if}
					
				{/foreach}

				
			</div>
			
			<!--
			<div id="reaseguro">
				{*$HOOK_REASEGUROS*}
			</div>
			-->
			
<!-- Footer -->
			
		</div>
		
		<div id="footer" class="grid_9 alpha omega clearfix">
				{*$HOOK_FOOTER*}
				<!--
				{*if $PS_ALLOW_MOBILE_DEVICE*}
					<p class="center clearBoth"><a href="{*$link->getPageLink('index', true)*}?mobile_theme_ok">{*l s='Browse the mobile site'*}</a></p>
				{*/if*}
				-->
		
				<div id="footerPrincipio"></div>


				<div id="footer2">
					
					<div id="contenidoFooter">
						{*$HOOK_FOOTER*}
						
						<div id="contenedorImagenFooter">
						
							<ul>
								
								<li><img id="logotipoFooter" class="logotipoFooterImg"></li>
								<li><h3>Dosa esta presente</h3></li>
								<li><h3>Siguenos en</h3></li>
							</ul>
							
						</div>
						
						<ul id="ayuda">
							<li><a href="{$base_dir}index.php?controller=dosa">Quienes Somos</a></li>
							<li><a href="{$base_dir}index.php?controller=contact">Contacta con nosotros</a></li>
						</ul>
					
						
					
						<ul id="comunidades1">
							
							
							<li>Andaluc&iacute;a</li>
							<li>Arag&oacute;n</li>
							<li>Cantabria</li>
							<li>Pais Basco</li>
							<li>Castilla y Le&oacute;n</li>
							<li>Castilla-La Mancha</li>
							<li>Catalu&ntildea</li>
							<li>Ceuta (Ciudad Aut&oacute;noma)</li>
							<li>Comunidad de Madrid</li>
							<li>Comunidad Valenciana</li>
							
								
						</ul>
						
						<ul id="comunidades2">
							<li>Extremadura</li>
							<li>Galicia</li>
							<li>Illes Balears</li>
							<li>Islas Canarias</li>
							<li>La Rioja</li>
							<li>Melilla (Ciudad Aut&oacute;noma)</li>
							<li>Navarra</li>
							<li>Pa&iacute;s Vasco</li>	
							<li>Principado de Asturias</li>	
							<li>Regi&oacute;n de Murcia</li>		
								
						</ul>
						
						
						
						<ul id="siguenos">
							
							<li><img src="{$base_dir}img/facebook.jpg"><a href="#">Facebook</a></li>
							
							<!--
							<li><img src="{$base_dir}img/googleplus.jpg"><a href="#">Google +</a></li>	
							<li><img src="{$base_dir}img/linkedin.jpg"><a href="#">Linkedin</a></li>
							-->
							
						</ul>
						
						<ul id="siguenos1">
							
							<li><img src="{$base_dir}img/twitter.jpg"><a href="#">Twitter</a></li>
							
							<!--
							<li><img src="{$base_dir}img/youtube.jpg"><a href="#">Youtube</a></li>	
							<li><img src="{$base_dir}img/blog.jpg"><a href="#">Blog</a></li>
							-->
							
						</ul>
					
					</div>
	
	
					<div id="footerFinal2"></div>
		
	
		
		
			</div>
	
	
		</div>
		
		</div>
		
		
	{/if}
	</body>
</html>
