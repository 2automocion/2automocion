{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{capture name=path}C&oacute;mo ser cliente{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<script>

$(document).ready(function(){
	
	$("#center_column").css('margin-right','10px');
	$("#center_column").css('border','1px solid #82bc00');
	$("#center_column").css('backgroundColor','#ebf4d6');
	$("#center_column").css('height','925px');
	$("#center_column").css('width','76%');
	
	
	
	
});

</script>

<div id="cuadroVender">

	<h1>C&oacute;mo ser cliente de distrubuciones DOSA, automoci&oacute;n S.L<span><br>El portal n&deg;1 en distrubuci&oacute;n de retrovisores para autm&oacute;vil </span></h1>

	<h3>&iquest;Eres fabricante, vendedor, distribuidor, taller&#63;<br><br>Te lo ponemos muy f&aacute;cil para pedir tus productos.<span>&nbsp;Ll&aacute;manos sin compromiso al +34 (93) 630 92 23  o al +34 (93) 640 01 29</span> o bien d&eacute;janos tus datos a trav&eacute;s de este formulario y nos pondr&eacute;mos en contacto contigo.</h3>

	<hr>

	<div>
		
		<div id="derechaVender">
			
			<p id="tituloDercha">D&eacute;janos tus datos y nos pondr&eacute;mos en contacto contigo</p>
			
			
		
			<form action="{$base_uri}?controller=contact" method="post" class="std" enctype="multipart/form-data">
		
		<fieldset>
			
			<p class="select">
				
				<select id="id_contact" name="id_contact" style="">
					<option value="2">Quiero contactar con Dosa</option>
				</select>
			
			</p>
			
			<p class="text">
				<label for="email">{l s='Email address'}</label>
				{if isset($customerThread.email)}
					<input type="text" id="email" name="from" value="{$customerThread.email|escape:'htmlall':'UTF-8'}" readonly="readonly" style="margin-left:80px!Important;margin-top:20px!Important;width:400px!Important;" />
				{else}
					<input type="text" id="email" name="from" value="{$email|escape:'htmlall':'UTF-8'}" style="margin-left:80px!Important;margin-top:20px!Important;width:400px!Important;"/>
				{/if}
			</p>
			
		{if !$PS_CATALOG_MODE}
		
			<input type="hidden" name="id_order" id="id_order" value="" readonly="readonly" />
			{if isset($isLogged) && $isLogged}
				<p class="text select">
					<label for="id_product">{l s='Product'}</label>
					{if !isset($customerThread.id_product)}
					{foreach from=$orderedProductList key=id_order item=products name=products}
						<select name="id_product" id="{$id_order}_order_products" class="product_select" style="width:300px;{if !$smarty.foreach.products.first} display:none; {/if}" {if !$smarty.foreach.products.first}disabled="disabled" {/if}>
							<option value="0">{l s='-- Choose --'}</option>
							{foreach from=$products item=product}
								<option value="{$product.value|intval}">{$product.label|escape:'htmlall':'UTF-8'}</option>
							{/foreach}
						</select>
					{/foreach}
					{elseif $customerThread.id_product > 0}
						<input type="text" name="id_product" id="id_product" value="{$customerThread.id_product|intval}" readonly="readonly" />
					{/if}
				</p>
			{/if}
		{/if}
		
		{if $fileupload == 1}
			<p class="text">
			<label for="fileUpload">{l s='Attach File'}</label>
				<input type="hidden" name="MAX_FILE_SIZE" value="2000000" />
				<input type="file" name="fileUpload" id="fileUpload" />
			</p>
		{/if}
		
		
		<p style="float:left;">Escribe tus datos:</p>
		<p style="float:left;">	
			<textarea id="message" name="message" rows="15" cols="10" style="float: left ! important; margin-left: 66px ! important; height: 80px ! important; width: 410px ! important;height:400px!important;text-align:left!Important;font-family:Verdana, Arial, Helvetica, sans-serif!Important;">Solicitud: Alta Cliente&#13;Nombre:&#13;Telf:&#13;email:&#13;</textarea>
		</p>
			
		<p class="submit">
			<!--<input type="submit" name="submitMessage" id="submitMessage" value="{*l s='Send'*}" class="button_large" />-->
			
			<input type="submit" name="submitMessage" id="submitMessage" value="Solicitar" />	
			
		</p>
		
	</fieldset>
	
</form>
		
		</div>
				
	</div>
		
</div>	
	