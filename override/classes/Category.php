<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.
class Category extends CategoryCore
{
	public static function updateFromShop($categories, $id_shop)
	{
				return true;
	}
	
	public static function getRootCategory($id_lang = null, Shop $shop = null)
	{
		if(!Module::isInstalled('agilemultipleseller'))return parent::getRootCategory($id_lang, $shop);
		$id_seller_home = AgileSellerManager::get_current_logged_seller_home_category_id();
		if((int)$id_seller_home <=0) 	return parent::getRootCategory($id_lang, $shop);
		if((int)$id_lang <=0)$id_lang = Context::getContext()->language->id;			
		$category = new Category($id_seller_home, $id_lang);
		if(!Validate::isLoadedObject($category))return parent::getRootCategory($id_lang, $shop);
		return $category;
	}
		
    	static public function getCategories($id_lang = false, $active = true, $order = true, $sql_filter = '', $sql_sort = '',$sql_limit = '')
	{
	    global $cookie;
	    
	    if(!Module::isInstalled('agilemultipleseller'))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit);	    if(intval($cookie->id_employee)==0)return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit); 	    if(intval($cookie->profile) != intval(Configuration::get('AGILE_MS_PROFILE_ID')))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit); 
	    
	 	if (!Validate::isBool($active))
	 		die(Tools::displayError());

		$sql = '
		SELECT *
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_owner` co ON c.`id_category` = co.`id_category`
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`
		WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)($id_lang) : '').'
		'.($active ? 'AND `active` = 1' : '');

		if(_PS_VERSION_ > '1.5')
			$sql .= (Shop::$id_shop_owner>0? ' AND s.id_seller='  . Shop::$id_shop_owner : '' );
			
		
		$sql .= (!$id_lang ? 'GROUP BY c.id_category' : '').'
		'.($sql_sort != '' ? $sql_sort : 'ORDER BY c.`level_depth` ASC, c.`position` ASC').'
		'.($sql_limit != '' ? $sql_limit : '');


		        
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

		if (!$order)
			return $result;

		$categories = array();
		foreach ($result AS $row)
			$categories[$row['id_parent']][$row['id_category']]['infos'] = $row;

		return $categories;
	}
		

	public function getProducts($id_lang, $p, $n, $orderBy = NULL, $orderWay = NULL, $getTotal = false, $active = true, $random = false, $randomNumberProducts = 1, $checkAccess = true, Context $context = null)
	{
		global $cookie;
		if (!$checkAccess OR !$this->checkAccess($cookie->id_customer))
			return false;
		
				if(Module::isInstalled('agilemembership') AND $this->id == Configuration::get('AGILE_MEMBERSHIP_CID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);

				if(Module::isInstalled('agileprepaidcredit') AND $this->id == Configuration::getGlobalValue('AGILE_PCREDIT_CID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);

				if(Module::isInstalled('agilesellerlistoptions') AND $this->id == Configuration::get('ASLO_CATEGORY_ID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);
		
		if(!$context)$context = Context::getContext();

		$agile_sql_parts = AgileSellerManager::getAdditionalSqlForProducts("p");
		if(empty($agile_sql_parts['selects']) AND  empty($agile_sql_parts['joins']) AND empty($agile_sql_parts['wheres']))
			return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);
		
	    if(Module::isInstalled('agilesellerlistoptions'))
		{
			require_once(_PS_ROOT_DIR_ . "/modules/agilesellerlistoptions/agilesellerlistoptions.php");
			if($this->id <= 1 OR ($this->id == 2 AND  version_compare(_PS_VERSION_, '1.5', ">=")))
				return AgileSellerListOptions::get_home_products($id_lang, $p, $n);	
			
			if(empty($orderBy) || $orderBy == 'position')$orderBy = 'position2';
		}
        		

		if ($p < 1) $p = 1;

		if (empty($orderBy))
			$orderBy = 'position';
		else
						$orderBy = strtolower($orderBy);

		if (empty($orderWay))
			$orderWay = 'ASC';
		if ($orderBy == 'id_product' OR	$orderBy == 'date_add')
			$orderByPrefix = 'p';
		elseif ($orderBy == 'name')
			$orderByPrefix = 'pl';
		elseif ($orderBy == 'manufacturer')
		{
			$orderByPrefix = 'm';
			$orderBy = 'name';
		}
		elseif ($orderBy == 'position')
			$orderByPrefix = 'cp';

		if ($orderBy == 'price')
			$orderBy = 'orderprice';

		if (!Validate::isBool($active) OR !Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay))
			die (Tools::displayError());


		$id_supplier = (int)(Tools::getValue('id_supplier'));

				if ($getTotal)
		{
			$sql = '
			SELECT COUNT(cp.`id_product`) AS total
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
			    '. $agile_sql_parts['joins']. '
			WHERE cp.`id_category` = '.(int)($this->id).($active ? ' AND p.`active` = 1' : '').'
		    '. $agile_sql_parts['wheres']. '
			'.($id_supplier ? 'AND p.id_supplier = '.(int)($id_supplier) : '');

 			
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
			return isset($result) ? $result['total'] : 0;
		}


		$sql = '
		SELECT p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new,
			(p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice
		    '. $agile_sql_parts['selects']. '
		FROM `'._DB_PREFIX_.'category_product` cp
		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
		                                           AND tr.`id_country` = '.(_PS_VERSION_ > '1.5' ?  (int)Context::getContext()->country->id : (int)Country::getDefaultCountryId()).'
	                                           	   AND tr.`id_state` = 0)
	    LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
		LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
	    '. $agile_sql_parts['joins']. '
		WHERE 1
		    AND cp.`id_category` = '.(int)($this->id).($active ? ' AND p.`active` = 1' : '').'
		    '. $agile_sql_parts['wheres']. '
    		'.($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');


		if ($random === true)
		{
			$sql .= ' ORDER BY RAND()';
			$sql .= ' LIMIT 0, '.(int)($randomNumberProducts);
		}
		else
		{
			$sql .= ' ORDER BY '.(isset($orderByPrefix) ? $orderByPrefix.'.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).'
			LIMIT '.(((int)($p) - 1) * (int)($n)).','.(int)($n);

											}


		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

		if (!$result)
			return false;

						$seller = array();
		$id_seller = array();
		$link_seller = array();
		foreach($result as $row)
		{
    		$pid = $row['id_product'];
			$seller[$pid] = isset($row['seller'])?$row['seller'] : '';
			$id_seller[$pid]= isset($row['id_seller'])? intval($row['id_seller']) : 0;
			$link_seller[$pid]= isset($row['has_sellerlink'])? $row['has_sellerlink'] : '';
		}
			
		$resultsArray=Product::getProductsProperties((int)$id_lang, $result);
		for($idx=0;$idx<count($resultsArray); $idx++)
		{
		      $pid = $resultsArray[$idx]['id_product'];
		      $resultsArray[$idx]['seller'] = $seller[$pid];
		      $resultsArray[$idx]['id_seller'] = $id_seller[$pid];
		      $resultsArray[$idx]['has_sellerlink'] = $link_seller[$pid];
		}

		$resultsArray = AgileSellerManager::prepareSellerRattingInfo($resultsArray);
		return $resultsArray;

	}
	
	
	public static function getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, Shop $shop = null, $use_shop_context = true)
	{
		global $cookie;

		if(!Module::isInstalled('agilemultipleseller')) 
			return parent::getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, $shop, $use_shop_context);

		$isSeller = (intval($cookie->profile) == Configuration::get('AGILE_MS_PROFILE_ID') OR intval($cookie->profile) == 0);
		if(!$isSeller)
			return parent::getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, $shop, $use_shop_context);

		require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleseller/agilemultipleseller.php");
		require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleseller/SellerInfo.php");

		if(intval($cookie->profile) > 0)
		{
			 			$id_seller = $cookie->id_employee;
		}	
		else
		{
				        $sellerinfo = new SellerInfo(SellerInfo::getIdByCustomerId($cookie->id_customer));
			$id_seller = $sellerinfo->id_seller;
		}

		$exclude = AgileMultipleSeller::getSpecialCatrgoryIds();
		
		$selectedCat = explode(',', str_replace(' ', '', $selectedCat));		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT c.`id_category`, c.`level_depth`, cl.`name`, IF((
			SELECT COUNT(*)
			FROM `'._DB_PREFIX_.'category` c2
			WHERE c2.`id_parent` = c.`id_category`
		) > 0, 1, 0) AS has_children, '.($selectedCat ? '(
			SELECT count(c3.`id_category`)
			FROM `'._DB_PREFIX_.'category` c3
			WHERE c3.`nleft` > c.`nleft`
			AND c3.`nright` < c.`nright`
			AND c3.`id_category`  IN ('.implode(',', array_map('intval', $selectedCat)).')
		)' : '0').' AS nbSelectedSubCat
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`
		LEFT JOIN `'._DB_PREFIX_.'category_owner` co ON c.id_category=co.id_category
		WHERE `id_lang` = '.(int)($id_lang).'
		AND c.`id_parent` = '.(int)($id_parent).'
		' . (empty($exclude)?'': 'AND c.id_category NOT IN (' . $exclude . ')' ) . '
		AND (IFNULL(co.id_owner,0) = ' . $id_seller . ' OR IFNULL(co.id_owner,0)=0)
		ORDER BY `position` ASC');
	}
			static public function getCategoriesOrdenadoNombre($id_lang = false, $active = true, $order = true, $sql_filter = '', $sql_sort = '',$sql_limit = '')	{		global $cookie;					if(!Module::isInstalled('agilemultipleseller'))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit);	    if(intval($cookie->id_employee)==0)return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit); 	    if(intval($cookie->profile) != intval(Configuration::get('AGILE_MS_PROFILE_ID')))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit);					if (!Validate::isBool($active))			die(Tools::displayError());			$sql = '		SELECT *		FROM `'._DB_PREFIX_.'category` c		LEFT JOIN `'._DB_PREFIX_.'category_owner` co ON c.`id_category` = co.`id_category`		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`		WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)($id_lang) : '').'		'.($active ? 'AND `active` = 1' : '');			if(_PS_VERSION_ > '1.5')			$sql .= (Shop::$id_shop_owner>0? ' AND s.id_seller='  . Shop::$id_shop_owner : '' );						$sql .= ($sql_sort != '' ? $sql_sort : 'ORDER BY cl.`name` ASC').'		'.($sql_limit != '' ? $sql_limit : '');					$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);			if (!$order)			return $result;			$categories = array();		foreach ($result AS $row)			$categories[$row['id_parent']][$row['id_category']]['infos'] = $row;			return $categories;	}		public function getTotal($carroceria,$version,$puertas,$fecha_inicio,$fecha_fin,$despiece,$regulacion,$cristal,$mano){			$url = '';		$active = 1;				if ($carroceria){ $url = ' AND p.`carroceria` LIKE \''.Psql($carroceria).'\' '; }		if ($version){$url = $url. ' AND p.`version` LIKE \''.Psql($version).'\' '; }		if ($puertas){ $url = $url. ' AND p.`puertas` LIKE \''.Psql($puertas).'\' '; }		if ($fecha_inicio){ $url = $url. ' AND p.`fecha_inicio` LIKE \''.Psql($fecha_inicio).'\' '; }		if ($fecha_fin){ $url = $url. ' AND p.`fecha_fin` LIKE \''.Psql($fecha_fin).'\' '; }		if ($despiece){ $url = $url. ' AND p.`despiece` LIKE \''.Psql($despiece).'\' '; }		if ($regulacion){ $url = $url. ' AND p.`regulacion` LIKE \''.Psql($regulacion).'\' '; }		if ($cristal){ $url = $url. ' AND p.`cristal` LIKE \''.Psql($cristal).'\' '; }		if ($mano){ $url = $url. ' AND p.`mano` LIKE \''.Psql($mano).'\' '; }			$sql = 'SELECT COUNT(cp.`id_product`) AS total				FROM `'._DB_PREFIX_.'product` p				LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`				LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON ps.`id_product` = p.`id_product`				WHERE ps.`active` = '.(int)$active.					$url.'				AND cp.`id_category` = '.$this->id;		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);		return $result[0]['total'];		}			/**	 * Return current category products	 *	 * @param integer $id_lang Language ID	 * @param integer $p Page number	 * @param integer $n Number of products per page	 * @param boolean $get_total return the number of results instead of the results themself	 * @param boolean $active return only active products	 * @param boolean $random active a random filter for returned products	 * @param int $random_number_products number of products to return if random is activated	 * @param boolean $check_access set to false to return all products (even if customer hasn't access)	 * @return mixed Products or number of products	 */	public function getProductsBuscador($id_lang, $p, $n, $order_by = null, $order_way = null, $get_total = false,$carroceria,$version,$puertas,$fecha_inicio,$fecha_fin,$despiece,$regulacion,$cristal,$mano)	{			$url = ' ';			if ($carroceria){ $url = ' AND p.`carroceria` LIKE \''.Psql($carroceria).'\' '; }		if ($version){ $url = $url. ' AND p.`version` LIKE \''.Psql($version).'\' '; }		if ($puertas){ $url = $url. ' AND p.`puertas` LIKE \''.Psql($puertas).'\' '; }		if ($fecha_inicio){ $url = $url. ' AND p.`fecha_inicio` LIKE \''.Psql($fecha_inicio).'\' '; }		if ($fecha_fin){ $url = $url. ' AND p.`fecha_fin` LIKE \''.Psql($fecha_fin).'\' '; }		if ($despiece){ $url = $url. ' AND p.`despiece` LIKE \''.Psql($despiece).'\' '; }		if ($regulacion){ $url = $url. ' AND p.`regulacion` LIKE \''.Psql($regulacion).'\' '; }		if ($cristal){ $url = $url. ' AND p.`cristal` LIKE \''.Psql($cristal).'\' '; }		if ($mano){ $url = $url. ' AND p.`mano` LIKE \''.Psql($mano).'\' '; }			if (!$context)			$context = Context::getContext();		if ($check_access && !$this->checkAccess($context->customer->id))			return false;			$front = true;		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))			$front = false;					if ($p < 1) $p = 1;			if (empty($order_by))			$order_by = 'position';		else			/* Fix for all modules which are now using lowercase values for 'orderBy' parameter */			$order_by = strtolower($order_by);			if (empty($order_way))			$order_way = 'ASC';		if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd')			$order_by_prefix = 'p';		elseif ($order_by == 'name')		$order_by_prefix = 'pl';		elseif ($order_by == 'manufacturer')		{			$order_by_prefix = 'm';			$order_by = 'name';		}		elseif ($order_by == 'position')		$order_by_prefix = 'cp';			if ($order_by == 'price')			$order_by = 'orderprice';			if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way))			die (Tools::displayError());			$id_supplier = (int)Tools::getValue('id_supplier');			/* Return only the number of products */		if ($get_total)		{			$sql = 'SELECT COUNT(cp.`id_product`) AS total					FROM `'._DB_PREFIX_.'product` p					'.Shop::addSqlAssociation('product', 'p').'					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`					WHERE cp.`id_category` = '.(int)$this->id.						($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').						($active ? ' AND product_shop.`active` = 1' : '').						($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);		}			$sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`,					pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,					il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,					DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'						DAY)) > 0 AS new, product_shop.price AS orderprice				FROM `'._DB_PREFIX_.'category_product` cp				LEFT JOIN `'._DB_PREFIX_.'product` p					ON p.`id_product` = cp.`id_product`				'.Shop::addSqlAssociation('product', 'p').'				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa				ON (p.`id_product` = pa.`id_product`)				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl					ON (product_shop.`id_category_default` = cl.`id_category`					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl					ON (p.`id_product` = pl.`id_product`					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')				LEFT JOIN `'._DB_PREFIX_.'image` i					ON (i.`id_product` = p.`id_product`)'.						Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'				LEFT JOIN `'._DB_PREFIX_.'image_lang` il					ON (image_shop.`id_image` = il.`id_image`					AND il.`id_lang` = '.(int)$id_lang.')				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m					ON m.`id_manufacturer` = p.`id_manufacturer`				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.					$url.'					AND cp.`id_category` = '.(int)$this->id						.($active ? ' AND product_shop.`active` = 1' : '')						.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')						.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')						.' GROUP BY product_shop.id_product';			if ($random === true)		{			$sql .= ' ORDER BY RAND()';			$sql .= ' LIMIT 0, '.(int)$random_number_products;		}		else			$sql .= ' ORDER BY '.(isset($order_by_prefix) ? $order_by_prefix.'.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way).'			LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);		if ($order_by == 'orderprice')			Tools::orderbyPrice($result, $order_way);			if (!$result)			return array();			/* Modify SQL result */		return Product::getProductsProperties($id_lang, $result);	}	/**	 * Return current category products	 *	 * @param integer $id_lang Language ID	 * @param integer $p Page number	 * @param integer $n Number of products per page	 * @param boolean $get_total return the number of results instead of the results themself	 * @param boolean $active return only active products	 * @param boolean $random active a random filter for returned products	 * @param int $random_number_products number of products to return if random is activated	 * @param boolean $check_access set to false to return all products (even if customer hasn't access)	 * @return mixed Products or number of products	 */	public function getDestacados($id_lang, $p, $n, $order_by = null, $order_way = null, $get_total = false, $active = true, $random = false, $random_number_products = 1, $check_access = true, Context $context = null)	{		if (!$context)			$context = Context::getContext();		if ($check_access && !$this->checkAccess($context->customer->id))			return false;			$front = true;		if (!in_array($context->controller->controller_type, array('front', 'modulefront')))			$front = false;					if ($p < 1) $p = 1;			if (empty($order_by))			$order_by = 'position';		else			/* Fix for all modules which are now using lowercase values for 'orderBy' parameter */			$order_by = strtolower($order_by);			if (empty($order_way))			$order_way = 'ASC';		if ($order_by == 'id_product' || $order_by == 'date_add' || $order_by == 'date_upd')			$order_by_prefix = 'p';		elseif ($order_by == 'name')		$order_by_prefix = 'pl';		elseif ($order_by == 'manufacturer')		{			$order_by_prefix = 'm';			$order_by = 'name';		}		elseif ($order_by == 'position')		$order_by_prefix = 'cp';			if ($order_by == 'price')			$order_by = 'orderprice';			if (!Validate::isBool($active) || !Validate::isOrderBy($order_by) || !Validate::isOrderWay($order_way))			die (Tools::displayError());			$id_supplier = (int)Tools::getValue('id_supplier');			/* Return only the number of products */		if ($get_total)		{			$sql = 'SELECT COUNT(cp.`id_product`) AS total					FROM `'._DB_PREFIX_.'product` p					'.Shop::addSqlAssociation('product', 'p').'					LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`					WHERE cp.`id_category` = '.(int)$this->id.						($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '').						($active ? ' AND product_shop.`active` = 1' : '').						($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');			return (int)Db::getInstance(_PS_USE_SQL_SLAVE_)->getValue($sql);		}			$sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, MAX(product_attribute_shop.id_product_attribute) id_product_attribute, product_attribute_shop.minimal_quantity AS product_attribute_minimal_quantity, pl.`description`, pl.`description_short`, pl.`available_now`,					pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,					il.`legend`, m.`name` AS manufacturer_name, cl.`name` AS category_default,					DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'						DAY)) > 0 AS new, product_shop.price AS orderprice				FROM `'._DB_PREFIX_.'category_product` cp				LEFT JOIN `'._DB_PREFIX_.'product` p					ON p.`id_product` = cp.`id_product`				'.Shop::addSqlAssociation('product', 'p').'				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa				ON (p.`id_product` = pa.`id_product`)				'.Shop::addSqlAssociation('product_attribute', 'pa', false, 'product_attribute_shop.`default_on` = 1').'				'.Product::sqlStock('p', 'product_attribute_shop', false, $context->shop).'				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl					ON (product_shop.`id_category_default` = cl.`id_category`					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl					ON (p.`id_product` = pl.`id_product`					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')				LEFT JOIN `'._DB_PREFIX_.'image` i					ON (i.`id_product` = p.`id_product`)'.						Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'				LEFT JOIN `'._DB_PREFIX_.'image_lang` il					ON (image_shop.`id_image` = il.`id_image`					AND il.`id_lang` = '.(int)$id_lang.')				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m					ON m.`id_manufacturer` = p.`id_manufacturer`				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'					AND p.`destacado` = 1 AND cp.`id_category` = '.(int)$this->id						.($active ? ' AND product_shop.`active` = 1' : '')						.($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')						.($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')						.' GROUP BY product_shop.id_product';			if ($random === true)		{			$sql .= ' ORDER BY RAND()';			$sql .= ' LIMIT 0, '.(int)$random_number_products;		}		else			$sql .= ' ORDER BY '.(isset($order_by_prefix) ? $order_by_prefix.'.' : '').'`'.pSQL($order_by).'` '.pSQL($order_way).'			LIMIT '.(((int)$p - 1) * (int)$n).','.(int)$n;			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);		if ($order_by == 'orderprice')			Tools::orderbyPrice($result, $order_way);			if (!$result)			return array();			/* Modify SQL result */		return Product::getProductsProperties($id_lang, $result);	}
}
