<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.

class Link extends LinkCore
{
	public function getLanguageLink($id_lang, Context $context = null)
	{
		$pagename = AgileHelper::getPageName();
		if(Module::isInstalled('agilemultipleshop'))
		{
			require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleshop/agilemultipleshop.php");
			if($pagename =="agilesellers.php")
			{
				return $this->getAgileSellersLink(Tools::getValue('filter'),$id_lang, Tools::getValue('loclevel'), Tools::getValue('parentid'));
			}
			if($pagename =="sellerlocation.php")
			{
				return $this->getSellerLocationLink(AgileMultipleShop::getShopByLocationID(), AgileMultipleShop::getShopByLocationLevel(), NULL,$id_lang);
			}			
		}
		return parent::getLanguageLink($id_lang, $context);		
	}	
	
	public function getPaginationLink($type, $id_object, $nb = false, $sort = false, $pagination = false, $array = false)
	{
		global $link;
		
		$pagename = AgileHelper::getPageName();
		if(Module::isInstalled('agilemultipleshop'))
		{
			require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleshop/agilemultipleshop.php");
			if($pagename =="agilesellers.php")
			{
				$url = $this->getAgileSellersLink(Tools::getValue('filter'),NULL, Tools::getValue('loclevel'), Tools::getValue('parentid'));
				if(strpos($url, "?") === false) $url .= "?";
				$seller_location = Tools::getValue('seller_location');
				if(!empty($seller_location))$url .= "&seller_location=" . $seller_location;
				$seller_type = Tools::getValue('seller_type');
				if(!empty($seller_type))$url .= "&seller_type=" . $seller_type;
				$userview = Tools::getValue('userview');
				if(!empty($userview))$url .= "&userview=" . $userview;
				
				return $url;
			}
			if($pagename =="sellerlocation.php")
			{
				$url = $this->getSellerLocationLink(AgileMultipleShop::getShopByLocationID(), AgileMultipleShop::getShopByLocationLevel());
				if(strpos($url, "?") === false) $url .= "?";
				return $url;
			}
		}
		$fc = Tools::getValue('fc');
		$module = Tools::getValue('module');
		$controller = Tools::getValue('controller');
		if($fc == 'module' AND !empty($module) AND !empty($controller))
		{
			$url = $this->getModuleLink($module,$controller,array(),true);
			if(strpos($url, "?") === false) $url .= "?";
			return $url;
		}

		if($pagename =="sellerorders.php")
		{
			$url = $this->getModuleLink('agilemultipleseller','sellerorders',array(),true);
			if(strpos($url, "?") === false) $url .= "?";
			return $url;
		}

		if($pagename =="sellerhistory.php")
		{
			$url = $this->getModuleLink('agilemultipleseller','sellerhistory',array(),true);
			if(strpos($url, "?") === false) $url .= "?";
			return $url;
		}
		
		return parent::getPaginationLink($type, $id_object, $nb, $sort, $pagination, $array);
	}  

    public function getMySellerAccountLink($id_lang = NULL)
    {
		if($this->allow)
		{
			$myseller_account_dir = Configuration::get('AGILE_MS_MYSELLER_URL_DIRECTORY');
			if(empty($myseller_account_dir))$myseller_account_dir = 'my-seller-account';		
			$link = '';
			$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)$id_lang));
			$link .= $myseller_account_dir;
    		return $link;
        }
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'myselleraccount.php';
    }

	public function getAgileSellerLink($id_seller, $alias = NULL, $id_lang = NULL)
	{
		if(!$id_seller)return '';

        include_once(_PS_ROOT_DIR_  . "/modules/agilemultipleseller/SellerInfo.php");

		$sellerinfo = new SellerInfo(SellerInfo::getIdBSellerId($id_seller));
		$id_shopurl = Shop::get_main_url_id($sellerinfo->id_shop);
		$shopurl = new ShopUrl($id_shopurl);
		$url = $shopurl->getURL()  .$this->getLangLink();
		if(Module::IsInstalled('agilemultipleshop'))
		{
	        include_once(_PS_ROOT_DIR_  . "/modules/agilemultipleshop/agilemultipleshop.php");
			if((int)Configuration::get('ASP_SHOP_URL_MODE') == AgileMultipleShop::SHOP_URL_MODE_DOMAIN)
		
			return $url;
		}

		if($this->allow)
		{
			return $url . "home";
		}
		else
		{
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=agileseller&id_seller='.(int)($id_seller);
		}
	}
	
	

	private function getParentName($loclevel,$parentid, $id_lang)
	{
		if($loclevel == 'country'){
			return 'all';
		}
		if($loclevel == 'state'){
			$country = new Country($parentid, $id_lang);
			if(Validate::isLoadedObject($country))return $country->name;
		}
		if($loclevel == 'city'){
			$state = new State($parentid);
			if(Validate::isLoadedObject($state))return $state->name;
		}
		
	}
	
	public function getAgileSellersLink($namefilter = 'all', $id_lang=NULL, $loclevel = NULL,$parentid=NULL)
	{
		global $cookie;
		
		if($namefilter == NULL)$namefilter = 'all';
		if($loclevel ==NULL)$loclevel = 'country';
		if($parentid ==NULL)$parentid = 0;
		if($id_lang == NULL)$id_lang = $cookie->id_lang;
		if($this->allow)
		{
			$parentname = 'all';
			if($parentid>0)$parentname = Tools::link_rewrite($this->getParentName($loclevel,$parentid, $id_lang), true);
			$link = '';

			$shop = new Shop(Configuration::get('PS_SHOP_DEFAULT'));
			$link = __PS_BASE_URI__ . $this->getLangLink((int)$id_lang);

			$link .= 'shops-name-start-with-' . $namefilter;
			$link .= '-by-' . ($loclevel == NULL?'country':$loclevel) . '-in-' . intval($parentid) . '-' . $parentname;

    		return $link;
        }
		
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=agilesellers&filter=' . $namefilter . '&loclevel=' .  ($loclevel == NULL?'country':$loclevel) . '&parentid=' . ($parentid==NULL?0:$parentid);
	}
	
		
	public function getSellerCountryLink($id_country, $alias = NULL, $id_lang = NULL)
	{
	    global $cookie;
	    
        if(!intval($id_lang))$id_lang = $cookie->id_lang;
		if($this->allow)
		{
			$link = '';
			$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)$id_lang));
            if(!isset($alias) OR empty($alias))
            {
                $country = new Country($id_country,$id_lang);
                if(Validate::isLoadedObject($country))
                    $alias = Tools::link_rewrite($country->name);                
			}
			
			$link .= 'countries/' . $id_country . '-' . (empty($alias)?'all': Tools::link_rewrite($alias));
    		return $link;
        }
		if(_PS_VERSION_ > '1.5')
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=sellercountry&id_seller_country=' . $id_country;
		else
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'sellercountry.php?id_seller_country=' . $id_country;
	}

	public function getSellerLocationLink($id_location, $location_level='country', $alias = NULL, $id_lang = NULL)
	{
		global $cookie;

		include_once(_PS_ROOT_DIR_ . "/modules/agilemultipleshop/agilemultipleshop.php");
		include_once(_PS_ROOT_DIR_ . "/modules/agilemultipleshop/SellerType.php");
		
		if(!isset($location_level) || empty($location_level))$location_level = 'country';
		if(!intval($id_lang))$id_lang = $cookie->id_lang;
		if($this->allow)
		{
			$link = '';
			$link .= (_PS_BASE_URL_SSL_.__PS_BASE_URI__.$this->getLangLink((int)$id_lang));
			if(!isset($alias) OR empty($alias))
			{
				switch($location_level)
				{
					case 'country':
						$country = new Country($id_location,$id_lang);
						if(Validate::isLoadedObject($country))
							$alias = Tools::link_rewrite($country->name);   
						break;             
					case 'state':
						$state = new State($id_location);
						if(Validate::isLoadedObject($state))
							$alias = Tools::link_rewrite($state->name);   
						break;             
					case 'city':
						$alias = '';						break;
					case 'sellertype':
						$sellertype = new SellerType($id_location, $id_lang);
						$alias = $sellertype->name;   
						break;
					case 'custom':
						$alias = '';						break;
				}
			}
			$link .= 'shop-by-' . ($location_level == 'custom'? strtolower(AgileMultipleShop::SHOP_BY_CUSTOM_NAME) : $location_level) . '/' . Tools::link_rewrite($id_location) . '-' . (empty($alias)?'all': Tools::link_rewrite($alias));
			return $link;
		}
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=sellerlocation&id_location=' . $id_location . '&location_level=' . $location_level;
	}
	
	public function getShowcaseFormLink($alias = NULL, $id_lang = NULL)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=showcaseform';
	}

	public function getShowcaseViewLink($id_showcase,$alias = NULL, $id_lang = NULL)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=showcaseview&id_showcase=' . $id_showcase;
	}

	public function getShowcaseListLink($alias = NULL, $id_lang = NULL)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');
		if($allow)
		{
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'showcaselist';
		}
		else
		{
			return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'index.php?controller=showcaselist';
		}
	}
	

	public function getRatingListLink($id_type, $id_target, $alias = NULL)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');
		if(version_compare(_PS_VERSION_,'1.5','>='))
			return $this->getModuleLink("agilesellerratings","ratinglist", array('id_type'=>$id_type, 'id_target'=>$id_target), true);
			
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'modules/agilesellerratings/ratinglist.php?id_type=' . $id_type . '&id_target=' .$id_target;
	}


	public function getFeedbackWaitListLink($alias = NULL)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');
		if(version_compare(_PS_VERSION_,'1.5','>='))
			return $this->getModuleLink("agilesellerratings","feedbackwaitlist", array(), true);
		
		return _PS_BASE_URL_SSL_.__PS_BASE_URI__.'modules/agilesellerratings/feedbackwaitlist.php';
	}
}
