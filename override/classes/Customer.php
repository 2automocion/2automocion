<?php

class Customer extends CustomerCore
{
	public function getBoughtProducts()
	{
		if(!Module::isInstalled('agilemultipleseller'))return parent::getBoughtProducts();
		$context = Context::getContext();
		if($context->cookie->id_employee == 0 || ($context->cookie->profile != (int)Configuration::get('AGILE_MS_PROFILE_ID')))return parent::getBoughtProducts();  
		
		$sql = 'SELECT * 
				FROM `'._DB_PREFIX_.'orders` o
					LEFT JOIN `'._DB_PREFIX_.'order_detail` od ON o.id_order = od.id_order
					LEFT JOIN `'._DB_PREFIX_.'product_owner` po ON od.product_id = po.id_product
				WHERE o.valid = 1 
					AND o.`id_customer` = '.(int)$this->id . '
					AND po.`id_owner` = '.(int)$context->cookie->id_employee . '
				';

			return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	}
}

