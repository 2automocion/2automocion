<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.
class FrontController extends FrontControllerCore{
    
	public function initHeader()	{
        parent::initHeader();

        if(Module::isInstalled('agilemultipleshop')){

            include_once(_PS_ROOT_DIR_  . "/modules/agilemultipleshop/agilemultipleshop.php");
            AgileMultipleShop::init_shop_header();
            AgileMultipleShop::clear_blockcategory_cache();

        }
    }

    public function setMedia()	{

        if ($this->context->getMobileDevice() != false){
            $this->setMobileMedia();
            return true;
        }
        if (Tools::file_exists_cache(_PS_ROOT_DIR_.Tools::str_replace_once(__PS_BASE_URI__, DIRECTORY_SEPARATOR, _THEME_CSS_DIR_.'grid_prestashop.css'))){
            $this->addCSS(_THEME_CSS_DIR_.'grid_prestashop.css', 'all');
        }
        $this->addCSS(_THEME_CSS_DIR_.'global.css', 'all');
        $this->addjquery();
        $this->addjqueryPlugin('easing');
        $this->addJS(_PS_JS_DIR_.'tools.js');
        $this->addCSS(_THEME_CSS_DIR_.'global.css', 'all');
        $this->addCSS(_THEME_CSS_DIR_.'menu.css', 'all');
        $this->addCSS(_THEME_CSS_DIR_.'product2_old.css', 'all');
        $this->addjquery();
        $this->addjqueryPlugin('easing');
        $this->addJS(_PS_JS_DIR_.'tools.js');
        $this->addJS(_THEME_JS_DIR_.'menuSuperior.js');

        if (Tools::isSubmit('live_edit') && Tools::getValue('ad') && Tools::getAdminToken('AdminModulesPositions'.(int)Tab::getIdFromClassName('AdminModulesPositions').(int)Tools::getValue('id_employee')))		{
            $this->addJqueryUI('ui.sortable');
            $this->addjqueryPlugin('fancybox');
            $this->addJS(_PS_JS_DIR_.'hookLiveEdit.js');
            $this->addCSS(_PS_CSS_DIR_.'jquery.fancybox-1.3.4.css', 'all');
        }

        if ($this->context->language->is_rtl){
            $this->addCSS(_THEME_CSS_DIR_.'rtl.css');
        }

        Hook::exec('actionFrontControllerSetMedia', array());

    }

    public function initContent()	{

        $this->process();

        if (!isset($this->context->cart)){

            $this->context->cart = new Cart();
        }

        if ($this->context->getMobileDevice() == false)		{

            if (!isset($this->context->cart)){

                $this->context->cart = new Cart();

            }

            $this->context->smarty->assign(array('HOOK_HEADER' => Hook::exec('displayHeader'),'HOOK_TOP' => Hook::exec('displayTop'),'HOOK_LEFT_COLUMN' => ($this->display_column_left ? Hook::exec('displayLeftColumn') : ''),'HOOK_RIGHT_COLUMN' => ($this->display_column_right ? Hook::exec('displayRightColumn', array('cart' => $this->context->cart)) : ''),'HOOK_DESTACADOS'=> Hook::exec('destacados'),'HOOK_REASEGUROS'=> Hook::exec('reaseguros'),));

        }else{

            $this->context->smarty->assign(array('HOOK_MOBILE_HEADER' => Hook::exec('displayMobileHeader'),));

        }
            $paginasNoColumnas = array('product'=> 3,'listadoclientes' => 0,'index' => 1,'category' => 1,'contact'=>3,'cms'=>3,'authentication'=>1,	'dosa'=>1,'order-opc'=>1,'search'=>1,'cliente'=>1,'module-pagonuevo-payment'=>1,'my-account'=>1,'history'=>1,'order-slip'=>1,'addresses'=>1,'address'=>1,'identity'=>1,'module-favoriteproducts-account'=>1,'module-cashondelivery-validation'=>1,'order-confirmation'=>1);
            $this->context->smarty->assign(array('paginasNoColumnas' => $paginasNoColumnas));
            $this->context->smarty->assign(array('ENT_QUOTES' =>ENT_QUOTES,	'search_ssl' =>	Tools::usingSecureMode(),'ajaxsearch' =>Configuration::get('PS_SEARCH_AJAX'),'instantsearch' =>	Configuration::get('PS_INSTANT_SEARCH'),'self' =>dirname(__FILE__),));

    }





}
