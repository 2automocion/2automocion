{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<script>

	$(document).ready(function(){
		
		$("#center_column").css("width","77%");
		$("#center_column").css("marginRight","0px");
	
	});

</script>

<div id="contenedor">
		
		<div id="contenedorImagen">
		
			<div id="imagen">
			
				<img src="{$path}img/1.jpg">
				<img src="{$path}img/2.jpg">
				<img src="{$path}img/3.jpg">
				<img src="{$path}img/4.jpg">
				<img src="{$path}img/5.jpg">
				<img src="{$path}img/6.jpg">
				
				<!--<p id="titulo1">SOMOS UNA EMPRESA DE RECAMBIOS DE AUTOM&Oacute;VIL<BR></p>-->
				<!--<p id="titulo1_1">TAMBI&Eacute;N ONLINE 100% SEGURA</p>-->
				<!--<p id="titulo2">MAS DE 3000 REFERENCIAS DE RETROVISORES</p>-->
				<!--<p id="titulo3">SERVICIO URGENTE 24 HORAS<br> -->
				</p>
				
				<!--<input type="button" id="boton_1" value="" class="botonSlider">
				<p id="titulo4">2.000.000 visitas y m&aacute;s de 100.000 contactos de ventas al mes</p>-->
				<!--<img src="{$path}img/dosaLogo.png" id="dosaLogo">-->
				<!--<img src="{$path}img/garantia.png" id="garantia">-->
				<input type="hidden" id="url" value="{$base_url}index.php?controller=my-account">
				
				<!--<p id="titulo5">TE INTERESA COMPRAR, VENDER<br>, PRODUCTOS FARMACEUTICOS ?</p>
				<p id="titulo6">Y VENDER MOBILIARIO O <span id="utensilios">UTENSILIOS</span> ?</p>
				<p id="titulo7">PODRAS ENCONTRAR TAMBI&Eacute;N UN BUEN LISTADO DE <span id="vendedores">VENDEDORES,</span><br>PROVEEDORES, DISTRIBUIDORES Y FABRICANTES A <span id="nivelNacional">NIVEL NACIONAL.</span></p>
				
				<input type="button" id="boton_2" class="botonSlider" value="">-->
				
				<!--<p id="titulo8">EN MATERIALCLINIC.COM PODRAS ENCONTRAR TODOS<br>LOS UTIENSILIOS E INSTRUMENTAL QUE NECESITES<br>EN EL SECTOR DE LA ODONTOLOGIA</p>
				
				<p id="titulo9">Y TAMBI&Eacute;N VENDER <br>LOS TUYOS PROPIOS</p>		
				
				<input type="button" id="boton_3" class="botonSlider" value="">
				
				<p id="titulo10">PODRAS ENCONTRAR TAMBI&Eacute;N UN BUEN LISTADO DE VENDEDORES,<br>PROVEEDORES, DISTRIBUIDORES Y FABRICANTES A NIVEL NACIONAL.</p>
				
				
				
				<p id="titulo11">Estas buscando una silla de ruedas ?<br>Y un andador ?<br>O quiz&aacute;s una muleta ?<br> </p>
				
				<p id="titulo12">O QUIZ&Aacute;S QUIERAS VENDER <br>TUS PRODUCTOS ?</p>
				
				<input type="button" id="boton_4" class="botonSlider" value="">
				
				<p id="titulo13">PODRAS ENCONTRAR TAMBI&Eacute;N UN BUEN LISTADO DE VENDEDORES,<br>PROVEEDORES, DISTRIBUIDORES Y FABRICANTES A NIVEL NACIONAL.</p>
				
				
				<p id="titulo14">Estas buscando instrumental cl&iacute;nico ?<br>Y vender instrumental ?<br>O quiz&aacute;s adquirir instrumental ?</p> 
				
				<p id="titulo15">O QUIZ&Aacute;S RECIBIR PRESUPUESTOS<br>SOBRE INSTRUMENTAL ?</p>
				
				<input type="button" id="boton_5" class="botonSlider" value="">
				
				<p id="titulo16">PODRAS ENCONTRAR TAMBI&Eacute;N UN BUEN LISTADO DE VENDEDORES,<br>PROVEEDORES, DISTRIBUIDORES Y FABRICANTES A NIVEL NACIONAL.</p>
				
				
				<p id="titulo17">Estas buscando instrumental cl&iacute;nico ?<br>Y vender instrumental ?<br>O quiz&aacute;s adquirir instrumental ?</p> 
				
				<p id="titulo18">Eres vendedor / distribuidor / proveedor / frabricante / cliente ? <BR>Te interesa contactar con empresas ?<BR>Crees que pudes conseguir mejores precios ? </p>
				
				<input type="button" id="boton_6" class="botonSlider" value="">
				
				<p id="titulo19">EN MATERIALCLINIC.COM PODRAS ENCONTRAR TODO TIPO DE<BR>EMPRESAS DEL SECTOR CLINICO Y SANITARIO... COMPRUEBALO !!!</p>
				-->
				
			</div>
		
		</div>
		
		<img src="{$path}img/2.png" id="flecha">
		
		<ul id="lista">
			<li><a href="#">DOSA AUTOMOCI&Oacute;N</a></li>
			<li><a href="#">RECAMBIOS</a></li>
			<li><a href="#">RETROVISORES</a></li>
			<li><a href="#">CAT&Aacute;LOGO</a></li>
			<li><a href="#">CLIENTES</a></li>
			<li><a href="#">CONTACTO</a></li>
		</ul>
		
	</div>
	
	<!--
	<div class="header">
		<h4 class="pruebamodulo-title title_block">{*l s='Ofertas de odontologia ' mod='lofnewproduct'*}</h4>
	</div>
	-->
	
	
	