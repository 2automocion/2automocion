jQuery(document).ready(function($){

	// ************************************   Nuestro Objeto   ****************************** */
	
	var miObjeto = {
		
		indice : 0,
		
		rollover:function(){	
			
			$("#lista li").each(function(index){
				
				var a = $(this).find('a');
				var b = $(this);
				
				if (index == 0){
					
					b.css({"background-color" : "#006239"});
					
				}// End if
				
				$(this).mouseover(function(){
					
					if (index != miObjeto.indice){
						
						$(this).css({"background-color" : "#71a104"});
					
					}// End if
					
				});
				
				$(this).mouseout(function(){
					
					//b.css({"background-color" : "#023748"});
					
					if (index != miObjeto.indice){
						
						$(this).css({"background-color" : "#82bc00"});
						
					}// End if
					
				});
				
				$(this).click(function(event){
					
					$("#lista li").each(function(index2){
					
						if (miObjeto.indice == index2){
							
							$(this).css({"background-color" : "#82bc00"});
							
						}// End if
					
					});
					
					miObjeto.indice = index;
					valor = 0 + (index * 44.5 + 1 * index);
					$("#flecha").css({"top":valor})
					$(this).css({"background-color" : "#006239"});
					
					miObjeto.mover();
					
					event.preventDefault();
					
				});
				
				
			});
			
		},
		
		mover:function(){
			
			valor = (miObjeto.indice * 575) * -1;
			
			$("#imagen").animate({
				 
				 left: valor+'px',
				 }, 500, function() {
				     
					 
			});	
			
		},
			
		
		registrar:function(){
			
			$(".botonSlider").click(function(){
				
				$url = $("#url").val();
				window.location.href = $url;
				
			});
			
		}
		
	}// Fin de objeto
	
	
	miObjeto.rollover();
	miObjeto.registrar();
	
});