<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;


class Blockbuscador extends Module
{
	
	public $contador;
	public $page_name = '';
	
	public $coches 				   = array();
	public $modelos				   = array();
	public $versiones			   = array();
	public $carrocerias			   = array();
	public $fechas_inicio		   = array();
	public $fechas_fin			   = array();
	public $productos 			   = array();
	
	// B�squeda avanzada
	public $despiece 			   = array();
	public $regulacion 			   = array();
	public $cristal 			   = array();
	public $mano 			   	   = array();
	
	
	
	public $contadorCoches 		   = 0;
	public $contadorModelos 	   = 0;
	public $contadorVersiones      = 0;
	public $contadorCarrocerias    = 0;
	public $contadorFechas_inicio  = 0;
	public $contadorFechas_fin     = 0;
	public $contadorProductos      = 0;
	

	public $contadorEspecialidades = 0;
	
    public function __construct()
    {
        
    	$this->page_name = Dispatcher::getInstance()->getController();
		$this->page_name = (preg_match('/^[0-9]/', $this->page_name)) ? 'page_'.$this->page_name : $this->page_name;
		
    	$this->name = 'blockbuscador';
        $this->tab = 'front_office_features';
        $this->version = 1.0;
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

        parent::__construct();

		$this->displayName = $this->l('Buscador block');
        $this->description = $this->l('Display a categories block');
       
        
    }
	 
    
	public function install()
	{
		
		Configuration::updateValue('MANUFACTURER_DISPLAY_TEXT', true);
		Configuration::updateValue('MANUFACTURER_DISPLAY_TEXT_NB', 5);
		Configuration::updateValue('MANUFACTURER_DISPLAY_FORM', true);
		
        return parent::install() || $this->registerHook('header') || $this->registerHook('top');
        
    }

	
	
	public function devolverIdEspecialidad($resQuery,$idCat){
	
		$idEspe = 0;
	
		foreach($resQuery as $identificadorEspecialidad) {
				
			if ($identificadorEspecialidad['id_category'] == $idCat ){
	
				//echo ('Devuelve la especialidad'.$identificadorEspecialidad['id_parent']);
				$idEspe = $identificadorEspecialidad['id_parent'];
	
			}// End if
				
		}
	
		return $idEspe;
	
	}// End function
	
	
	public function hookHeader($params)
	{
		
		global $smarty;
		
		
		$this->context->controller->addCSS(($this->_path).'blockbuscador.css', 'all');
		$this->context->controller->addCSS(($this->_path).'blockpublicidad.css', 'all');
		$this->context->controller->addCSS(($this->_path).'csshorizontalmenu.css', 'all');
		
		$this->context->controller->addCSS((_THEME_CSS_DIR_).'menu.css', 'all');
		$this->context->controller->addJS(_THEME_JS_DIR_.'menuSuperior.js');
		
		
		
		
	}
	
	public function hookTop($params){
		
		//if ($this->page_name == 'index' || $this->page_name == 'category' || $this->page_name == 'product'){
			
			$this->makeBuscador();
			$this->probar();
			
			
			return $this->display(__FILE__, 'blockbuscador.tpl');
			
		//}
		
			
	}
	
	public function makeBuscador(){
	
		$correcto   = 0;
		$opcion     = 0;
		$saberNivel = 0;
	
		$saberNivel = 2;
		
		$this->guardarEspecialidades(0,0);
		
		/*
		if (Tools::getValue('id_category')){
	
			$categoria   = new Category((int)Tools::getValue('id_category'),$this->context->language->id,1);
			$saberNivel  = $categoria->level_depth;
					
			$this->guardarEspecialidades(Tools::getValue('id_category'),$saberNivel);
			
				
				
		}else{
				
			$saberNivel = 2;
				
			$this->guardarEspecialidades(0,0);
			
				
		}
		*/
	
		$this->smarty->assign('especialidades',$this->especialidades);
		$this->smarty->assign('idEspecialidad',$this->idEspecialidad);
		
	
		$this->despiece   = $this->getDespiece();
		$this->regulacion = $this->getRegulacion();
		$this->cristal    = $this->getCristal();
		$this->mano       = $this->getMano();	
		
		$this->smarty->assign('despiece',$this->despiece);
		$this->smarty->assign('regulacion',$this->regulacion);
		$this->smarty->assign('cristal',$this->cristal);
		$this->smarty->assign('mano',$this->mano);
		
	
	}
	
	
	public function getDespiece(){
	
		$sql = 'SELECT  d.*
				FROM `'._DB_PREFIX_.'despiece` d';
		$despiece = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	
		return $despiece;
	
	}
	
	public function getRegulacion(){
	
		$sql = 'SELECT  d.*
				FROM `'._DB_PREFIX_.'regulacion` d';
		$regulacion = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	
	
	
		return $regulacion;
	
	}
	
	public function getCristal(){
	
		$sql = 'SELECT  d.*
				FROM `'._DB_PREFIX_.'cristal` d';
		$cristal = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	
	
	
		return $cristal;
	
	}
	
	public function getMano(){
	
		$sql = 'SELECT  d.*
				FROM `'._DB_PREFIX_.'mano` d';
		$mano = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
	
	
	
		return $mano;
	
	}
	
	
	
	public function guardarEspecialidades($id_category,$nivel){
	
	
		/*	
		echo ('-------------- Valores -----------<br>');
		echo('idCategory: '.$id_category.'<br>');
		echo('-----------------------------------<br>');
		echo('idCategory: '.$nivel.'<br>');
		*/
		
		if ($id_category != 0 && $nivel != 1){
				
			switch ($nivel){
	
				case 2:
	
					$categoria  		  = new Category((int)$id_category,$this->context->language->id,1);
					$menu_items 		  = $categoria->getCategories($this->context->language->id,true,true,'AND c.id_parent='.$id_category,'');
					$this->idEspecialidad = 0;
	
					break;
	
				case 3:
	
					$especialidad		  = new Category((int)$id_category,$this->context->language->id,1);
					$categoria  		  = new Category((int)$especialidad->id_parent,$this->context->language->id,1);
					$menu_items 		  = $especialidad->getCategories($this->context->language->id,true,true,'AND c.id_parent='.$categoria->id_category,'');
					$this->idEspecialidad = $id_category;
						
						
					break;
	
				case 4:
	
					$categoriaHija 				= new Category((int)$id_category,$this->context->language->id,1);
					$this->idEspecialidad 		= $categoriaHija->id_parent;
					$categoriaEspecialidad 		= new Category((int)$this->idEspecialidad,$this->context->language->id,1);
					$categoria					= new Category((int)$categoriaEspecialidad->id_parent,$this->context->language->id,1);
					$menu_items 				= $categoria->getCategories($this->context->language->id,true,true,'AND c.id_parent='.$categoriaEspecialidad->id_parent,'');
						
	
					break;
	
				case 5:
						
					$categoriaNieto				= new Category((int)$id_category,$this->context->language->id,1);
					$idCategoriaHijo			= $categoriaNieto->id_parent;
					$categoriaHijo				= new Category((int)$idCategoriaHijo,$this->context->language->id,1);
					$this->idEspecialidad 		= $categoriaHijo->id_parent;
					$categoriaEspecialidad		= new Category((int)$this->idEspecialidad,$this->context->language->id,1);;
					$idCategoriaPadre			= $categoriaEspecialidad->id_parent;
					$categoria					= new Category((int)$idCategoriaPadre,$this->context->language->id,1);;
					$menu_items					= $categoria->getCategories($this->context->language->id,true,true,'AND c.id_parent='.$idCategoriaPadre,'');
						
					break;
	
			}
				
				
			foreach ($menu_items[$categoria->id_category] as $item)
			{
					
				$idEspecialidad 				= $item['infos']['id_category'];
				$nombreEspecialidad 			= $item['infos']['name'];
					
				$this->especialidades[$this->contadorEspecialidades]['id_category'] 		= $idEspecialidad;
				$this->especialidades[$this->contadorEspecialidades]['name']  				= $nombreEspecialidad;
					
				$this->contadorEspecialidades++;
					
			}
				
			//var_dump($this->especialidades);
				
			$this->contadorEspecialidades = 0;
			//$this->idEspecialidad = 0;
				
		}else{
				
			$categoria  = new Category(2,$this->context->language->id,1);
			$menu_items = $categoria->getCategories($this->context->language->id,true,true,'AND c.id_parent=2','','');
			
			
			
			$correcto 	= 1;
				
			foreach ($menu_items[$categoria->id_category] as $item)
			{
					
				$idEspecialidad 				= $item['infos']['id_category'];
				$nombreEspecialidad 			= $item['infos']['name'];
					
				$this->especialidades[$this->contadorEspecialidades]['id_category'] 		= $idEspecialidad;
				$this->especialidades[$this->contadorEspecialidades]['name']  				= $nombreEspecialidad;
					
				$this->contadorEspecialidades++;
					
			}
				
			$this->idEspecialidad = 0;
				
		}
	
		$this->contadorEspecialidades = 0;
	
		//var_dump($this->especialidades);
	
	}
	
	
	public function probar(){

		$subcategorias = array();
		$contadorCategorias = 0;
		
		$categoria   = new Category(56,$this->context->language->id,1);
		$menu_items  = $categoria->getCategories($this->context->language->id,true,true,'AND c.id_parent=56','','');
		$resultado   = 0;
		$subcategorias = array();
		
		//var_dump($menu_items);
		
		foreach ($menu_items[$categoria->id_category] as $item)
		{
		
			$idSeccion 						= $item['infos']['id_category'];
			$nombreSeccion 					= $item['infos']['name'];
		
			$subcategorias[$contadorCategorias]['id_category'] 					= $idSeccion;
			$subcategorias[$contadorCategorias]['name']  						= $nombreSeccion;

			//echo("Id Seccion: ".$idSeccion.'<br>');
			//echo("Nombre Seccion: ".$nombreSeccion.'<br>');
			
			$nuevaCategoria = new Category((int)$idSeccion,$this->context->language->id,1);
			
			if ($idSeccion == 633){
				
				$nb = 1000;
				$resultado = $nuevaCategoria->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 50000) );
				$this->versiones($resultado);
				
				
			}
			
			
			$contadorCategorias++;
			
			
		
		}
		
		
	}
	
	
	
	
	public function versiones($vector){
		
		$contador   = 0;
		$vectorTemp = array();
		$encontrado = false;
		
		for ($i=0;$i<count($vector);$i++){
					
			for ($n=0;$n<count($vectorTemp);$n++){
				
				if ($vector[$i]['version'] === $vectorTemp[$n]['version']){
						
					//echo ('Encontrado: '.$vector[$i]['carroceria'].'<br>');
					$encontrado = true;
						
				}
		
			}
				
			if ($encontrado == false){
		
				$vectorTemp[count($vectorTemp)] = $vector[$i];
				
				
			}
				
			$encontrado = false;
				
		}
		
		$encontrado = false;
		
		for ($m = 0;$m < count($vectorTemp);$m++){
				
			for ($p=0;$p < count($this->productos);$p++){
		
		
				if ($this->productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
						
					$encontrado = true;
						
				}// End if
		
					
			}// End for
				
		
			if ($encontrado == false){
		
				$this->productos[count($this->productos)] = $vectorTemp[$m];
		
			}
				
			$encontrado = false;
			
			
		}// End for
		
		
		/*
		echo('------------ Listado de versiones -----------<br>');
		
		foreach ($this->productos as $producto){
			
			echo('Referencia: '.$producto["reference"].'<br>');
			echo('Version: '.$producto["version"].'<br>');
			
		}
		
		echo('---------------------------------------------<br>');
		*/
		
		$this->carroceria($vector);
		
	}
	
	public function carroceria($vector){
		
		$contador   = 0;
		$vectorTemp = array();
		$encontrado = false;
		
		for ($i=0;$i<count($vector);$i++){
		
			for ($n=0;$n<count($vectorTemp);$n++){
				
				if ($vector[$i]['carroceria'] === $vectorTemp[$n]['carroceria']){
					
					//echo ('Encontrado: '.$vector[$i]['carroceria'].'<br>');
					$encontrado = true;
					
				}
				
			}
			
			if ($encontrado == false){
				
				$vectorTemp[count($vectorTemp)] = $vector[$i];
				
			}
			
			$encontrado = false;
			
		}
		
		$encontrado = false;
		
		for ($m = 0;$m < count($vectorTemp);$m++){
			
			for ($p=0;$p < count($this->productos);$p++){
				
				
				if ($this->productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
					
					$encontrado = true;
					
				}// End if
				
			
			}// End for
			
		
			if ($encontrado == false){
				
				$this->productos[count($this->productos)] = $vectorTemp[$m];
				
			}
			
			
			$encontrado = false;
			
		}// End for
		
		/*
		echo('------------ Listado de carrocerias -----------<br>');
		foreach ($this->productos as $producto){
			
			echo('Referencia: '.$producto["reference"].'<br>');
			echo('Carroceria: '.$producto["carroceria"].'<br>');
			
		}
		echo('---------------------------------------------<br>');
		*/
		
		$this->fechaInicioFechaFin($vector);
		
	}
	
	
	public function fechaInicioFechaFin($vector){
		
		$contador   = 0;
		$vectorTemp = array();
		$encontrado = false;
		
		for ($i=0;$i<count($vector);$i++){
					
			for ($n=0;$n<count($vectorTemp);$n++){
		
				if ($vector[$i]['fecha_inicio'] === $vectorTemp[$n]['fecha_inicio']){
						
					if ($vector[$i]['fecha_fin'] === $vectorTemp[$n]['fecha_fin']){
					
						//echo ('Encontrado: '.$vector[$i]['fecha_fin'].'<br>');
						$encontrado = true;

					}
					
				}
		
			}
				
			if ($encontrado == false){
		
				$vectorTemp[count($vectorTemp)] = $vector[$i];
		
			}
				
			$encontrado = false;
				
		}
		
		$encontrado = false;
		
		for ($m = 0;$m < count($vectorTemp);$m++){
			
			for ($p=0;$p < count($this->productos);$p++){
				
				
				if ($this->productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
					
					$encontrado = true;
					
				}// End if
				
			
			}// End for
			
		
			if ($encontrado == false){
				
				$this->productos[count($this->productos)] = $vectorTemp[$m];
				
			}
			
			$encontrado = false;
			
			
		}// End for
		
		/*
		echo('------------ Listado de fechas -----------<br>');
		foreach ($this->productos as $producto){
			
			echo('Referencia: '.$producto["reference"].'<br>');
			echo('Fecha inicio: '.$producto["fecha_inicio"].'<br>');
			echo('Fecha fin: '.$producto["fecha_fin"].'<br>');
			
		}
		echo('---------------------------------------------<br>');
		*/
		
		$this->puertas($vector);
		
		
	}
	
	
	public function puertas($vector){
		
		$contador   = 0;
		$vectorTemp = array();
		$encontrado = false;
		
		
		for ($i=0;$i<count($vector);$i++){
		
			for ($n=0;$n<count($vectorTemp);$n++){
		
				if ($vector[$i]['puertas'] === $vectorTemp[$n]['puertas']){
		
					if ($vector[$i]['puertas'] === $vectorTemp[$n]['puertas']){
							
						//echo ('Encontrado: '.$vector[$i]['puertas'].'<br>');
						$encontrado = true;
		
					}
						
				}
		
			}
		
			if ($encontrado == false){
		
				$vectorTemp[count($vectorTemp)] = $vector[$i];
		
			}
		
			$encontrado = false;
		
		}
		
		$encontrado = false;
		
		for ($m = 0;$m < count($vectorTemp);$m++){
				
			for ($p=0;$p < count($this->productos);$p++){
		
		
				if ($this->productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
						
					$encontrado = true;
						
				}// End if
		
					
			}// End for
				
		
			if ($encontrado == false){
		
				$this->productos[count($this->productos)] = $vectorTemp[$m];
				
			}
		
			$encontrado = false;
			
			
		}
		
		/*
		echo('------------ Listado de puertas -----------<br>');
		foreach ($this->productos as $producto){
			
			echo('Referencia: '.$producto["reference"].'<br>');
			echo('Puertas: '.$producto["puertas"].'<br>');
			
		}
		
		echo('--------------- Fin de listado ------------<br>');
		*/
		
	}
	
	
	
	
	
	
}
