<?php
		
		include(dirname(__FILE__).'/../../config/config.inc.php');
		include(dirname(__FILE__).'/../../init.php');

		$coches 			   = array();
		$modelos			   = array();
		$versiones			   = array();
		$carrocerias		   = array();
		$fechas_inicio		   = array();
		$fechas_fin			   = array();
		$productos 			   = array();
		
		$contadorCoches 		= 0;
		$contadorModelos 	    = 0;
		$contadorVersiones      = 0;
		$contadorCarrocerias    = 0;
		$contadorFechas_inicio  = 0;
		$contadorFechas_fin     = 0;
		$contadorProductos      = 0;
		
		
		if (isset($_GET["id_marca"])){

			$marca         = $_GET["id_marca"];
			
			$subcategorias = array();
			$contadorCategorias = 0;
			
			$categoria   = new Category((int)$marca,1,1);
			$menu_items  = $categoria->getCategoriesOrdenadoNombre(1,true,true,'AND c.id_parent='.(int)$marca,'ORDER BY cl.`name` ASC','');
			
			
			foreach ($menu_items[$categoria->id_category] as $item)
			{
			
				$idSeccion 						= $item['infos']['id_category'];
				$nombreSeccion 					= $item['infos']['name'];
				$link_rewrite					= $item['infos']['link_rewrite'];
				
				$subcategorias[$contadorCategorias]['id_category'] 					= $idSeccion;
				$subcategorias[$contadorCategorias]['name']  						= $nombreSeccion;
				$subcategorias[$contadorCategorias]['link_rewrite']  				= $link_rewrite;
				
				$contadorCategorias++;
			
			}
			
			
			$data = array(
			
					'id_marca'     => $_GET["id_marca"],
			
			);
			
			//var_dump($subcategorias);
			
			echo json_encode($subcategorias);
			
			
			
		}else if(isset($_GET["id_modelo"])){
			
			$nuevaCategoria = new Category((int)$_GET["id_modelo"],(int)Context::getContext()->language->id,1);
			
			$resultado = $nuevaCategoria->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 50000) );
			//var_dump($resultado);
			
			versiones($resultado);
			
			/*
			echo("-------- Listado de resultados ------------<br>");
			var_dump($resultado);
			echo("-------------------------------------------<br>");
			*/
			
			/*
			echo("-------- Listado de productos ------------<br>");
			var_dump($productos);
			echo("------------------------------------------<br>");
			*/
			
			
			
			for ($i=0;$i<count($productos);$i++){
				
				$productos[$i]['nombreModelo'] = $_GET["nombreModelo"];
				$productos[$i]['nombreMarca']  = $_GET["nombreMarca"];
				$productos[$i]['idModelo']     = $_GET["id_modelo"];
				$productos[$i]['idMarca']      = $_GET["idMarca"];
				
				
			
			}
			
			
			
			
			
			//var_dump($productos);
			
			echo json_encode($productos);
			
		}
		
		
		function versiones($vector){
		
			$contador   = 0;
			$vectorTemp = array();
			$encontrado = false;
			
			global $productos;
			
			for ($i=0;$i<count($vector);$i++){
		
				for ($n=0;$n<count($vectorTemp);$n++){
		
					if ($vector[$i]['version'] === $vectorTemp[$n]['version']){
		
						//echo ('Encontrado: '.$vector[$i]['version'].'<br>');
						$encontrado = true;
		
					}
		
				}
		
				if ($encontrado == false){
		
					$vectorTemp[count($vectorTemp)] = $vector[$i];
					//$productos[count($productos)] = $vector[$i];
		
				}
		
				$encontrado = false;
		
			}
		
			
			$encontrado = false;
			
			for ($m = 0;$m < count($vectorTemp);$m++){
			
				for ($p=0;$p < count($productos);$p++){
			
			
					if ($productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
			
						$encontrado = true;
			
					}// End if
			
			
				}// End for
			
			
				if ($encontrado == false){
			
					$productos[count($productos)] = $vectorTemp[$m];
					
					
				}
			
				$encontrado = false;
					
					
			}// End for
			
		
			/*
			echo('------------ Listado de versiones -----------<br>');
		
			foreach ($productos as $producto){
				
				echo('Referencia: '.$producto["reference"].'<br>');
				echo('Version: '.$producto["version"].'<br>');
				
			}
			
			echo('---------------------------------------------<br>');
			*/
		
			carroceria($vector);
		
		}
		
		function carroceria($vector){
		
			$contador   = 0;
			$vectorTemp = array();
			$encontrado = false;
		
			global $productos;
			
			for ($i=0;$i<count($vector);$i++){
					
				//var_dump($vector[$i]);
				//echo ('Valor mostrado: '.$vector[$i]['carroceria'].'<br>');
					
				for ($n=0;$n<count($vectorTemp);$n++){
		
					if ($vector[$i]['carroceria'] === $vectorTemp[$n]['carroceria']){
							
						//echo ('Encontrado: '.$vector[$i]['carroceria'].'<br>');
						$encontrado = true;
							
					}
		
				}
					
				if ($encontrado == false){
		
					$vectorTemp[count($vectorTemp)] = $vector[$i];
		
				}
					
				$encontrado = false;
					
			}
		
			$encontrado = false;
		
			for ($m = 0;$m < count($vectorTemp);$m++){
					
				for ($p=0;$p < count($productos);$p++){
		
		
					if ($productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
							
						$encontrado = true;
							
					}// End if
		
						
				}// End for
					
		
				if ($encontrado == false){
		
					$productos[count($productos)] = $vectorTemp[$m];
		
				}
					
				$encontrado = false;			
					
			}// End for
		

			/*
			echo('------------ Listado de carroceria -----------<br>');
		
			foreach ($productos as $producto){
				
				echo('Referencia: '.$producto["reference"].'<br>');
				echo('Carroceria: '.$producto["carroceria"].'<br>');
				
			}
			
			echo('---------------------------------------------<br>');
			*/
		
			fechaInicioFechaFin($vector);
		
		}
		
		
		function fechaInicioFechaFin($vector){
		
			$contador   = 0;
			$vectorTemp = array();
			$encontrado = false;
		
			global $productos;
			
			for ($i=0;$i<count($vector);$i++){
		
				//var_dump($vector[$i]);
				//echo ('Valor mostrado: '.$vector[$i]['carroceria'].'<br>');
		
				for ($n=0;$n<count($vectorTemp);$n++){
		
					if ($vector[$i]['fecha_inicio'] === $vectorTemp[$n]['fecha_inicio']){
		
						if ($vector[$i]['fecha_fin'] === $vectorTemp[$n]['fecha_fin']){
								
							//echo ('Encontrado: '.$vector[$i]['fecha_fin'].'<br>');
							$encontrado = true;
		
						}
							
					}
		
				}
		
				if ($encontrado == false){
		
					$vectorTemp[count($vectorTemp)] = $vector[$i];
		
				}
		
				$encontrado = false;
		
			}
		
			$encontrado = false;
		
			for ($m = 0;$m < count($vectorTemp);$m++){
					
				for ($p=0;$p < count($productos);$p++){
		
		
					if ($productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
							
						$encontrado = true;
							
					}// End if
		
						
				}// End for
					
		
				if ($encontrado == false){
		
					$productos[count($productos)] = $vectorTemp[$m];
		
				}
					
				$encontrado = false;	
				
				
			}// End for
		
		
			
			/*
			echo('------------ Listado de fechas -----------<br>');
		
			foreach ($productos as $producto){
				
				echo('Fecha inicio: '.$producto["fecha_inicio"].'<br>');
				echo('Fecha fin: '.$producto["fecha_fin"].'<br>');
				echo('Referencia: '.$producto["reference"].'<br>');
				echo('Version: '.$producto["version"].'<br>');
				
			}
			
			echo('---------------------------------------------<br>');
			*/
		
			puertas($vector);
			
		}
		
		function puertas($vector){
		
			$contador   = 0;
			$vectorTemp = array();
			$encontrado = false;
		
			global $productos;
			
			for ($i=0;$i<count($vector);$i++){
					
				//var_dump($vector[$i]);
				//echo ('Valor mostrado: '.$vector[$i]['carroceria'].'<br>');
					
				for ($n=0;$n<count($vectorTemp);$n++){
		
					if ($vector[$i]['puertas'] === $vectorTemp[$n]['puertas']){
							
						//echo ('Encontrado: '.$vector[$i]['puertas'].'<br>');
						$encontrado = true;
							
					}
		
				}
					
				if ($encontrado == false){
		
					$vectorTemp[count($vectorTemp)] = $vector[$i];
		
				}
					
				$encontrado = false;
					
			}
		
			$encontrado = false;
		
			for ($m = 0;$m < count($vectorTemp);$m++){
					
				for ($p=0;$p < count($productos);$p++){
		
		
					if ($productos[$p]['id_product'] === $vectorTemp[$m]['id_product']){
							
						$encontrado = true;
							
					}// End if
		
		
				}// End for
					
		
				if ($encontrado == false){
		
					$productos[count($productos)] = $vectorTemp[$m];
		
				}

				$encontrado = false;
				
					
			}// End for

			$encontrado = false;
			
			/*
			echo('------------ Listado de puertas -----------<br>');
		
			foreach ($productos as $producto){
				
				echo('Puertas: '.$producto["puertas"].'<br>');
				echo('Referencia: '.$producto["reference"].'<br>');
				echo('Version: '.$producto["version"].'<br>');
				
			}
			
			echo('---------------------------------------------<br>');
			*/
		
			//$this->fechaInicioFechaFin($vector);
			
		
		}
		
		
	
     

?>
