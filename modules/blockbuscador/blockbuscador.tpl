{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- Block manufacturers module -->


<script type="text/javascript">
	
	$(document).ready(function(){
		
		/*
		$("#busquedaAvanzada a").click(function(event){
    		event.preventDefault();
    		$("#cuadroBusquedaAvanzada").slideToggle("slow");
  		});
		*/
		
		$("#buscar,#submit_search").click(function(event){
		//$("#submit_search").click(function(event){
		
			if ( $("#retrovisores").prop("checked", true) ){
			
				event.preventDefault();
				
				var baseUri = '{$base_uri|addslashes}';
				
				var indiceMarca = $("#especialidades_list").val();
				
				if (indiceMarca != 0){
					
					var start_pos      = indiceMarca.indexOf('id_category') + 12;
			    	var end_pos 	   = indiceMarca.indexOf('&',start_pos);
			    	var idMarca        = indiceMarca.substring(start_pos,end_pos);	
				
				}
				
				
				var indiceModelo = $("#categorias_list").val();
				
				if (indiceModelo != 0){
					
					start_pos      = indiceModelo.indexOf('id_category') + 12;
			    	end_pos 	   = indiceModelo.indexOf('&',start_pos);
			    	var idModelo   = indiceModelo.substring(start_pos,end_pos);	
					
				}
				
				
				var indiceVersion = $("#subcategorias_list").val();
				
				if (indiceVersion != 0){
					
					start_pos       = indiceVersion.indexOf('id_category') + 12;
			    	end_pos 	    = indiceVersion.indexOf('&',start_pos);
			    	var idVersion   = indiceVersion.substring(start_pos,end_pos);	
					var url			= baseUri + "?controller=category";
					
					var link = indiceVersion.split("&");
					
					for (index = 0; index < link.length; index++) {
	  		  		
	  		  			url = url + "&" + link[index];
						
					} 
					
					var indiceDespiece    = $("#despiece").val();
					var indiceRegulacion  = $("#regulacion").val();
					var indiceCristal     = $("#cristal").val();
					var indiceMano 		  = $("#mano").val();
					
					if (indiceDespiece != 0){
						
						url = url + "&despiece="+indiceDespiece;
						
					}
					
					if (indiceRegulacion != 0){
						
						url = url + "&regulacion="+indiceRegulacion;
						
					}
					
					if (indiceCristal != 0){
						
						url = url + "&cristal="+indiceCristal;
						
					}
					
					if (indiceMano != 0){
						
						url = url + "&mano="+indiceMano;
						
					}
				
					document.location = url;
					
				}else if(indiceModelo != 0){
				
					var url				 = baseUri + "?controller=category&id_category="+idModelo;
					var indiceDespiece   = $("#despiece").val();
					var indiceRegulacion = $("#regulacion").val();
					var indiceCristal    = $("#cristal").val();
					var indiceMano 		 = $("#mano").val();
					
					
					if (indiceDespiece != 0){
						
						url = url + "&despiece="+indiceDespiece;
						
					}
					
					if (indiceRegulacion != 0){
						
						url = url + "&regulacion="+indiceRegulacion;
						
					}
					
					if (indiceCristal != 0){
						
						url = url + "&cristal="+indiceCristal;
						
					}
					
					if (indiceMano != 0){
						
						url = url + "&mano="+indiceMano;
						
					}
					
					
					document.location = url;
				
				}else if(indiceMarca != 0){
				
					var url				  = baseUri + "?controller=category&id_category="+idMarca;
					var indiceDespiece    = $("#despiece").val();
					var indiceRegulacion  = $("#regulacion").val();
					var indiceCristal     = $("#cristal").val();
					var indiceMano 		  = $("#mano").val();
				
					if (indiceDespiece != 0){
						
						url = url + "&despiece="+indiceDespiece;
						
					}
					
					if (indiceRegulacion != 0){
						
						url = url + "&regulacion="+indiceRegulacion;
						
					}
					
					if (indiceCristal != 0){
						
						url = url + "&cristal="+indiceCristal;
						
					}
					
					if (indiceMano != 0){
						
						url = url + "&mano="+indiceMano;
						
					}
					
					
					document.location = url;
					
				}
			
			}
					
		});
		
		$(".btncontactarpieza").click(function(){
			
			direccion = "{$link->getPageLink('contact', true)}";
			document.location = direccion;
		
		});
	
		$("#retrovisores").click(function(){
		
			$("#retrovisores").prop("checked", true);
			$("#iluminacion").prop("checked", false);
			$("#derivabrisas").prop("checked", false);
			$("#accesorios").prop("checked", false);	
	
			$("#cuadroBusquedaAvanzada").slideDown("slow");
			
			
		});
	
		$("#iluminacion").click(function(){
			
			$("#retrovisores").prop("checked", false);
			$("#iluminacion").prop("checked", true);
			$("#derivabrisas").prop("checked", false);
			$("#accesorios").prop("checked", false);
		
		
			$("#cuadroBusquedaAvanzada").slideUp("slow");
		
		});
		
		$("#derivabrisas").click(function(){
			
			$("#retrovisores").prop("checked", false);
			$("#iluminacion").prop("checked", false);
			$("#derivabrisas").prop("checked", true);
			$("#accesorios").prop("checked", false);
		
			$("#cuadroBusquedaAvanzada").slideUp("slow");
		
		});
		
		$("#accesorios").click(function(){
			
			$("#retrovisores").prop("checked", false);
			$("#iluminacion").prop("checked", false);
			$("#derivabrisas").prop("checked", false);
			$("#accesorios").prop("checked", true);
		
			$("#cuadroBusquedaAvanzada").slideUp("slow");
		
		});
	
		
	
	});
	
	function showUser(str,id) {
  		
  		if (id == "especialidades_list"){
  		
  		  //document.location = str;	
  				
	  	  var start_pos    = str.indexOf('id_category') + 12;
		  var end_pos 	   = str.indexOf('&',start_pos);
		  var $text_to_get = str.substring(start_pos,end_pos);	
		 	
	  	  $.ajax({
	          
	          url: '{$base_dir}modules/blockbuscador/ajax-call.php',
	          type: 'GET',
	          data: {
	          	id_marca:$text_to_get
	          	
	          },
	          
		  	  dataType: 'JSON',
	          
	          success: function(data) {
	               
	              //console.log('success');
	               
	               $('#categorias_list').empty();
	               
	               $("#categorias_list").append("<option value=0>Todos los modelos</option>");
	               
	               {ldelim}
	               
	  			   $.each(data,function(i) {
	                                        
				        id_categoria    = parseInt(data[i].id_category);
				        name 			= data[i].name;
				        link			= data[i].link_rewrite;
				     	
				        url 			= "{$link->getCategoryLink(id_categoria,link)|escape:'htmlall':'UTF-8'}"+"&id_category="+id_categoria+"&";
				       
				        $("#categorias_list").append("<option value="+url+">"+data[i].name+"</option>");
				   		
				   
				   });
				   
				   {rdelim} 
	          
	          }
	          
	      });	
  		  		
  	
  	 }else if (id == "categorias_list"){ // Else
  	   	 
  	   	var str2		   = $("#especialidades_list option:selected").val();	 
  	   	var start_pos      = str2.indexOf('id_category') + 12;
		var end_pos 	   = str2.indexOf('&',start_pos);
		var $text_to_get   = str2.substring(start_pos,end_pos);	 
  	   	
		start_pos      = str.indexOf('id_category') + 12;
		end_pos 	   = str.indexOf('&',start_pos);
		var idModelo   = str.substring(start_pos,end_pos);	
				
  	 	$.ajax({
	          
	          url: '{$base_dir}modules/blockbuscador/ajax-call.php',
	          type: 'GET',
	          data: {
	          	id_modelo:idModelo,
	      		idMarca:$text_to_get,
	      		nombreModelo:$("#categorias_list option:selected").text(),    	
	          	nombreMarca:$("#especialidades_list option:selected").text()
	          	
	          },
	          
		  	  dataType: 'JSON',
	          
	          success: function(data) {
	               
	               $('#subcategorias_list').empty();
	               
	               $("#subcategorias_list").append("<option value=0>Todas las versiones</option>");
	               
	               var url   = "";
	               var valor = "";
	                
	  			   $.each(data,function(i) {
	                    
	                    url   = "id_category="+data[i].idModelo;
	                    valor = data[i].nombreMarca+"  -  "+data[i].nombreModelo;
	                    
	                    console.log(data[i].version,data[i].puertas,data[i].carroceria,data[i].fecha_inicio,data[i].fecha_fin);
	                    
	                    if (data[i].version != "" && data[i].version != undefined){
	                    
	                    	url   = url + "&version="+data[i].version;
							valor = valor + " ("+data[i].version+")";
								                    
	                    }
	                    
	                    if (data[i].puertas != "" && data[i].puertas != undefined){
	                    
	                    	url = url + "&puertas="+data[i].puertas;
	                    	valor = valor + " (Puertas:"+data[i].puertas+")";
	                    
	                    }
	                    
	                    if (data[i].carroceria != "" && data[i].carroceria != undefined){
	                    
	                    	url = url + "&carroceria="+data[i].carroceria;
	                    	valor = valor + " ("+data[i].carroceria+")";
	                    
	                    }
	                    
	                 
	                    if (data[i].fecha_inicio != "" && data[i].fecha_inicio != undefined){
	                    
	                    	url = url + "&fecha_inicio="+data[i].fecha_inicio;
	                    	valor = valor + " ("+data[i].fecha_inicio+" - ";
	                    
	                    }
	                    
	                 	if (data[i].fecha_fin != "" && data[i].fecha_fin != undefined){
	                    
	                    	url = url + "&fecha_fin="+data[i].fecha_fin;
	                    	valor = valor + data[i].fecha_fin+")";
	                    
	                    }else if(data[i].fecha_inicio != "" && data[i].fecha_inicio != undefined){
	                    
	                    	var currentYear = (new Date).getFullYear();
	                    	url = url + "&fecha_fin="+currentYear;
	                    	valor = valor + currentYear+")";
	                    	
	                    
	                    }
	                 	
	                 	   
	                    $("#subcategorias_list").append("<option value="+url+">"+valor+"</option>");
	                   
				   
				   });
				   
	          
	          }
	          
	      });	
  	 
  	 
  	 }else if (id == "subcategorias_list"){ // Else
  	
	
			
	
	
	}
	
  }

</script>

<form name="formularioBuscador" method="post" action="">

<div id="buscador">
		
		<div id="buscadorCategorias">	
			
			<div class="numberCircle">1</div><h1>Identifica tu vehiculo:</h1>
			
			<div id="buscadorIzquierda">
				
				<p>Marcas:</p>
				<p>Modelos:</p>
				<p>Versiones:</p>
			
			</div>
			
			<div id="buscadorDerecha">
				
				<select id="especialidades_list" onchange="showUser(this.value,this.id);">
				
					<option value="0" selected="selected">Todas las marcas</option>
					
					
					{if count($especialidades) > 0}
						
						
						{foreach from=$especialidades key=myId item=i}
							
							
							
							{if (int)$idEspecialidad == (int)$i.id_category}
							
								<option value="{$link->getCategoryLink($i.id_category, $i.link_rewrite)|escape:'htmlall':'UTF-8'}&id_prueba={$myId}" selected="selected">{$i.name|escape:'htmlall':'UTF-8'}</option>
							
							{else}
							
								<option value="{$link->getCategoryLink($i.id_category, $i.link_rewrite)|escape:'htmlall':'UTF-8'}&id_prueba={$myId}">{$i.name|escape:'htmlall':'UTF-8'}</option>	
							
							{/if}
							
						{/foreach}
						
						
					{/if}
					
				
				</select>	
				
				
				<select id="categorias_list" onchange="showUser(this.value,this.id);">
				
					<option value="0" selected="selected">Todos los modelos</option>
					
					{if count($categorias)>0}	
						{foreach from=$categorias key=myId item=i}
							{if (int)$idCategoria == (int)$i.id_category}
								<option value="{$link->getCategoryLink($i.id_category, $i.link_rewrite)|escape:'htmlall':'UTF-8'}" selected = "selected">{$i.name|escape:'htmlall':'UTF-8'}</option>
							{else}
								<option value="{$link->getCategoryLink($i.id_category, $i.link_rewrite)|escape:'htmlall':'UTF-8'}">{$i.name|escape:'htmlall':'UTF-8'}</option>	
							{/if}
						{/foreach}
					{/if}
				
				</select>	
				
				<select id="subcategorias_list" onchange="showUser(this.value,this.id);">
						<option value="0" selected="selected">Todas las versiones</option>
						{if count($subcategorias)>0}
							{foreach from=$subcategorias key=myId2 item=i2}
								{if (int)$idSubcategoria == (int)$i2.id_category}
									<option value="{$link->getCategoryLink($i2.id_category, $i2.link_rewrite)|escape:'htmlall':'UTF-8'}" selected = "selected">{$i2.name|escape:'htmlall':'UTF-8'}</option>	
								{else}
									<option value="{$link->getCategoryLink($i2.id_category, $i2.link_rewrite)|escape:'htmlall':'UTF-8'}">{$i2.name|escape:'htmlall':'UTF-8'}</option>
								{/if}
							{/foreach}
						{/if}
				</select>
				
				<input type="submit" id="submit_search" name="submit_search" class="buscarAvanzadaButton2" value="Buscar" />	
			
			</div>
			
			<div id="coche"></div>	
			
			
				
		</div>
		
		
		<!--
		<div id="buscadorPiezas">
		
			<div class="numberCircle">2</div><h1>Identifica tus recambios:</h1>
			
			
			<div id="buscadorPiezasIzquierda">
				
				<input type="radio" name="retrovisores" id="retrovisores" checked>
				<input type="radio" name="iluminacion" id="iluminacion">
				<input type="radio" name="derivabrisas" id="derivabrisas">
				<input type="radio" name="accesorios" id="accesorios"> 
				 
			
			</div>
			
			
			<div id="buscadorPiezasDerecha">
				
				<p>Retrovisores:</p>
				<p>Iluminaci&oacute;n:</p>
				<p>Derivabrisas:</p>
				<p>Accesorios:</p>
			
			</div>
			
			<div id="llave"></div>
			
		</div>
		-->
		
		
		<div id="contactarPieza">
		
			<div id="esquina"></div>
		
			<h1>Conoces la pieza que estas buscando ?</h1>
				
			<input type="button" class="btncontactarpieza" value="Contactar">
		
		</div>
		
		<div id="buscarPieza">
		
			<form name="buscarPieza" method="get" action="{$link->getPageLink('search')|escape:'html'}" id="searchbox">
		
				<div id="esquina2"></div>
			
				<h1>Conoces la referencia de la pieza que estas buscando ?</h1>
				
				<input type="hidden" name="controller" value="search" />
				
				<input type="hidden" name="orderby" value="position" />
				
				<input type="hidden" name="orderway" value="desc" />
				
				<input class="search_query" type="text" id="search_query_top" name="search_query" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" style="float:left;height:30px;margin-left:3px;width:110px;padding-left:5px;">
				
				<input type="submit" name="submit_search" class="btnbuscarpieza" value="OK" />
		
		
			</form>
			
		
		</div>
		
		
		<div id="busquedaAvanzada">
		
			<p><a href="#">B&uacute;squeda Avanzada</a></p>
		
			<div id="cuadroBusquedaAvanzada">
				
				<p>Despiece:</p>
				
				<select name="despiece" id="despiece" class="select">
					<option value="0">Lista de despieces</option>
					{foreach from=$despiece key=myId item=i}
						<option value="{$i.id}">{$i.nombre}</option>
					{/foreach}
				</select>
				
				<p>Regulacion:</p>
				
				<select name="regulacion" id="regulacion" class="select">
					<option value="0">Lista de regulaciones</option>
					{foreach from=$regulacion key=myId item=i}
						<option value="{$i.id}">{$i.nombre}</option>
					{/foreach}
				</select>
				
				<p>Cristal:</p>
				
				<select name="cristal" id="cristal" class="select">
					<option value="0">Lista de cristales</option>
					{foreach from=$cristal key=myId item=i}
						<option value="{$i.id}">{$i.nombre}</option>
					{/foreach}
				</select>
				
				<p>Mano:</p>
				
				<select name="mano" id="mano" class="select">
					<option value="0">Lista de manos</option>
					{foreach from=$mano key=myId item=i}
						<option value="{$i.id}">{$i.nombre}</option>
					{/foreach}
				</select>
				
				
				<!-- <p>Caracter&iacute;sticas:<input type="checkbox" name="caracteristicas" id="caracteristicas"></input></p> -->
				
				<p><input type="submit" class="buscarAvanzadaButton" value="Buscar" id="buscar"></p>
				
			</div>
		
		</div>
		
</div>

</form>




<!-- /Block manufacturers module -->
