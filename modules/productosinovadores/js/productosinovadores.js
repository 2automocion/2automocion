jQuery(document).ready(function($){

	// ************************************   Nuestro Objeto   ****************************** */
	var objetoInovadores = {
		
		objetoGaleria 	:$('#mygalleryInovadores'),
		objetoBelt	  	:$('.beltInovadores'),
		rightBoton		:$('#previnovadores'),
		leftBoton		:$('#nextinovadores'),
		totalImagenes	:0,
		posImagen  :0,
		diferencia:0,
		ancho:253, 
		
		crearEventos:function(){
			
			// Contador de elementos destacados
			$(".beltInovadores li").each(function(index) {
				
				objetoInovadores.totalImagenes++;	
						
			});
			
			objetoInovadores.diferencia = objetoInovadores.totalImagenes - 3;
			
			if (objetoInovadores.totalImagenes > 3){
			
				objetoInovadores.rightBoton.click(function(e){
						
						e.preventDefault();
						objetoInovadores.evaluar(2);
						 
						
				})
					
					
				objetoInovadores.leftBoton.click(function(e){
						
						e.preventDefault(); 
						objetoInovadores.evaluar(1);
						
						
				})
			
			
			}// End if
			
			
		},
		
		evaluar:function(param){
				
			if (param == 1 && objetoInovadores.posImagen < objetoInovadores.diferencia){
				
				var resultado =  ( (objetoInovadores.ancho) * (objetoInovadores.posImagen+1) ) * -1;
				objetoInovadores.posImagen++;
			
			}	
			
			if (param == 2 && objetoInovadores.posImagen > 0){
				
				var resultado =  ( (objetoInovadores.ancho) * (objetoInovadores.posImagen-1) ) * -1;
				objetoInovadores.posImagen--;
				
			}
			
			
			
			objetoInovadores.mover(resultado);
			
		},
		
		mover:function(param1){
			
			objetoInovadores.objetoBelt.animate({
				 
				 left: param1+'px',
				 }, 400, function() {
				    // Animation complete.
					
			});	
			
		}
		
	}// Fin de objeto
	
	objetoInovadores.crearEventos();
	
	
});