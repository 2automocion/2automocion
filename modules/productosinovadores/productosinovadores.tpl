{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
	
	<div class="contenedorInovadores">
		
		<div class="header">
		
			<div class="newproduct-nav">
				<a id="previnovadores" class="prev" href="#">Atras</a>
				<a id="nextinovadores" class="next" href="#">Adelante</a>
			</div>
			
			<h4 class="newproduct-title">PRODUCTOS DESTACADOS<span id="contadorInovadores">&nbsp;({count($productos)})</span></h4>
		
		</div>
		
		<div class="contenedorInovadores1">
	
			<div id="mygalleryInovadores" class="stepcarouselInovadores">
		
				<div class="beltInovadores">
				
					<ul id="ulInovadores">
						
						{foreach from=$productos item=product}    
						
						<li>
							
							<div class="articulo">
									
									<h5 class="entry-title">
										<a href="{$product.link|escape:'htmlall':'UTF-8'}">{$product.name}</a>
									</h5>
									
									
									<div class="imagenInovador">
										
										<a href="{$product.link|escape:'htmlall':'UTF-8'}" title="{$product.name|escape:html:'UTF-8'}" class="product_image">
											
											<img class="responsive-img" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html'}" alt="{$product.name}"/>
																			 	
										</a>
									
									</div>
									
									<p class="product_descInovador">{$product.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</p>
									
									<p class="precio">
										<span class="price lof-price">{convertPrice price=$product.price}&nbsp;&euro;</span>
									</p>
										
									{if !$PS_CATALOG_MODE && is_array($item.specific_prices) AND ($priceSpecial  eq '1') AND !isset($restricted_country_mode) && $item.show_price}
										
										<div class="entry-price-discount">
											{displayWtPrice p=$item.price_without_reduction}
										</div>
										
									{/if}
									
									<div class="boton">
										
										{if ($item.id_product_attribute == 0 OR (isset($add_prod_display) AND ($add_prod_display == 1))) AND $item.available_for_order AND !isset($restricted_country_mode) AND $item.minimal_quantity == 1 AND $item.customizable != 2 AND !$PS_CATALOG_MODE}
		
											{if (($item.quantity > 0 OR $item.allow_oosp))}
											
												<a class="lof-add-cart ajax_add_to_cart_button" rel="ajax_id_product_{$product.id_product}" href="{$site_url}cart.php?add&amp;id_product={$product.id_product}&amp;token={$token}"><span>{l s='Add to cart' mod='lofnewproduct'}</span></a>
											
											{else}
											
												<span class="lof-add-cart">{l s='Add to cart' mod='lofnewproduct'}</span></a>
											
											{/if}
		
										{/if}
									
										<a href="{$product.link|escape:'htmlall':'UTF-8'}" class="botonInovadores">VER M&Aacute;S</a>
									
									</div>
									
									
									
							</div>
											
						</li>
					
						{/foreach}
					
					</ul>		
			
		</div>

	</div>

</div>

</div>
