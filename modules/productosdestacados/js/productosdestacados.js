jQuery(document).ready(function($){

	// ************************************   Nuestro Objeto   ****************************** */
	var objetoOfertas = {
	
		objetoBelt	  	:$('.beltOfertas'),
		rightBoton		:$('#prevOferta'),
		leftBoton		:$('#nextOferta'),
		totalImagenes	:0,
		posImagen  :0,
		diferencia:0,
		ancho:253, 
		
		crearEventos:function(){
			
			// Contador de elementos destacados
			$(".beltOfertas li").each(function(index) {
				
				objetoOfertas.totalImagenes++;	
						
			});
			
			objetoOfertas.diferencia = objetoOfertas.totalImagenes - 3;
			
			if (objetoOfertas.totalImagenes > 3){
			
				objetoOfertas.rightBoton.click(function(e){
						
						objetoOfertas.evaluar(2);
						e.preventDefault(); 
						
				})
					
					
				objetoOfertas.leftBoton.click(function(e){
						
						
						objetoOfertas.evaluar(1);
						e.preventDefault(); 
						
				})
			
			
			}// End if
			
			
		},
		
		evaluar:function(param){
				
			if (param == 1 && objetoOfertas.posImagen < objetoOfertas.diferencia){
				
				var resultado =  ( (objetoOfertas.ancho) * (objetoOfertas.posImagen+1) ) * -1;
				objetoOfertas.posImagen++;
			
			}	
			
			if (param == 2 && objetoOfertas.posImagen > 0){
				
				var resultado =  ( (objetoOfertas.ancho) * (objetoOfertas.posImagen-1) ) * -1;
				objetoOfertas.posImagen--;
				
			}
			
			
			
			objetoOfertas.mover(resultado);
			
		},
		
		mover:function(param1){
			
			objetoOfertas.objetoBelt.animate({
				 
				 left: param1+'px',
				 }, 400, function() {
				    // Animation complete.
					
			});	
			
		}
		
	}// Fin de objeto
	
	objetoOfertas.crearEventos();
	
	
});