<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.
include_once(_PS_ROOT_DIR_ .'/modules/agilemultipleseller/SellerInfo.php');
if(Module::isInstalled('agilemultipleshop'))
	include_once(_PS_ROOT_DIR_ .'/modules/agilemultipleshop/SellerType.php');

class AgileMultipleSellerSellerBusinessModuleFrontController extends AgileModuleFrontController
{
	public function setMedia()
	{
		parent::setMedia();

		$this->addJS(_THEME_JS_DIR_.'tools/statesManagement.js');
	}
	
	
	public function init()
	{
		parent::init();

		$deflang = new Language(self::$cookie->id_lang);
		$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$deflang->iso_code.'.js') ? $deflang->iso_code : 'en');
		$ad = str_replace("\\", "\\\\", dirname($_SERVER["PHP_SELF"]));		$languages = Language::getLanguages(false);
		
		$sellermodule = new AgileMultipleSeller();
		$conf = Configuration::getMultiple($sellermodule->getCustomFields());
		$custom_labels = $sellermodule->getCustomLabels(':');
		$custom_multi_lang_fields = SellerInfo::getCustomMultiLanguageFields();
		$str_custom_multi_lang_fields = "";
		foreach ($custom_multi_lang_fields as $custom_multi_lang_field)
		{
			$str_custom_multi_lang_fields .= '&curren;'.$custom_multi_lang_field;
		}
			
				self::$smarty->assign(array(
			'seller_tab_id' => 2
			,'ad' => $ad
			,'isoTinyMCE' => $isoTinyMCE
			,'theme_css_dir' => _THEME_CSS_DIR_
			,'languages' => $languages
			,'current_id_lang' => self::$cookie->id_lang
			,'conf' => $conf
			,'custom_labels' => $custom_labels
			,'str_custom_multi_lang_fields' => $str_custom_multi_lang_fields
			,'shop_url_mode' => (int)Configuration::get('ASP_SHOP_URL_MODE')
			));
	}

	
	public function initContent()
	{
		parent::initContent();
		
		$default_shop = new Shop( Configuration::get('PS_SHOP_DEFAULT'));
		$seller_shop = new Shop($this->sellerinfo->id_shop); 
		$seller_shopurl = new ShopUrl(Shop::get_main_url_id($seller_shop->id));

		
		self::$smarty->assign(array(		
			'default_shop' => $default_shop
			,'seller_shop'=>$seller_shop
			,'seller_shopurl' => $seller_shopurl 
			,'sellertypes' => (Module::isINstalled('agilemultipleshop')? SellerType::getSellerTypes($this->context->language->id,  $this->l('Please choose seller type')) : array())
			));

		$this->assignCountries();
		$this->assignVatNumber();
		$this->assignAddressFormat();

		$this->setTemplate('sellerbusiness.tpl');
	}
	
	public function postProcess()
	{
		if (Tools::isSubmit('submitSellerinfo'))
			$this->processSubmitSellerinfo();
	}
	
	
		protected function assignCountries()
	{
				if (Tools::isSubmit('id_country') && !is_null(Tools::getValue('id_country')) && is_numeric(Tools::getValue('id_country')))
			$selected_country = (int)Tools::getValue('id_country');
		else if (isset($this->sellerinfo) && isset($this->sellerinfo->id_country) && !empty($this->sellerinfo->id_country) && is_numeric($this->sellerinfo->id_country))
			$selected_country = (int)$this->sellerinfo->id_country;
		else if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']))
		{
			$array = preg_split('/,|-/', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
			if (!Validate::isLanguageIsoCode($array[0]) || !($selected_country = Country::getByIso($array[0])))
				$selected_country = (int)Configuration::get('PS_COUNTRY_DEFAULT');
		}
		else
			$selected_country = (int)Configuration::get('PS_COUNTRY_DEFAULT');

				if (Configuration::get('PS_RESTRICT_DELIVERED_COUNTRIES'))
			$countries = Carrier::getDeliveredCountries($this->context->language->id, true, true);
		else
			$countries = Country::getCountries($this->context->language->id, true);

				$list = '';
		foreach ($countries as $country)
		{
			$selected = ($country['id_country'] == $selected_country) ? 'selected="selected"' : '';
			$list .= '<option value="'.(int)$country['id_country'].'" '.$selected.'>'.htmlentities($country['name'], ENT_COMPAT, 'UTF-8').'</option>';
		}

				$this->context->smarty->assign(array(
			'countries_list' => $list,
			'countries' => $countries,
			));
	}

		protected function assignAddressFormat()
	{
		$id_country = is_null($this->sellerinfo)? 0 : (int)$this->sellerinfo->id_country;
		$dlv_adr_fields = AddressFormat::getOrderedAddressFields($id_country, true, true);
		$this->context->smarty->assign('ordered_adr_fields', $dlv_adr_fields);
	}

	
	protected function assignVatNumber()
	{
		$vat_number_exists = file_exists(_PS_MODULE_DIR_.'vatnumber/vatnumber.php');
		$vat_number_management = Configuration::get('VATNUMBER_MANAGEMENT');
		if ($vat_number_management && $vat_number_exists)
			include_once(_PS_MODULE_DIR_.'vatnumber/vatnumber.php');

		if ($vat_number_management && $vat_number_exists && VatNumber::isApplicable(Configuration::get('PS_COUNTRY_DEFAULT')))
			$vat_display = 2;
		else if ($vat_number_management)
			$vat_display = 1;
		else
			$vat_display = 0;

		$this->context->smarty->assign(array(
			'vatnumber_ajax_call' => file_exists(_PS_MODULE_DIR_.'vatnumber/ajax.php'),
			'vat_display' => $vat_display,
			));
	}


	protected function processSubmitSellerinfo()
	{
				AgileMultipleSeller::ensure_date_custom_field();
		
				$shop_name = '';
		if(isset($_POST['shop_name'])) $shop_name = trim($_POST['shop_name']," ");
		if(isset($_POST['virtual_uri']))
			$virtual_uri = Tools::link_rewrite(trim($_POST['virtual_uri']," /")) . "/";

		if(empty($_POST['postcode']))
			$this->errors[] = Tools::displayError('Postcode is required field.');
		
		$this->errors = array_merge($this->errors, $this->sellerinfo->validateController());

		$this->sellerinfo->id_customer = self::$cookie->id_customer;
		if(Module::isInstalled('agilemultipleshop'))
		{
			if(empty($shop_name))
				$this->errors[] = Tools::displayError('The shop name can not be empty.');

			if(empty($_POST['virtual_uri']) AND (int)Configuration::get('ASP_SHOP_URL_MODE') == agilemultipleshop::SHOP_URL_MODE_VIRTUAL)
				$this->errors[] = Tools::displayError('The shop Virtual Uri can not be empty.');

						if($this->sellerinfo->id_shop <=1)$this->sellerinfo->id_shop = 0;
			
			$seller_shop = new Shop($this->sellerinfo->id_shop); 
			if(Shop::shop_name_duplicated($shop_name, $seller_shop->id))
				$this->errors[] = Tools::displayError('The shop name you select has been used by other seller. Please choose a new one.');

			if($this->errors)return;
			if(!Validate::isLoadedObject($seller_shop))
			{
				$this->sellerinfo->id_shop = AgileMultipleShop::create_new_shop($this->sellerinfo->id_seller, $shop_name);
				$this->sellerinfo->update();
				$seller_shop = new Shop($this->sellerinfo->id_shop);
			}
			
			$seller_shopurl = new ShopUrl(Shop::get_main_url_id($seller_shop->id));
			$id_found = $seller_shopurl->canAddThisUrl($seller_shopurl->domain,$seller_shopurl->domain_ssl,$seller_shopurl->physical_uri, $virtual_uri);
			if(intval($id_found)>0)
				$this->errors[] = Tools::displayError('The uri you select has been used by other seller. Please choose a new one.');
		}
				if (!($country = new Country($this->sellerinfo->id_country)) || !Validate::isLoadedObject($country))
			throw new PrestaShopException('Country cannot be loaded with address->id_country');

		if ((int)$country->contains_states && !(int)$this->sellerinfo->id_state)
			$this->errors[] = Tools::displayError('This country requires a state selection.');

				if ($this->sellerinfo->id_country == Country::getByIso('US'))
		{
			$languages = Language::getLanguages(false);
			foreach($languages as $language)
			{
				if($language['language_code'] == 'en-us')
				{
					$us_id_lang = $language['id_lang'];
					break;
				}
			}
			if(isset($us_id_lang))
			{
				include_once(_PS_TAASC_PATH_.'AddressStandardizationSolution.php');
				$normalize = new AddressStandardizationSolution;
				$this->sellerinfo->address1[$us_id_lang] = $normalize->AddressLineStandardization($this->sellerinfo->address1[$us_id_lang]);
				$this->sellerinfo->address2[$us_id_lang] = $normalize->AddressLineStandardization($this->sellerinfo->address2[$us_id_lang]);
			}
		}

				$zip_code_format = $country->zip_code_format;
		if ($country->need_zip_code)
		{
			if (($postcode = Tools::getValue('postcode')) && $zip_code_format)
			{
				$zip_regexp = '/^'.$zip_code_format.'$/ui';
				$zip_regexp = str_replace(' ', '( |)', $zip_regexp);
				$zip_regexp = str_replace('-', '(-|)', $zip_regexp);
				$zip_regexp = str_replace('N', '[0-9]', $zip_regexp);
				$zip_regexp = str_replace('L', '[a-zA-Z]', $zip_regexp);
				$zip_regexp = str_replace('C', $country->iso_code, $zip_regexp);
				if (!preg_match($zip_regexp, $postcode))
					$this->errors[] = '<strong>'.Tools::displayError('Zip / Postal code').'</strong> '
						.Tools::displayError('is invalid.').'<br />'.Tools::displayError('Must be typed as follows:')
						.' '.str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $zip_code_format)));
			}
			else if ($zip_code_format)
				$this->errors[] = '<strong>'.Tools::displayError('Zip / Postal code').'</strong> '.Tools::displayError('is required.');
			else if ($postcode && !preg_match('/^[0-9a-zA-Z -]{4,9}$/ui', $postcode))
				$this->errors[] = '<strong>'.Tools::displayError('Zip / Postal code').'</strong> '.Tools::displayError('is invalid.')
					.'<br />'.Tools::displayError('Must be typed as follows:').' '
					.str_replace('C', $country->iso_code, str_replace('N', '0', str_replace('L', 'A', $zip_code_format)));
		}

				if ($country->isNeedDni() && (!Tools::getValue('dni') || !Validate::isDniLite(Tools::getValue('dni'))))
			$this->errors[] = Tools::displayError('Identification number is incorrect or has already been used.');

		$this->sellerinfo->dni = Tools::getValue('dni');
		
		$this->sellerinfo->latitude = Tools::getValue('latitude');
		$this->sellerinfo->longitude = Tools::getValue('longitude');

		$this->sellerinfo->id_sellertype1 = Tools::getValue('id_sellertype1');
		$this->sellerinfo->id_sellertype2 = Tools::getValue('id_sellertype2');


		SellerInfo::processLogoUpload($this->sellerinfo);	

		$this->errors = array_merge($this->errors, $this->sellerinfo->validateController());
		
		if (!empty($this->errors))
			return;

		$this->sellerinfo->save();
		
		if(Module::isInstalled('agilemultipleshop') AND Validate::isLoadedObject($seller_shop))
		{
			$seller_shop->name = $shop_name;
			$seller_shop->save();
			$seller_shopurl->virtual_uri = $virtual_uri;
			$seller_shopurl->save();
			Tools::generateHtaccess();
		}
		
		if(empty($this->errors))self::$smarty->assign('cfmmsg_flag',1);
			}

}