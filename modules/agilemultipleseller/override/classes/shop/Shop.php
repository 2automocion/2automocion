<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.

class Shop extends ShopCore
{
	
		public static $id_shop_owner =0;
		public static $id_shop_virtual =0;
		public static $sellerinfo;
	
		public static function getContextShopID($null_value_without_multishop = false)
	{
		self::$context_id_shop = 1;
		return self::$context_id_shop;
	}	

	public static function isFeatureActive()
	{
		$isFeatureActive = parent::isFeatureActive();
				if(!Module::isInstalled('agilemultipleseller'))return $isFeatureActive;
		
				if($isFeatureActive)Configuration::updateGlobalValue('PS_MULTISHOP_FEATURE_ACTIVE' ,0);	
		return false;
		
	}
		public static function initialize()
	{		
		global $cookie;
		
		$shop = parent::initialize();		

		if(Module::isInstalled('agilemultipleseller') AND Module::isInstalled('agilemultipleshop') AND $shop->id > 1)
		{
			include_once(_PS_ROOT_DIR_ . "/modules/agilemultipleseller/SellerInfo.php");
			include_once(_PS_ROOT_DIR_ . "/modules/agilemultipleshop/agilemultipleshop.php");

			self::$id_shop_owner = AgileSellerManager::getSellerIdByShopId($shop->id);
			
			if((int)Configuration::get('ASP_SHOP_URL_MODE') == agilemultipleshop::SHOP_URL_MODE_VIRTUAL)
				self::check_redirect_seller_home($shop, self::$id_shop_owner);

			self::$id_shop_virtual = $shop->id;
			self::$sellerinfo = new SellerInfo(SellerInfo::getIdBSellerId(self::$id_shop_owner));
			$shop->id = Configuration::get('PS_SHOP_DEFAULT');
			self::$context_id_shop = $shop->id;
		}

		return $shop;
	}
	
	public static function addSqlAssociation($table, $alias, $inner_join = true, $on = null, $force_not_default = false)
	{
		$sql = 	parent::addSqlAssociation($table, $alias, $inner_join, $on, $force_not_default);
		if(!Module::isInstalled('agilemultipleshop'))return $sql;

		$eaccess = AgileSellerManager::get_entity_access($table);
		if($eaccess['owner_table_type'] == AgileSellerManager::OWNER_TABLE_UNKNOWN)return $sql;

		$xr_table = $eaccess['owner_xr_table'];
		
				$include_shared = '';
		if(!$eaccess['is_exclusive'])$include_shared = ',0';

		if(Shop::$id_shop_owner >0)
		{
			if($eaccess['owner_table_type'] == AgileSellerManager::OWNER_TABLE_DEFINED)
				$sql .= 'INNER JOIN ' . _DB_PREFIX_ . $table . '_owner ' . $table . '_owner ON (' . $alias . '.id_' . $table . '=' . $table . '_owner.id_' . $table . ' AND IFNULL(' . $table . '_owner.id_owner,0) IN (' . Shop::$id_shop_owner . $include_shared . '))';
			else
				$sql .= 'INNER JOIN ' . _DB_PREFIX_ . 'object_owner ' . $table . '_object_owner ON ('. $table . '_object_owner.entity=\'' . $table. '\' AND ' . $alias . '.id_' . $table . '= ' . $table . '_object_owner.id_object AND IFNULL(' . $table . '_object_owner.id_owner,0) IN (' . Shop::$id_shop_owner . $include_shared . '))';
		}
		return $sql;
	}	
	
	public static function shop_name_duplicated($name, $id)
	{
		if(empty($name))return false;
		$sql = '
			SELECT id_shop
			FROM '._DB_PREFIX_.'shop
			WHERE name = \''.$name .'\'';
		
		$id_found = intval(Db::getInstance()->getValue($sql));
				if($id_found ==0)return false;
		if($id_found>0 AND $id_found != (int)$id)return true;
		return false;
	}

	public static function get_main_url_id($id_shop)
	{
		if(empty($id_shop))return 0;
		$sql = '
			SELECT id_shop_url
			FROM '._DB_PREFIX_.'shop_url
			WHERE main=1 AND id_shop = \''.(int)$id_shop .'\'';
		
		$id_shopurl = intval(Db::getInstance()->getValue($sql));
		return $id_shopurl;
	}
	
	public static function check_redirect_seller_home($shop,$id_seller)
	{
		if($shop->id <=1)return;

		$is_ssl = (isset($_SERVER['HTTPS']) AND strtolower($_SERVER['HTTPS']) != 'off');
		
		$domain_name = $_SERVER['SERVER_NAME'];
		if($_SERVER['SERVER_PORT'] != "80" AND $_SERVER['SERVER_PORT'] != "443")
			$domain_name .= ":" .  $_SERVER['SERVER_PORT'];

		$sql = 'SELECT id_shop_url FROM ' . _DB_PREFIX_ . 'shop_url WHERE id_shop =' . $shop->id;
		if($is_ssl)$sql .= ' AND domain_ssl=\'' . $domain_name . '\'';
		else $sql .=  ' AND domain=\'' . $domain_name . '\'';

		$uri = 	$_SERVER['REQUEST_URI'];
		$utilen = strlen($uri);
		$endiwth = $utilen>0? substr($uri,strlen($uri)-1,1) : "";
		if($endiwth != "/")$uri .= "/";
		
		$sql .= ' AND CONCAT(physical_uri,virtual_uri)=\'' . $uri . '\'';	
		
		$id_shop_url = intval(Db::getInstance()->getValue($sql));
		if($id_shop_url <=0)return;

		if(!empty($_POST) AND Module::isInstalled('agilemultipleshop') AND Tools::getValue('controller')!='' AND Tools::getValue('ajax') =='true' )
		{
			
			$carturl = "index.php?";
			$idx = 0;
			foreach($_POST as $k => $v)
			{
				if($idx>0)$carturl .= "&";
				$carturl .= "$k=$v";
				$idx++;
			}
			Tools::redirect($carturl);
		}

		
		$shop_url = new ShopUrl($id_shop_url);
		$redirect_url = $shop_url->getUrl() . self::getLangLink() . 'home';
		header('Location: '.$redirect_url);
		exit;

	}

	private static function getLangLink($id_lang = null)
	{
		$allow = (int)Configuration::get('PS_REWRITING_SETTINGS');		
		if (!$allow || !Language::isMultiLanguageActivated(1))	return '';

		if (!$id_lang) $id_lang = Configuration::get('PS_LANG_DEFAULT');

		$language = new Language($id_lang);
		return $language->iso_code .  '/';
	}
}
