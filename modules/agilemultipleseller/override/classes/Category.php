<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.
class Category extends CategoryCore
{
	public static function updateFromShop($categories, $id_shop)
	{
				return true;
	}
	
	public static function getRootCategory($id_lang = null, Shop $shop = null)
	{
		if(!Module::isInstalled('agilemultipleseller'))return parent::getRootCategory($id_lang, $shop);
		$id_seller_home = AgileSellerManager::get_current_logged_seller_home_category_id();
		if((int)$id_seller_home <=0) 	return parent::getRootCategory($id_lang, $shop);
		if((int)$id_lang <=0)$id_lang = Context::getContext()->language->id;			
		$category = new Category($id_seller_home, $id_lang);
		if(!Validate::isLoadedObject($category))return parent::getRootCategory($id_lang, $shop);
		return $category;
	}
		
    	static public function getCategories($id_lang = false, $active = true, $order = true, $sql_filter = '', $sql_sort = '',$sql_limit = '')
	{
	    global $cookie;
	    
	    if(!Module::isInstalled('agilemultipleseller'))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit);	    if(intval($cookie->id_employee)==0)return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit); 	    if(intval($cookie->profile) != intval(Configuration::get('AGILE_MS_PROFILE_ID')))return parent::getCategories($id_lang, $active, $order, $sql_filter, $sql_sort,$sql_limit); 
	    
	 	if (!Validate::isBool($active))
	 		die(Tools::displayError());

		$sql = '
		SELECT *
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_owner` co ON c.`id_category` = co.`id_category`
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`
		WHERE 1 '.$sql_filter.' '.($id_lang ? 'AND `id_lang` = '.(int)($id_lang) : '').'
		'.($active ? 'AND `active` = 1' : '');

		if(_PS_VERSION_ > '1.5')
			$sql .= (Shop::$id_shop_owner>0? ' AND s.id_seller='  . Shop::$id_shop_owner : '' );
			
		
		$sql .= (!$id_lang ? 'GROUP BY c.id_category' : '').'
		'.($sql_sort != '' ? $sql_sort : 'ORDER BY c.`level_depth` ASC, c.`position` ASC').'
		'.($sql_limit != '' ? $sql_limit : '');


		        
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

		if (!$order)
			return $result;

		$categories = array();
		foreach ($result AS $row)
			$categories[$row['id_parent']][$row['id_category']]['infos'] = $row;

		return $categories;
	}
		

	public function getProducts($id_lang, $p, $n, $orderBy = NULL, $orderWay = NULL, $getTotal = false, $active = true, $random = false, $randomNumberProducts = 1, $checkAccess = true, Context $context = null)
	{
		global $cookie;
		if (!$checkAccess OR !$this->checkAccess($cookie->id_customer))
			return false;
		
				if(Module::isInstalled('agilemembership') AND $this->id == Configuration::get('AGILE_MEMBERSHIP_CID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);

				if(Module::isInstalled('agileprepaidcredit') AND $this->id == Configuration::getGlobalValue('AGILE_PCREDIT_CID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);

				if(Module::isInstalled('agilesellerlistoptions') AND $this->id == Configuration::get('ASLO_CATEGORY_ID')) 
            return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);
		
		if(!$context)$context = Context::getContext();

		$agile_sql_parts = AgileSellerManager::getAdditionalSqlForProducts("p");
		if(empty($agile_sql_parts['selects']) AND  empty($agile_sql_parts['joins']) AND empty($agile_sql_parts['wheres']))
			return parent::getProducts($id_lang, $p, $n, $orderBy, $orderWay, $getTotal, $active, $random, $randomNumberProducts, $checkAccess);
		
	    if(Module::isInstalled('agilesellerlistoptions'))
		{
			require_once(_PS_ROOT_DIR_ . "/modules/agilesellerlistoptions/agilesellerlistoptions.php");
			if($this->id <= 1 OR ($this->id == 2 AND  version_compare(_PS_VERSION_, '1.5', ">=")))
				return AgileSellerListOptions::get_home_products($id_lang, $p, $n);	
			
			if(empty($orderBy) || $orderBy == 'position')$orderBy = 'position2';
		}
        		

		if ($p < 1) $p = 1;

		if (empty($orderBy))
			$orderBy = 'position';
		else
						$orderBy = strtolower($orderBy);

		if (empty($orderWay))
			$orderWay = 'ASC';
		if ($orderBy == 'id_product' OR	$orderBy == 'date_add')
			$orderByPrefix = 'p';
		elseif ($orderBy == 'name')
			$orderByPrefix = 'pl';
		elseif ($orderBy == 'manufacturer')
		{
			$orderByPrefix = 'm';
			$orderBy = 'name';
		}
		elseif ($orderBy == 'position')
			$orderByPrefix = 'cp';

		if ($orderBy == 'price')
			$orderBy = 'orderprice';

		if (!Validate::isBool($active) OR !Validate::isOrderBy($orderBy) OR !Validate::isOrderWay($orderWay))
			die (Tools::displayError());


		$id_supplier = (int)(Tools::getValue('id_supplier'));

				if ($getTotal)
		{
			$sql = '
			SELECT COUNT(cp.`id_product`) AS total
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'category_product` cp ON p.`id_product` = cp.`id_product`
			    '. $agile_sql_parts['joins']. '
			WHERE cp.`id_category` = '.(int)($this->id).($active ? ' AND p.`active` = 1' : '').'
		    '. $agile_sql_parts['wheres']. '
			'.($id_supplier ? 'AND p.id_supplier = '.(int)($id_supplier) : '');

 			
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->getRow($sql);
			return isset($result) ? $result['total'] : 0;
		}


		$sql = '
		SELECT p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new,
			(p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice
		    '. $agile_sql_parts['selects']. '
		FROM `'._DB_PREFIX_.'category_product` cp
		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = cp.`id_product`
		LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
		                                           AND tr.`id_country` = '.(_PS_VERSION_ > '1.5' ?  (int)Context::getContext()->country->id : (int)Country::getDefaultCountryId()).'
	                                           	   AND tr.`id_state` = 0)
	    LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
		LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
	    '. $agile_sql_parts['joins']. '
		WHERE 1
		    AND cp.`id_category` = '.(int)($this->id).($active ? ' AND p.`active` = 1' : '').'
		    '. $agile_sql_parts['wheres']. '
    		'.($id_supplier ? 'AND p.id_supplier = '.(int)$id_supplier : '');


		if ($random === true)
		{
			$sql .= ' ORDER BY RAND()';
			$sql .= ' LIMIT 0, '.(int)($randomNumberProducts);
		}
		else
		{
			$sql .= ' ORDER BY '.(isset($orderByPrefix) ? $orderByPrefix.'.' : '').'`'.pSQL($orderBy).'` '.pSQL($orderWay).'
			LIMIT '.(((int)($p) - 1) * (int)($n)).','.(int)($n);

											}


		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

		if (!$result)
			return false;

						$seller = array();
		$id_seller = array();
		$link_seller = array();
		foreach($result as $row)
		{
    		$pid = $row['id_product'];
			$seller[$pid] = isset($row['seller'])?$row['seller'] : '';
			$id_seller[$pid]= isset($row['id_seller'])? intval($row['id_seller']) : 0;
			$link_seller[$pid]= isset($row['has_sellerlink'])? $row['has_sellerlink'] : '';
		}
			
		$resultsArray=Product::getProductsProperties((int)$id_lang, $result);
		for($idx=0;$idx<count($resultsArray); $idx++)
		{
		      $pid = $resultsArray[$idx]['id_product'];
		      $resultsArray[$idx]['seller'] = $seller[$pid];
		      $resultsArray[$idx]['id_seller'] = $id_seller[$pid];
		      $resultsArray[$idx]['has_sellerlink'] = $link_seller[$pid];
		}

		$resultsArray = AgileSellerManager::prepareSellerRattingInfo($resultsArray);
		return $resultsArray;

	}
	
	
				public static function getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, Shop $shop = null, $use_shop_context = true)
	{
		global $cookie;

		if(!Module::isInstalled('agilemultipleseller')) 
			return parent::getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, $shop, $use_shop_context);

		$isSeller = (intval($cookie->profile) == Configuration::get('AGILE_MS_PROFILE_ID') OR intval($cookie->profile) == 0);
		if(!$isSeller)
			return parent::getChildrenWithNbSelectedSubCat($id_parent, $selectedCat,  $id_lang, $shop, $use_shop_context);

		require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleseller/agilemultipleseller.php");
		require_once(_PS_ROOT_DIR_ . "/modules/agilemultipleseller/SellerInfo.php");

		if(intval($cookie->profile) > 0)
		{
			 			$id_seller = $cookie->id_employee;
		}	
		else
		{
				        $sellerinfo = new SellerInfo(SellerInfo::getIdByCustomerId($cookie->id_customer));
			$id_seller = $sellerinfo->id_seller;
		}

		$exclude = AgileMultipleSeller::getSpecialCatrgoryIds();
		
		$selectedCat = explode(',', str_replace(' ', '', $selectedCat));		
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
		SELECT c.`id_category`, c.`level_depth`, cl.`name`, IF((
			SELECT COUNT(*)
			FROM `'._DB_PREFIX_.'category` c2
			WHERE c2.`id_parent` = c.`id_category`
		) > 0, 1, 0) AS has_children, '.($selectedCat ? '(
			SELECT count(c3.`id_category`)
			FROM `'._DB_PREFIX_.'category` c3
			WHERE c3.`nleft` > c.`nleft`
			AND c3.`nright` < c.`nright`
			AND c3.`id_category`  IN ('.implode(',', array_map('intval', $selectedCat)).')
		)' : '0').' AS nbSelectedSubCat
		FROM `'._DB_PREFIX_.'category` c
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON c.`id_category` = cl.`id_category`
		LEFT JOIN `'._DB_PREFIX_.'category_owner` co ON c.id_category=co.id_category
		WHERE `id_lang` = '.(int)($id_lang).'
		AND c.`id_parent` = '.(int)($id_parent).'
		' . (empty($exclude)?'': 'AND c.id_category NOT IN (' . $exclude . ')' ) . '
		AND (IFNULL(co.id_owner,0) = ' . $id_seller . ' OR IFNULL(co.id_owner,0)=0)
		ORDER BY `position` ASC');
	}
	
}
