<?php
///-build_id: 2013122911.5947
/// This source file is subject to the Software License Agreement that is bundled with this 
/// package in the file license.txt, or you can get it here
/// http://addons-modules.com/en/content/3-terms-and-conditions-of-use
///
/// @copyright  2009-2012 Addons-Modules.com
///  If you need open code to customize or merge code with othe modules, please contact us.

class AdminImportController extends AdminImportControllerCore
{
	
	public function postProcess()
	{
				if (Module::isInstalled('agilemultipleseller') AND Tools::isSubmit('submitFileUpload') AND $this->is_seller)
		{
			if (isset($_FILES['file']) && empty($_FILES['file']['error']))
			{
				$dir = _PS_ADMIN_DIR_.'/import/' . $this->context->cookie->id_employee . '/';
				if(!file_exists($dir))mkdir($dir);
				$csvfile = $dir . date('Ymdhis').'-'.$_FILES['file']['name'];
				if (!file_exists($_FILES['file']['tmp_name']) ||
					!@move_uploaded_file($_FILES['file']['tmp_name'], $csvfile))
				{
					$this->errors[] = $this->l('an error occurred while uploading and copying file');
					return;
				}
				else
				{
					Tools::redirectAdmin(self::$currentIndex.'&token='.Tools::getValue('token').'&conf=18');
				}
			}
		}
		parent::postProcess();
	}
	
		protected function openCsvFile()
	{
		if(Module::isInstalled('agilemultipleseller')  AND $this->is_seller)
		{
			$dir = _PS_ADMIN_DIR_.'/import/' . $this->context->cookie->id_employee . '/';
		
			$handle = fopen($dir .strval(preg_replace('/\.{2,}/', '.', Tools::getValue('csv'))), 'r');

			if (!$handle)
				$this->errors[] = Tools::displayError('Cannot read the .CSV file');

			AdminImportController::rewindBomAware($handle);

			for ($i = 0; $i < (int)Tools::getValue('skip'); ++$i)
				$line = fgetcsv($handle, MAX_LINE_SIZE, $this->separator);
			return $handle;
		}

		return parent::openCsvFile();
	}
	
}

