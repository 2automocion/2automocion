{if isset($recepients) && count($recepients) >0}
<script language="javascript" type="text/javascript">
function toogle_visibility_cart_details(id)
{
	$("#" + id).toggle();
}
function pay_this_seller(formid, id, id_subcart)
{
	$("#" + id).val(id_subcart);
///alert($("#" + id).val());
	$("form#" + formid).submit();
}
</script>
<p style="margin:5px 5px 5px 15px;">
	{if $moduleformid != 'parallel_confirm_form'}
		<span style="color:red;">{l s='Please Note' mod='agilemultipleseller'}</span>
		<br>
		{l s='The product items in your shopping cart are from different sellers, you have to approve the payment for each seller separately.' mod='agilemultipleseller'}
	{/if}
		{l s='Your order payment has not been finished yet, please click "confirm" to finalize your payment .' mod='agilemultipleseller'}
	<br>
		{if $moduleformid != 'parallel_confirm_form'}
		{l s='After you finish each vendor payment, please return to the store to complete additional order payments.' mod='agilemultipleseller'}
		<br>
		<br>
	{/if}
	<input type="hidden" name="id_subcart" value="" id="id_subcart_{$modulename}">
	{foreach from=$recepients key=k item=recepient}
		<div>
		<p>
			<table style="margin-left:20px;width:500px;"><tr><td>
			{$recepient.seller_name}: {convertPrice price=$recepient.subcart_total} 
			</td><td align="right">
			{if $recepient.support_payment == 1 OR $moduleformid == 'parallel_confirm_form'}
				<input type="button" value="{l s='Details' mod='agilemultipleseller'}" onclick="toogle_visibility_cart_details('subcart_details_{$modulename}_{$recepient.id_subcart}');"/>
				{if $moduleformid != 'parallel_confirm_form'}
				<input type="button" value="{l s='Pay this seller' mod='agilemultipleseller'}" onclick="pay_this_seller('{$moduleformid}', 'id_subcart_{$modulename}',{$recepient.id_subcart});"/>
				{/if}
			{else}
				{if $moduleformid != 'parallel_confirm_form'}
				This payment module is not in use by this seller.
				{/if}
			{/if}
			</td></tr>
			</table>
		</p>
		<table id="subcart_details_{$modulename}_{$recepient.id_subcart}" style="display:none; margin-left:20px;width:500px;" class="std">
		<thead>
		<tr><td>{l s='Product' mod='agilemultipleseller'}</td><td  align="center">{l s='Quantity' mod='agilemultipleseller'}</td><td align="right">{l s='Price' mod='agilemultipleseller'}</td></tr>
		</thead>
		{foreach from=$recepient.products item=product}
			<tr><td>{$product.name}</td>
			<td align="center">{$product.quantity}</td>
			<td  align="right">{convertPrice price=$product.price}</td></tr>
		{/foreach}
		<tr><td></td><td align="right">{l s='Total Discount/Credits' mod='agilemultipleseller'}</td><td align="right">-{convertPrice price=$recepient.subcart_totaldiscounts}</td></tr>
		<tr><td></td><td align="right">{l s='Shipping' mod='agilemultipleseller'}</td><td align="right">{convertPrice price=$recepient.shippingCost}</td></tr>

		<tr><td></td><td align="right">{l s='Total Tax' mod='agilemultipleseller'}</td><td align="right">{convertPrice price=$recepient.total_tax}</td></tr>
		</table>
		</div>
	{/foreach}
	<br>
	<br>
</p>
{/if}