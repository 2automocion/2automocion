Ver 1.2 
- No manually file change is required at installation.
- Integrated with Agile paypal, make it possible for seller to collect their own sales
- Disable all products when a seller is disabled or deleted
- Seller can have their own categories 
- Seller info extend - photo (logo), address other info 
- Seller can create and maintain their own CMS pages
- Able to show seller info & Google map as a tab on product details page
- Admin is able to register categories/products for seller, then assign it to seller

1.2.2
- Some changes on configuration screen
- Seller collects sales, cart items contorl now support both ajaxCart and non ajaxCart[done]
- filtering out products list for Autocomplete input box -  ajax_product_list.php
- fltering out the category list at product detail page for set product category/default category

1.2.3
- fixed an issue with Customer Message (seller filtering was not working) 
- fixed an potential security issue(when seller manually input id_order at URL to see other sellers orders)
- fixed label/message translation issue

1.2.4 - 2011.10.08
- product listing expiry date(like ebay), it will be disabled automatically when it goes expried.
- seller can direct go to back office from front office My-Account page. 
  (It is available when "Allow customer becomes seller" is eanbled) 

1.2.5 - 2011.11.05
- Hide products from other sellers in order alert email sent to seller and display a notes
- Hide products from other sellers in order detailed page at back office if current logged in is a seller.
- Hide products from other sellers in PDF invoice at back office if current logged in is a seller.
- Display following message in above 3 "This order contains products from other sellers, they are hidden from you. But they they are visible to admin and your customer."
- Enable HTML editor for seller info description fields. 
- Fix the duplication display of customer in customer "Tab"
- Moved the "Seller Info" tab from "Employees" tab to "Customers" tab in back office.


1.3 - 2011.11.05
- Integration with seller commission module 
- If customer sign up a seller account from front office, the seller account is initially inactive. 
   Admin has to activate the seller account.

1.3.1 - 2012.01.12
- Improved the integration with Seller Commission module.

1.4.0 - 2012.01.21
 - Limit one cart only allow products from the same seller
 - Each seller is able to set his/her shipping
 - Order is no longer shared by different sellers. 

1.4.1 - 2012.02.04
- Integration of Seller List options module
  Show as HOT option
  Show on HOME option
  Show on TOP of the list(category)
- remove expiry date of product. It is implemented by listing option module 

1.4.1.2 - 2012.02.17
- Make it configurable for the approval requirement when Seller signup from front office 

1.4.1.3 - 2012.02.25
- Integrated with Agile PresteShop Paypal Adaptive payment, now can handle commission payement automatically without manually perform any payment.
- Make listing approval requirement configurable. You can enable or disable listing approval at module configuration screen. 

1.4.1.6 - 2012.03.22
- Merged Seller Info tab (back office) into Employees tab so that manage seller's other info and payment info are in in one tab.
- Seller can set Bank Account information for receiving bank transfer payment
- Integrated Bank transfer payment method, so that "Seller Collect payments" is available for bank transfer(Must use customized Agile Bank Transfer module)
- It also make it available for "Seller Collect payments" available Cash On Delivery (PrestaShop original Cash On Delivery module)
- Send all new order to store admin (send to Shop Email)

1.4.1.7 - 2012.04.08
- Remove configuration item "Automatically sign up seller account"
- Add Checkbox option at customer sign up page to choose "Create Seller Account" or not
- Hide "Position" column of categories list and product list from sellers if the seller is not the owner of the category, to prevent from sellers to changing sort positions.  

1.4.1.8 - 2012.04.14
- Integration with Agile Seller Shipping module (the module is not relaesed yet)
- Integration with Agile Membership module so that you can use membership module to limit the numebr of products listing for sellers

2.0.0 - 2012.10.20
- Module compatible with PrestaShop 1.5x, most functionalities are rebuilt in new PrestaShop architecture.
- Implemented seller front store management functionality. 
  Seller is able to manage products/orders/business info/payment info/product inquiry

2.0.1 - 2012.10.25
- Fixed search problem at PrestaShop 1.5x
- Fixed an error when input single quotation in seller info (business info) description. 

2.0.2 - 2012.11.06
- Fixed an issue that seller can not change product quantity 
- Fixed an issue that seller is not able to change order satus.

Ver 2.1.0 - 2012.12.01
- Integrated with new released Agile Multiple Shop module which allow each seller to have a virtual shop, virtual directory and seperated themes.
- show "My Seller Account" when "Allow customer to sign up a seller" is on or cuttent user has valid "Seller Account"
- Fixed a incorrect account balnce(it was rounded to integer number) at Seller Summary page
- Integrated with membership module, show membership inside of "My Seller Account" page, hide "My Membership" links.

Ver 2.1.1 - 2012.12.05
--> New seller account email send to seller and admin
--> Seller account approval email send to seller (only when module is configured at Seller Account Approval required) 
--> Seller Payment info page, only show those modules unstalled, hide those not installed in Prestashop store.

Ver 2.1.2 - 2013.01.07
- Adjusted the module to be compatible with PrestaShop 1.5.3
- Able to assign all products in a category including subcategpory to the owner of current category
  (The button visibile to admin and when the category has an owner (seller).
->Seller is able to choose List Options at front Product management page when add/edit product (My Seller Account). 
->Seller is able to register virtual(downloadable) products at front store Product Management page
->Fixed an displaying issue at Back office - Seller Payment Info tab - seller payment info list (company name was displayed incorrectly)
->fixed an issue at Seller Collects payment, the tax is not displayed correctly at payment selection page.
->Fixed an issue at front store adding a new productand choose language other than default language

Ver 2.1.4.0 - 2013.01.07
- Add new field Seller Type to seller info, each seller can have primary seller type and secondary seller type
- Seller type can be managed at back office new tab Administration tab - Seller Types tab
- At Agile Multiple Shop module - you are able to search sellers by Seller Type and Location
- When customer send a message/comment about an order from My Account - Order History, seller will also received the message by email.
- It is configurable to allow store owner to choose use Seller/Vendor Terms & Conditons or not and choose a CMS page for Seller/Vendor Terms & Conditons, and require seller/vendor agree on it before signup.
- Seller can use import function of Categories/Products/Customers(PrestaSgop 1.5x only) at back office
- Order status "Payment Remotely accepted" will be be considered as same as status "Payment Acceptd" for a valid order.
- New product notification function - when you configure the module as "Product Listing Approval Required", as admin you will receive a "New Product" notification.
- Assigned the Seller to be the owner of his/her front customer account, before it was set as public. 
- At "Seller Collects Payment" payment collection mode - now Seller is able to choose to use a sepcific payment method or not from available payment methods. 
and the each payment method will appear or disappear in the payment method list when customer checkout process.


Ver 2.1.4.3 - 2013.03.24
- Removed seller type from this module and added into Agile multiple seller module.
- Hide "root" category at front store My Seller Account - Product tabs - Information page
- Display fill path of category in drop down list at My Seller Account - Product tab - Information page 
- Show seller type fields at business info form only when agile multiple shop module is installed
- Use category full path name in category dropdown list for default category selections at My Seller Account - Products tab - Informaiton page
- Enforced required field validation - company, address1, city, postcode, country
- Gift warping fee will apply to each seller when a sopping cart contains products from multiple sellers.
- Separate the module for PrestaShop 1.4x and prestahop 15.x or higher

- Fxied an issue at Seller front office Product Management page  - price incrased automatically when there is combination of yhe price.
- Fixed an error that non seller was able to add a products
- Fixed a bug at front store My Seller Account - Account History page. It dispalys wrong records of transactions data.
- Fixed a bug at back office Customers tab - a new seller was created even when you update non seller customer account
- Fixed a bug at Seller Product management at front store - when adding specific price, if you lease the price to 0(to apply default price), it will be really set price to 0, but it should not.
- fixed an Fatal Error when admin create order from back office.
- fixed an Final price display issue at front office My Seller Account product list page
- fixed an displaying issue at specific price (discount) at My Seller Account product - specific price page.
- Fixed some translations issue
- Fixed an issue in My Seller Account when SSL is enabled - that data in form is not saved.
- Fixed an issue - when seller logged in to back office and add accessory products, seller is able to add products from other sellers.

Ver 2.1.4.5 - 2013.04.07
- Module is compatible with PrestaShop 1.5.4
- Allow seller to pay account balance (commission) from front store - My Seller Account - Sumamry page


Ver 2.1.5.0 - 2013.06.01
- Multiple language support for seller info company name and description field
- Integrated with Agile prepaid token module, and seller can convert account balance to token/credit so that seller can use it to purchase products at store.
- removed default category field from Information tab at front store My Seller Account - Products page
  (this is to avoid performance and resource issue when build full name path category tree when there are many categories)
- seller paypal email address format validation when entering payment info at both front store and back office.
- Add image format validation for product image uploading at front store My Seller Account management panel

- fixed an issue that bank wire account owner info nor saved at front store My Seller Account - Payment Info tab
- fixed and issue : seller can delete any products from front store My Seller account
- fixed an issue that products and categories will be displayed duplicated after add or import a new language.
- fixed filtering issue at Bulk Approval tab
- fixed mail subject translation issue

Ver 2.2.2.0 - 2013.10.06
=============================================
1. New features to front catalogue management page
A. Seller is able to manage product shipping info
B. Seller is able to management product attachment
C. Seller is able to have private attachments (those created by seller will only visible to sellers)  

2. Custom fields for Seller Info, make it easier for you to customize
A.It makes more flexible for store owner and system integrator to customize the module by using custom fields.
B.Support different type of custom fields: datetime, number, text, HTML
C.Configurable to choose the the usage of the fields
D.Use translation function to name the custom fields at your own needs

3. Enforce Seller Home Category
- Admin can set Seller Home Category for each seller at Seller Business Info
- Seller can only register products under categpries start from Seller Home Category
- When seller logged in at both from or back office Management pages, the category tree will start from Seller Home Category

4.Token/credit module integration improvement
This is available when Agile Multiple module, Seller Commission module and Prepaid credit module are all installed. 
A. Seller can request Convert account balances to credits/tokens (to use it at front store)
B. Seller can request Convert tokens/credit to Seller Account Balance

5. New fields are added to seller signup form front store by default
 When customer choose to sign up a seller and click on Agree on Term & Conditions, a set of new seller info fields will be displayed for seller to fill in.
 - Company
 - Address (all fields)
 - Phone

6. Following tabs back office has been changed to use PS 1.5x standard, removed compatibility with PrestaShop 1.4x
A. Bulk Approval (Under Catalogue tab when product list approval required)
B. Order Products (under Orders tab)

7 When seller signup at front office, seller basic information like address and phone information will be included in the sign up form

8. "New Order" email send to seller now include product customization information.

9. You can choose to show Seller Management Panel position on left or right side of front store page. It is configurable at module configuration screen.
   By Default, front store Seller Management Panel uses left column and center column, that is at left side of the page. You can change that based on your theme design.

10. Changes and fixes
Change - When Seller List Options module is installed, it will always show store owner's products because it never need to pay for list options
Change - Special product like membership/tokens/list options will not be included in Top Seller list
Change - Agile Installer is updated to detect override conflicts automatically and override classes installation in a standard way
Change - Exclude special products like membership/token/list options from Top Seller list
Change - Always show store owner products even when Seller List Options is installed
Fixed bug - product customization data lost when splitting the cart contains products from multiple sellers.
Fixed bug - Error occurs when Seller to try to update order carrier tracking number at order details page at back office
Fixed bug - Google displaying issue in some themes that does not have same DIV tag as the prestaShop default theme.
Fixed bug - Checkbox for show_price is always disabled even remove "available for order"
Fixed bug - Paypal email address validation error even it is not selected as in use
Fixed bug - Description length validation is not correct, it is hard coded with 256
Fixed bug - Error occurs when seller try to delete price/weight range at back office
Fixed bug - Error occurs when Seller to try to update order carrier tracking number at order details page at back office
Fixed bug - Error occurs when seller try to delete price/weight range at back office


2.2.2.0 - 2013.12.22
- Show following tabs before seller is approved to allow seller to enter to required information before approval.
      Seller Summary/Seller Payment Info/Seller Business Info
- Configurable allow seller to register products to Home category or not (as Featured Products)
- open category tree for those selected category make it easy for seller to manage the product association.
- when seller info is saved, copy empty multiple language filed from non-empty fields in current language
- fixed an issue at front product management panel - image tab, it does not work properly at Italian language because of seismical characters
- fixed an issue at back office - seller is able add category via Quick Access link even the module is configured as not allowed.


Future tasks
- Duplicate sellers product like admin logged in, inherits the ownership of the products
- Seller's order sales stattistics
- seller to leave a message to admin
- 
