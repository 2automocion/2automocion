	{if !$is_seller OR $approveal_required}<div class="separation"></div>{/if}
	<table cellspacing="0" cellpadding="5" border="0">
		{if $is_seller}
			<tr><td colspan="2">
				<input type="hidden" name="id_seller" value="$id_seller">
			</td></tr>
		{else}
			<tr>
				<td class="col-left">
					<label>{l s='Seller:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<select name="id_seller" id="id_seller" style="width:350px;">
						{foreach from=$sellers item=seller}
							<option value="{$seller['id_seller']}" {if $id_seller== $seller['id_seller']}selected{/if}>{$seller['id_seller']} - {$seller['name']}</option>
						{/foreach}
					</select>
					({l s='The owner of the product' mod='agilemultipleseller'})
				</td>
			</tr>
		{/if}
		{if $approveal_required}
			<tr>
				<td class="col-left">
					<label>{l s='Listing Approved' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					{if $is_seller}
						<input type="hidden" name="approved" id="approved" value="{if $approved}1{else}0{/if}" />
						<input type="checkbox" name="approved" id="approved" value="1" {if $approved}checked{/if} disabled="true"  />
					{else}
						<input type="checkbox" name="approved" id="approved" value="1" {if $approved}checked{/if} />
					{/if}
					<span style="font-style:italic;">({l s='Indicates whether this product is approved for listing.  This field appears if [Listing Approval Required] is configured.' mod='agilemultipleseller'})</span>
				</td>
			</tr>
		{/if}
	</table>
