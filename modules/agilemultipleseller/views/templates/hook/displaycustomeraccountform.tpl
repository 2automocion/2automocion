<script type="text/javascript">
	$(document).ready(function() {
		selleraccountsignup();

		 $("a#seller_terms").fancybox({
	            'type' : 'iframe',
	            'width':600,
	            'height':600
	        });	
	});

	function selleraccountsignup()
	{
		if($("input[id='seller_account_signup']").attr('checked') == 'checked')
		{
			$("table[id='sellerinformation']").show();
		} else
		{
			$("table[id='sellerinformation']").hide();
		}
	}
</script>
<fieldset class="account_creation">
	<h3>{l s='Seller Account' mod='agilemultipleseller'}</h3>
	<p style="padding:5px 20px 10px 50px;">
	    {l s='If you register for a seller account, you will be able to list your products for sale on this website.'  mod='agilemultipleseller'}
	    {l s='You can also choose to create your seller account at a later time. You can register for your seller account at any time from My Account - My Seller Account page.'  mod='agilemultipleseller'}
		<br><br>
	    <input id="seller_account_signup" type="checkbox" name="seller_account_signup" value="1" {if isset($smarty.post.seller_account_signup)}{if $smarty.post.seller_account_signup == 1}checked{/if}{/if} />&nbsp;
		<script type="text/javascript">
			$("input[name='seller_account_signup']").change(function(){
				selleraccountsignup();
			});
		</script>

	    {if isset($id_cms_seller_terms) AND $id_cms_seller_terms >0}
			{l s='Yes, I have read and I agree on the Seller Terms & conditions' mod='agilemultipleseller'},
	    {/if}
	    {l s='Please create a seller account for me' mod='agilemultipleseller'}<br>
			{if isset($id_cms_seller_terms) AND $id_cms_seller_terms >0}
			</br>
			<span style="padding:5px 20px 10px 20px;"><a href="{$link_terms}" id="seller_terms">{l s='Seller Terms & conditions(read)' mod='agilemultipleseller'}</a></span>
 	    	{/if}
	</p>
    <p>&nbsp;</p>

	{$seller_sign_up_fields}
</fieldset>
 <script type="text/javascript">$('a.iframe').fancybox();</script>
