<script type="text/javascript" src="{$module_dir}js/common.js"></script>
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="{$module_dir}js/googlemaps.js"></script>
<script type="text/javascript">
    var map;
    var geocoder = new google.maps.Geocoder();
    var markersArray = [];

   $(document).ready(function() 
   {ldelim}
        initializeMap({$sellerInfo->latitude},{$sellerInfo->longitude} , 12, "map_canvas");
        loc = new google.maps.LatLng({$sellerInfo->latitude}, {$sellerInfo->longitude});
        addMarker('0',loc);
    {rdelim}
    );



</script>
<div id="idTab19">
		<div class="margin-form">
            <h3>{$sellerInfo->company}</h3>
		    <table cellpadding="2" cellspacing="5">
		    <tr>
		    <td valign="top" style="padding:10px;">
           	    {* The logo image is always use the orignal size of logo image, please use either width OR height to display size  *}
		        <img src="{$sellerInfo->get_seller_logo_url()}" width="120" /><br />
		    </td>
	    
		    <td valign="top" style="padding:10px;">
		        <b>{l s='Address:' mod='agilemultipleseller'}</b><br />
		        	{$sellerInfo->address1}<br />
		        	{if $sellerInfo->address2}{$sellerInfo->address2}<br />{/if}
		        	{$sellerInfo->city}, {$sellerInfo->state} {$sellerInfo->postcode}<br />
		        	{$sellerInfo->country} <br /><br />
		        {if $sellerInfo->phone}

    		    <b>{l s='Phone:' mod='agilemultipleseller'}</b><br />{$sellerInfo->phone}<br />
		        {/if}
                <br />
		        {$sellerInfo->description}
		    </td>
		    </tr>
		    </table>
		</div>


		<div class="margin-form">
    	    <div id="map_canvas" style="width:480px;height:250px;padding:0px;margin:0px;"></div>
		</div>
        
</div>
<script type="text/javascript">
	var goreviewtab = {$goreviewtab};///integration with agile product review
	function switchbacktomoreinfo()
	{
		if(goreviewtab !== 1)
		    $("#more_info_block ul").idTabs("idTab1"); 
	}

    $("#more_info_tabs").idTabs("idTab19"); //make google map show first
	setTimeout("switchbacktomoreinfo()",600);
</script>
