<script type="text/javascript" src="{$base_dir_ssl}js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/tinymce.inc.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
<script type="text/javascript">
	function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
	{
		changeLanguage(field, fieldsString, id_language_new, iso_code);
		$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
	}
</script>
<table id="sellerinformation" name="sellerinformation" cellpadding="15" style="display: none;width: 80%;border:dotted 0px gray;"align="center">
{if in_array('company', $display_fields)}
<tr>
	<td style="width:150px" align="right"><p><label>{l s='Company:' mod='agilemultipleseller'}</p></label></td>
	<td nowrap> 
		<table>
			<tr>
			<td>
				{foreach $languages AS $language}
					{assign var="parameter" value="company_"|cat:$language.id_lang}
					<div id="company_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="company_{$language.id_lang}" name="company_{$language.id_lang}" value="{if isset($smarty.post[$parameter])}{$smarty.post[$parameter]}{else}{if $sellerinfo->company != null and array_key_exists($language.id_lang, $sellerinfo->company)}{$sellerinfo->company[$language.id_lang]}{/if}{/if}" /><sup>*</sup>
					</div>
				{/foreach}
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_company" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('company', 'company&curren;description&curren;address1&curren;address2&curren;city', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td></tr>
		</table>
	</td>
</tr>
{/if}
{if in_array('address1', $display_fields)}
<tr>
	<td style="width:150px;"><p><label>{l s='Address1:' mod='agilemultipleseller'}</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				{foreach $languages AS $language}
					{assign var="parameter" value="address1_"|cat:$language.id_lang}
					<div id="address1_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="address1_{$language.id_lang}" name="address1_{$language.id_lang}" value="{if isset($smarty.post[$parameter])}{$smarty.post[$parameter]}{else}{if $sellerinfo->address1 != null and array_key_exists($language.id_lang, $sellerinfo->address1)}{$sellerinfo->address1[$language.id_lang]}{/if}{/if}" />
					</div>
				{/foreach}
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address1" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('address1', 'company&curren;description&curren;address1&curren;address2&curren;city', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td></tr>
		</table>
	</td>
</tr>
{/if}
{if in_array('address2', $display_fields)}
<tr>
	<td style="width:150px;"><p><label>{l s='Address2:' mod='agilemultipleseller'}</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				{foreach $languages AS $language}
					{assign var="parameter" value="address2_"|cat:$language.id_lang}
					<div id="address2_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="address2_{$language.id_lang}" name="address2_{$language.id_lang}" value="{if isset($smarty.post[$parameter])}{$smarty.post[$parameter]}{else}{if $sellerinfo->address2 != null and array_key_exists($language.id_lang, $sellerinfo->address2)}{$sellerinfo->address2[$language.id_lang]}{/if}{/if}" />
					</div>
				{/foreach}
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address2" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
						<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
							alt="{$language.name}"
							class="pointer"
							title="{$language.name}"
							onclick="changeMyLanguage('address2', 'company&curren;description&curren;address1&curren;address2&curren;city', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td></tr>
		</table>
	</td>
</tr>
{/if}
{if in_array('city', $display_fields)}
<tr>
	<td style="width:150px;"><p><label>{l s='City:' mod='agilemultipleseller'}</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				{foreach $languages AS $language}
					{assign var="parameter" value="city_"|cat:$language.id_lang}
					<div id="city_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="city_{$language.id_lang}" name="city_{$language.id_lang}" value="{if isset($smarty.post[$parameter])}{$smarty.post[$parameter]}{else}{if $sellerinfo->city != null and array_key_exists($language.id_lang, $sellerinfo->city)}{$sellerinfo->city[$language.id_lang]}{/if}{/if}" />
					</div>
				{/foreach}
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_city" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('city', 'company&curren;description&curren;address1&curren;address2&curren;city', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td></tr>
		</table>
	</td>
</tr>
{/if}
{if in_array('postcode', $display_fields)}
<tr>
	<td style="width:150px;"><p><label>{l s='Zip/Postal Code:' mod='agilemultipleseller'}</p></label></td>
	<td>  
		&nbsp;<input type="text" style="width:200px;" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{$sellerinfo->postcode|escape:'htmlall':'UTF-8'}{/if}" />
	</td>
</tr>
{/if}
{if in_array('id_country', $display_fields)}
<tr>
	<td style="width:150px;"><p><label for="id_country">{l s='Country:' mod='agilemultipleseller'}</p></label></td>
	<td>
		<select name="id_country" id="id_country">
			{foreach from=$countries item=country}
				<option value="{$country.id_country}" {if isset($smarty.post.id_country)}{if $smarty.post.id_country == $country.id_country}selected="selected"{/if}{else}{if $sellerinfo->id_country == $country.id_country}selected="selected"{/if}{/if}>{$country.name|escape:'htmlall':'UTF-8'}</option>
			{/foreach}
		</select><sup>*</sup>
	</td>
</tr>
{/if}
{if in_array("id_state", $display_fields)}
<tr>
	<td style="width:150px;"><p class="id_state"><label for="id_state">{l s='State:' mod='agilemultipleseller'}</label></p></td>
	<td nowrap>
		<p class="id_state">
			<select name="id_state" id="id_state">
			</select>
		</p>
	</td>
</tr>
{/if}
{if in_array('phone', $display_fields)}
<tr>
	<td style="width:150px;"><p><label for="phone">{l s='Phone:' mod='agilemultipleseller'}</p></label></td>
	<td>&nbsp;<input type="text" name="phone" id="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{$sellerinfo->phone|escape:'htmlall':'UTF-8'}{/if}"/>
	</td>
</tr>
{/if}
{$addtional_fields_html}
<input type="hidden" name="signin" id="signin" value="1"/>
</table>