{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilemultipleseller'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilemultipleseller'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='My Seller Account' mod='agilemultipleseller'}</h1>
{include file="$tpl_dir./errors.tpl"}
		<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
		<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.slider.css" rel="stylesheet" type="text/css" media="all" />
		<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />
		<link href="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="all" />
        <script type="text/javascript">	
        var iso = "{$isoTinyMCE}";
        var pathCSS = '{$smarty.const._THEME_CSS_DIR_}';
        var ad = "{$ad}";

		var is_multilang = 1;

        </script>
        <script type="text/javascript" src="{$base_dir_ssl}js/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="{$base_dir_ssl}js/tinymce.inc.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/jquery.typewatch.js"></script>   
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.core.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.mouse.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.slider.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.datepicker.min.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.datepicker-{$iso_code}.js"></script>
		<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>   
		<script type="text/javascript">
			$(document).ready(function() {
		        tinySetup({ elements:"nourlconvert", file_browser_callback: false });

				$('.datepicker').datepicker({
					prevText: '',
					nextText: '',
					dateFormat: 'yy-mm-dd',
				});
			});
		</script>
        		
        {include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}

    <script type="text/javascript">
    /// <![CDATA[
    idSelectedCountry = {if isset($smarty.post.id_state)}{$smarty.post.id_state|intval}{else}{if isset($sellerinfo->id_state)}{$sellerinfo->id_state|intval}{else}false{/if}{/if};
    countries = new Array();
    countriesNeedIDNumber = new Array();
    countriesNeedZipCode = new Array();
    {foreach from=$countries item='country'}
        {if isset($country.states) && $country.contains_states}
            countries[{$country.id_country|intval}] = new Array();
            {foreach from=$country.states item='state' name='states'}
	            countries[{$country.id_country|intval}].push({ldelim}'id' : '{$state.id_state}', 'name' : '{$state.name|escape:'htmlall':'UTF-8'}'{rdelim});
            {/foreach}
        {/if}
        {if $country.need_identification_number}
            countriesNeedIDNumber.push({$country.id_country|intval});
        {/if}
        {if isset($country.need_zip_code)}
            countriesNeedZipCode[{$country.id_country|intval}] = {$country.need_zip_code};
        {/if}
    {/foreach}
    $(function(){ldelim}
        $('#id_state option[value={if isset($smarty.post.id_state)}{$smarty.post.id_state}{else}{if isset($sellerinfo->id_state)}{$sellerinfo->id_state|escape:'htmlall':'UTF-8'}{/if}{/if}]').attr('selected', 'selected');
    {rdelim});
    {if $vat_management}
    {literal}
        $(document).ready(function() {
            $('#company').blur(function(){
	            vat_number();
            });
            vat_number();
            function vat_number()
            {
	            if ($('#company').val() != '')
		            $('#vat_number').show();
	            else
		            $('#vat_number').hide();
            }
        });
    {/literal}
    {/if}
    ///]]>
    </script>

	<script language="javascript" type="text/javascript">
		function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
		{
			changeLanguage(field, fieldsString, id_language_new, iso_code);
			$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
		}
	</script>


{if isset($seller_exists) AND $seller_exists}
    <form action="{$link->getModuleLink('agilemultipleseller', 'sellerbusiness', [], true)}" enctype="multipart/form-data" method="post" class="std">
        <h3>{l s='Your business information' mod='agilemultipleseller'}</h3>
         <input type="hidden" name="token" value="{$token}" />
        <table style="width:100%;" border="0">
        <tr style="display:{if $is_multiple_shop_installed}{else}none{/if};">
            <td>
                <label for="company">{l s='Primary Type' mod='agilemultipleseller'}</label>
            </td>
            <td>
	            <select name="id_sellertype1" id="id_sellertype1">
		            {foreach from=$sellertypes item=sellertype}
					<option value="{$sellertype['id_sellertype']}" {if $sellerinfo->id_sellertype1==$sellertype['id_sellertype']}selected{/if}>{$sellertype['name']}</option>
					{/foreach}
	            </select>
            </td>
            <td>
                <label for="company">{l s='Secondary Type' mod='agilemultipleseller'}</label>
            </td>
            <td>
	            <select name="id_sellertype2" id="id_sellertype2">
		            {foreach from=$sellertypes item=sellertype}
					<option value="{$sellertype['id_sellertype']}" {if $sellerinfo->id_sellertype2==$sellertype['id_sellertype']}selected{/if} >{$sellertype['name']}</option>
					{/foreach}
	            </select>
            </td>
        </tr>

        <tr>
            <td>
                <label for="company">{l s='Company' mod='agilemultipleseller'}</label>&nbsp;<sup>*</sup>
            </td>
            <td>
				<div class="translatable">
					{foreach $languages AS $language}
					<div id="company_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
						<input type="text" style="width:200px;" id="company_{$language.id_lang}" name="company_{$language.id_lang}" value="{if isset($smarty.post.company[{$language.id_lang}])}{$smarty.post.company[{$language.id_lang}]}{else}{if isset($sellerinfo->company[{$language.id_lang}])}{$sellerinfo->company[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					</div>
					{/foreach}
				<div/>
				<div class="displayed_flag" style="float:left" >
					<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
						class="pointer"
						id="language_current_company"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_company" class="language_flags" style="display:none;float: left; background-color:lightgray" >
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								class="pointer"
								alt="{$language.name}"
								title="{$language.name}"
								onclick="changeMyLanguage('company', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td>
            <td>
				{if $is_multiple_shop_installed}
                <label for="shop_name">{l s='Shop Name' mod='agilemultipleseller'}</label>
                {/if}
            </td>
            <td>
				{if $is_multiple_shop_installed}
	            <input type="text" style="width:200px;" id="shop_name" name="shop_name" value="{if isset($smarty.post.shop_name)}{$smarty.post.shop_name}{else}{if isset($seller_shop->name)}{$seller_shop->name|escape:'htmlall':'UTF-8'}{/if}{/if}" />
	            ({l s='ID' mod='agilemultipleseller' mod='agilemultipleseller'}:{$seller_shop->id})
	            {/if}
            </td>
        </tr>
		{if $is_multiple_shop_installed}
		<tr><td height="3px"></td></tr>			
        <tr>
			<td>
				{l s='Shop full Url' mod='agilemultipleseller'}
			</td>
			<td>
				<span style="color:blue;">{$seller_shopurl->getURL()}</span>
			</td>
            <td>
                <label for="virtual_uri" style="display:{if $shop_url_mode==1}none{/if};">{l s='Virtual Uri' mod='agilemultipleseller'}</label>
            </td>
            <td>
				<input type="text" style="width:200px; display:{if $shop_url_mode==1}none{/if};" id="virtual_uri" name="virtual_uri" value="{if isset($smarty.post.virtual_uri)}{$smarty.post.virtual_uri}{else}{if isset($seller_shopurl)}{$seller_shopurl->virtual_uri|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>
		</tr>
		{/if}
        <tr>
            <td valign="center">{l s='Show Logo'  mod='agilemultipleseller'}</td>
            <td align="center">
                <div style="padding:10px;">
	           	    {* The logo image is always use the orignal size of logo image, please use either width OR height to display size  *}
                    <img src="{$sellerinfo->get_seller_logo_url()}" width="120" alt="{l s='You Logo' mod='agilemultipleseller'}" title="{l s='Your Logo' mod='agilemultipleseller'}" /></td>
                </div>
            <td colspan="2" align="left">
                <div style="padding:10px;">
                &nbsp;&nbsp;<input type="file" name="logo" /><br />
				&nbsp;&nbsp;{l s='Upload your logo from your computer' mod='agilemultipleseller'}
                </div>
	        </td>
        </tr>
        <tr>
            <td>
                <label for="address1">{l s='Address' mod='agilemultipleseller'} <sup>*</sup></label>
            </td>
            <td>
				<div class="translatable">
					{foreach $languages AS $language}
					<div id="address1_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
						<input type="text" style="width:200px;" id="address1_{$language.id_lang}" name="address1_{$language.id_lang}" value="{if isset($smarty.post.address1[{$language.id_lang}])}{$smarty.post.address1[{$language.id_lang}]}{else}{if isset($sellerinfo->address1[{$language.id_lang}])}{$sellerinfo->address1[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					</div>
					{/foreach}
				<div/>
				<div class="displayed_flag" style="float:left" >
					<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
						class="pointer"
						id="language_current_address1"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address1" class="language_flags" style="display:none;float: left; background-color:lightgray" >
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								class="pointer"
								alt="{$language.name}"
								title="{$language.name}"
								onclick="changeMyLanguage('address1', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
            </td>
            <td>
	            <label for="address2">{l s='Address (Line 2)' mod='agilemultipleseller'}</label>
            </td><td>
				<div class="translatable">
					{foreach $languages AS $language}
					<div id="address2_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
						<input type="text" style="width:200px;" id="address2_{$language.id_lang}" name="address2_{$language.id_lang}" value="{if isset($smarty.post.address2[{$language.id_lang}])}{$smarty.post.address2[{$language.id_lang}]}{else}{if isset($sellerinfo->address2[{$language.id_lang}])}{$sellerinfo->address2[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					</div>
					{/foreach}
				<div/>
				<div class="displayed_flag" style="float:left" >
					<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
						class="pointer"
						id="language_current_address2"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address2" class="language_flags" style="display:none;float: left; background-color:lightgray" >
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								class="pointer"
								alt="{$language.name}"
								title="{$language.name}"
								onclick="changeMyLanguage('address2', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
            </td>
        </tr>
        <tr>
            <td>
	            <label for="postcode">{l s='Postal Code' mod='agilemultipleseller'} <sup>*</sup></label>
            </td>
            <td>
	            <input type="text" id="postcode" name="postcode" value="{if isset($smarty.post.postcode)}{$smarty.post.postcode}{else}{if isset($sellerinfo->postcode)}{$sellerinfo->postcode|escape:'htmlall':'UTF-8'}{/if}{/if}" onkeyup="$('#postcode').val($('#postcode').val().toUpperCase());" />
            </td>
            <td>
	            <label for="city">{l s='City' mod='agilemultipleseller'} <sup>*</sup></label>
            </td>
            <td>
				<div class="translatable">
					{foreach $languages AS $language}
					<div id="city_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
						<input type="text" style="width:200px;" id="city_{$language.id_lang}" name="city_{$language.id_lang}" value="{if isset($smarty.post.city[{$language.id_lang}])}{$smarty.post.city[{$language.id_lang}]}{else}{if isset($sellerinfo->city[{$language.id_lang}])}{$sellerinfo->city[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}" />
					</div>
					{/foreach}
				<div/>
				<div class="displayed_flag" style="float:left" >
					<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
						class="pointer"
						id="language_current_city"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_city" class="language_flags" style="display:none;float: left; background-color:lightgray" >
					{l s='Choose language:'}<br /><br />
					{foreach $languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								class="pointer"
								alt="{$language.name}"
								title="{$language.name}"
								onclick="changeMyLanguage('city', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
            </td>                
        </tr>
        <tr>
            <td>
	            <label for="id_country">{l s='Country' mod='agilemultipleseller'} <sup>*</sup></label>
            </td>
            <td>
	            <select id="id_country" name="id_country">{$countries_list}</select>
            </td>
            <td>
	            <label for="id_state">{l s='State' mod='agilemultipleseller'}</label>
            </td><td>
	            <select name="id_state" id="id_state">
		            <option value="">-</option>
	            </select>
            </td>		            
        </tr>

        <tr>
            <td>
	            <label for="phone">{l s='Phone' mod='agilemultipleseller'}</label>
            </td>
            <td>
	            <input type="text" id="phone" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{else}{if isset($sellerinfo->phone)}{$sellerinfo->phone|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>			            
            <td>
	            <label for="phone_mobile">{l s='Fax' mod='agilemultipleseller'}</label>
            </td>
            <td>
	            <input type="text" id="fax" name="fax" value="{if isset($smarty.post.fax)}{$smarty.post.fax}{else}{if isset($sellerinfo->fax)}{$sellerinfo->fax|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>
        </tr>

        <tr>
            <td>
	            <label for="phone">{l s='Identification' mod='agilemultipleseller'}</label>
            </td>
            <td>
	            <input type="text" id="dni" name="dni" value="{if isset($smarty.post.dni)}{$smarty.post.dni}{else}{if isset($sellerinfo->dni)}{$sellerinfo->dni|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>			            
            <td>
            </td>
            <td>
            </td>
        </tr>

		<tr><td height="3px"></td></tr>			
        <tr>
            <td valign="top">
	            <label for="description">{l s='Description' mod='agilemultipleseller'}</label>
            </td>
            <td colspan="3" nowrap>
			   <div class="translatable">
					{foreach $languages AS $language}
						<div id="description_{$language.id_lang}" class="lang_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'};">
							 <textarea class="rte" id="mce_{$language.id_lang}" aria-hidden="true" name="description_{$language.id_lang}" cols="26" rows="3">{if isset($smarty.post.description[{$language.id_lang}])}{$smarty.post.description[{$language.id_lang}]}{else}{if isset($sellerinfo->description[{$language.id_lang}])}{$sellerinfo->description[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
						</div>	
					{/foreach}
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_description"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_description" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('description', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</div>
            </td>
        </tr>                  
		<tr><td height="3px"></td></tr>			
        <tr>
            <td style="padding:5px;">
	            <label for="phone_mobile">{l s='Longitude' mod='agilemultipleseller'}</label>
            </td>
            <td style="padding:5px;">
	            <input type="text" id="longitude" name="longitude" value="{if isset($smarty.post.longitude)}{$smarty.post.longitude}{else}{if isset($sellerinfo->longitude)}{$sellerinfo->longitude|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>			            
            <td style="padding:5px;">
	            <label for="phone_mobile">{l s='Latitude' mod='agilemultipleseller'}</label>
            </td>
            <td style="padding:5px;">
	            <input type="text" id="latitude" name="latitude" value="{if isset($smarty.post.latitude)}{$smarty.post.latitude}{else}{if isset($sellerinfo->latitude)}{$sellerinfo->latitude|escape:'htmlall':'UTF-8'}{/if}{/if}" />
            </td>
        </tr>			            
		<tr><td height="3px"></td></tr>			
		{* Decouple customize text items to allow display position to be adjusted in flexible *}
		{if $conf["AGILE_MS_SELLER_TEXT1"]}
			{$field_name = "ams_custom_text1"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}        

		{if $conf["AGILE_MS_SELLER_TEXT2"]}
			{$field_name = "ams_custom_text2"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}   

		{if $conf["AGILE_MS_SELLER_TEXT3"]}
			{$field_name = "ams_custom_text3"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_TEXT4"]}
			{$field_name = "ams_custom_text4"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_TEXT5"]}
			{$field_name = "ams_custom_text5"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 		

		{if $conf["AGILE_MS_SELLER_TEXT6"]}
			{$field_name = "ams_custom_text6"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_TEXT7"]}
			{$field_name = "ams_custom_text7"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_TEXT8"]}
			{$field_name = "ams_custom_text8"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_TEXT9"]}
			{$field_name = "ams_custom_text9"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_TEXT10"]}
			{$field_name = "ams_custom_text10"}
			<tr>
	            <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'}; float: left;">
							<input type="text" size="100" id="{$field_name}_{$language.id_lang}" name="{$field_name}_{$language.id_lang}" 
								value="{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}" />
 							</div>
						{/foreach}
					<div/>
					<div class="displayed_flag" style="float:left" >
						<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
							class="pointer"
							id="language_current_{$field_name}"
							onclick="toggleLanguageFlags(this);" />
					</div>
					<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
						{l s='Choose language:'}<br /><br />
						{foreach $languages as $language}
								<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
									class="pointer"
									alt="{$language.name}"
									title="{$language.name}"
									onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
						{/foreach}
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{for $i=1 to 2}
			{if $conf[sprintf("AGILE_MS_SELLER_HTML%s",$i)]}
			{$field_name = sprintf("ams_custom_html%s",$i)}
			<tr>
				 <td valign="top">
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
				   <div class="translatable">
						{foreach $languages AS $language}
							{$field_value = $sellerinfo->getFieldByLang($field_name,$language.id_lang)}
							<div id="{$field_name}_{$language.id_lang}" class="lang_{$language.id_lang}" style="display: {($language.id_lang == $current_id_lang)? 'block' : 'none'};">
								 <textarea class="rte" id="mce_{$field_name}_{$language.id_lang}" aria-hidden="true" name="{$field_name}_{$language.id_lang}" cols="100" rows="3">
									{if isset($smarty.post[$field_name][$language.id_lang])}{$smarty.post[$field_name][$language.id_lang]}{else}{if isset($field_value)}{$field_value|escape:'htmlall':'UTF-8'}{/if}{/if}
								</textarea>
							</div>	
						{/foreach}
						<div class="displayed_flag" style="float:left" >
							<img src="{$base_dir}img/l/{$current_id_lang}.jpg"
								class="pointer"
								id="language_current_{$field_name}"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_{$field_name}" class="language_flags" style="display:none;float: left; background-color:lightgray" >
							{l s='Choose language:'}<br /><br />
							{foreach $languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										class="pointer"
										alt="{$language.name}"
										title="{$language.name}"
										onclick="changeMyLanguage('{$field_name}', 'company&curren;description&curren;address1&curren;address2&curren;city{$str_custom_multi_lang_fields}', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</div>
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
			{/if}        
        {/for}  
		{* Decouple customize number items to allow display position to be adjusted in flexible *}
		{if $conf["AGILE_MS_SELLER_NUMBER1"]}
			{$field_name = "ams_custom_number1"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER2"]}
			{$field_name = "ams_custom_number2"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER3"]}
			{$field_name = "ams_custom_number3"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER4"]}
			{$field_name = "ams_custom_number4"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER5"]}
			{$field_name = "ams_custom_number5"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER6"]}
			{$field_name = "ams_custom_number6"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER7"]}
			{$field_name = "ams_custom_number7"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER8"]}
			{$field_name = "ams_custom_number8"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER9"]}
			{$field_name = "ams_custom_number9"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{if $conf["AGILE_MS_SELLER_NUMBER10"]}
			{$field_name = "ams_custom_number10"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if}

		{* Decouple customize date items to allow display position to be adjusted in flexible *}
		{if $conf["AGILE_MS_SELLER_DATE1"]}
			{$field_name = "ams_custom_date1"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" class="datepicker" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_DATE2"]}
			{$field_name = "ams_custom_date2"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" class="datepicker" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_DATE3"]}
			{$field_name = "ams_custom_date3"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" class="datepicker" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_DATE4"]}
			{$field_name = "ams_custom_date4"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" class="datepicker" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		{if $conf["AGILE_MS_SELLER_DATE5"]}
			{$field_name = "ams_custom_date5"}
			<tr>
				<td>
					<label>{$custom_labels[$field_name]}</label>
				</td>
				<td colspan="3" nowrap>
					<input type="text" id="{$field_name}" name="{$field_name}" class="datepicker" 
					value="{if isset($smarty.post[$field_name])}{$smarty.post[$field_name]}{else}{if isset($sellerinfo->{$field_name})}{$sellerinfo->{$field_name}|escape:'htmlall':'UTF-8'}{/if}{/if}" />
				</td>
			</tr>
			<tr><td height="3px"></td></tr>			
		{/if} 

		<tr>
            <td valign="top">
                <br />
	            <label for="phone_mobile">{l s='Map' mod='agilemultipleseller'}</label>
            </td>
            <td colspan="3" valign="top">
                <div style="margin:0px 0px 0px 5px;">
                    {include file="$agilemultipleseller_views./templates/googlemap.tpl"}
	            </div>
            </td>
        </tr>
		<tr><td height="3px"></td></tr>			
        <tr>
            <td></td>
            <td><sup>*</sup> {l s='Required field' mod='agilemultipleseller'}</td>
            <td></td>
            <td>
                <input type="hidden" name="id_sellerinfo" value="{$sellerinfo->id|intval}" />
                <input type="hidden" name="id_customer" value="{$sellerinfo->id_customer|intval}" />
                {if isset($select_address)}<input type="hidden" name="select_address" value="{$select_address|intval}" />{/if}
                <input type="submit" name="submitSellerinfo" id="submitSellerinfo" value="{l s='Save' mod='agilemultipleseller'}" class="button" />
            </td>
        </tr>			
        </table>
    </form>

{/if}
{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}
