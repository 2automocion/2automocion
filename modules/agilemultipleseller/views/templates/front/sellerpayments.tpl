{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilemultipleseller'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilemultipleseller'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='My Seller Account' mod='agilemultipleseller'}</h1>
{include file="$tpl_dir./errors.tpl"}

{include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}

{if isset($seller_exists) AND $seller_exists}
<form action="{$link->getModuleLink('agilemultipleseller', 'sellerpayments', [], true)}" method="post" class="std" id="add_adress">
	<fieldset style="margin:10px;padding:10px;">
		<legend><strong></strng>{l s='Your Paypal Account' mod='agilemultipleseller'}</strong></legend>
       <input type="hidden"  name="id_paymentinfo_paypal" value="{$paymentinfo_paypal->id}" />
        <input type="checkbox" id="paypal_in_use" name ="paypal_in_use"  alt="if you want to use this payment type." value="1" {if $paymentinfo_paypal->in_use eq 1}checked="checked"{/if}><strong> In use</strong></input>
		<p class="text">
            <label for="phone">{l s='Paypal Account Email' mod='agilemultipleseller'}</label>
            <input type="text" id="paypal_email" size="80" name="paypal_email" value="{if isset($smarty.post.paypal_email)}{$smarty.post.paypal_email}{else}{if isset($paymentinfo_paypal->info1)}{$paymentinfo_paypal->info1|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
	</fieldset>
        
	<fieldset style="margin:10px;padding:10px;display:{if $is_agilegooglecheckout_installed}{else}none{/if};">
		<legend><strong></strng>{l s='Your Google Checkout Account' mod='agilemultipleseller'}</strong></legend>
        <input type="hidden" name="id_paymentinfo_gcheckout" value="{$paymentinfo_gcheckout->id}" />
        <input type="checkbox" id="gcheckout_in_use" name ="gcheckout_in_use" tip="if you want to use this payment type." value="1" {if $paymentinfo_gcheckout->in_use eq 1}checked="checked"{/if}><strong> In use</strong></input>
		<p class="text">
        <label>{l s='Google Checkout Account' mod='agilemultipleseller'}</label>
        <input type="text"  name="gcheckout_merchant_id"  size="80" value="{if isset($smarty.post.gcheckout_merchant_id)}{$smarty.post.gcheckout_merchant_id}{else}{if isset($paymentinfo_gcheckout->info1)}{$paymentinfo_gcheckout->info1|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
        <p class="text">
        <label>{l s='Google API Key' mod='agilemultipleseller'}</label>
        <input type="text" name="gcheckout_key" value="{if isset($smarty.post.gcheckout_key)}{$smarty.post.gcheckout_key}{else}{if isset($paymentinfo_gcheckout->info2)}{$paymentinfo_gcheckout->info2|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
	</fieldset>
        
	<fieldset style="margin:10px;padding:10px;display:{if $is_agilebankwire_installed}{else}none{/if};">
		<legend><strong></strng>{l s='Your Bank Account' mod='agilemultipleseller'}</strong></legend>
        <input type="hidden" name="id_paymentinfo_bankwire" value="{$paymentinfo_bankwire->id}" />
        <input type="checkbox" id="bankwire_in_use" name ="bankwire_in_use" tip="if you want to use this payment type." value="1" {if $paymentinfo_bankwire->in_use eq 1}checked="checked"{/if}><strong> In use</strong></input><p class="text">
        <label>{l s='Bank Account Owner' mod='agilemultipleseller'}</label>
        <input type="text" name="bankwire_accountowner"  size="80" value="{if isset($smarty.post.bankwire_accountowner)}{$smarty.post.bankwire_accountowner}{else}{if isset($paymentinfo_bankwire->info1)}{$paymentinfo_bankwire->info1|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
        <p class="text">
        <label>{l s='Bank Account Details' mod='agilemultipleseller'}: </label>
        <textarea rows="3"  style="width:370px;" name="bankwire_accountdetails">{if isset($smarty.post.bankwire_accountdetails)}{$smarty.post.bankwire_accountdetails}{else}{if isset($paymentinfo_bankwire->info2)}{$paymentinfo_bankwire->info2|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
        </p>
        <p class="text">
        <label>{l s='Bank Address' mod='agilemultipleseller'}: </label>
        <textarea rows="3" style="width:370px;" name="bankwire_bankaddress">{if isset($smarty.post.bankwire_bankaddress)}{$smarty.post.bankwire_bankaddress}{else}{if isset($paymentinfo_bankwire->info3)}{$paymentinfo_bankwire->info3|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
        </p>
	</fieldset>

	<fieldset style="margin:10px;padding:10px;display:{if $is_agilepaybycheque_installed}{else}none{/if};">
		<legend><strong></strng>{l s='Your Check Account' mod='agilemultipleseller'}</strong></legend>
        <input type="hidden" name="id_paymentinfo_agilepaybycheque" value="{$paymentinfo_agilepaybycheque->id}" />
        <input type="checkbox" id="agilepaybycheque_in_use" name ="agilepaybycheque_in_use" tip="if you want to use this payment type." value="1" {if $paymentinfo_agilepaybycheque->in_use eq 1}checked="checked"{/if}><strong> In use</strong></input><p class="text">
		<p class="text">
        <label>{l s='To the order of' mod='agilemultipleseller'}</label>
        <input type="text" name="agilepaybycheque_paytoname" value="{if isset($smarty.post.agilepaybycheque_paytoname)}{$smarty.post.agilepaybycheque_paytoname}{else}{if isset($paymentinfo_agilepaybycheque->info1)}{$paymentinfo_agilepaybycheque->info1|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
        <p class="text">
        <label>{l s='Address' mod='agilemultipleseller'}</label>
        <input type="text" name="agilepaybycheque_address" value="{if isset($smarty.post.agilepaybycheque_address)}{$smarty.post.agilepaybycheque_address}{else}{if isset($paymentinfo_agilepaybycheque->info2)}{$paymentinfo_agilepaybycheque->info2|escape:'htmlall':'UTF-8'}{/if}{/if}" />
        </p>
	</fieldset>


    <p class="submit2">
		<center>
        <input type="hidden" name="id_sellerinfo" value="{$sellerinfo->id|intval}" />
        <input type="submit" name="submitSellerinfo" id="submitSellerinfo" value="{l s='Save' mod='agilemultipleseller'}" class="button" />
		</center>
    </p>
</form> 
{/if}
{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}
