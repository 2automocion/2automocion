{*
*}

<script type="text/javascript">
	var msg_combination_1 = '{l s='Please choose an attribute' mod='agilemultipleseller'}';
	var msg_combination_2 = '{l s='Please choose a value' mod='agilemultipleseller'}';
	var msg_combination_3 = '{l s='You can only add one combination per type of attribute' mod='agilemultipleseller'}';
	var msg_new_combination = '{l s='New combination' mod='agilemultipleseller'}';
</script>
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.slider.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="all" />


<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/price.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/attributesBack.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/jquery.typewatch.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.slider.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}modules/agilemultipleseller/js/front-products.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/i18n/jquery.ui.datepicker-{$iso_code}.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>   

{if isset($product->id) && !$product->is_virtual}
	<input type="hidden" name="submitted_tabs[]" value="Combinations" />
	<script type="text/javascript">
		{$combinationImagesJs}
	
        var noTax = {if $tax_exclude_taxe_option}true{else}false{/if};
		taxesArray = new Array ();
		taxesArray[0] = 0;
		{foreach $tax_rules_groups as $tax_rules_group}
			{if isset($taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']])}
			taxesArray[{$tax_rules_group.id_tax_rules_group}] = {$taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']]};
				{else}
			taxesArray[{$tax_rules_group.id_tax_rules_group}] = 0;
			{/if}
		{/foreach}
        
        
		var attrs = new Array();
		var modifyattributegroup = "{l s='Modify this attribute combination'  mod='agilemultipleseller' js=1}";
		attrs[0] = new Array(0, "---");
	    {foreach from=$attributeJs key=idgrp item=group}
		    attrs[{$idgrp}] = new Array(0
		    , '---'
		    {foreach from=$group key=idattr item=attrname}
			    , "{$idattr}", "{$attrname|addslashes}"
		    {/foreach}
		    );
	    {/foreach}
	    
	    
	    add_new_combination_form_visible = {if count($errors)>0}true{else}false{/if};
        ajax_products_url = "{$base_dir_ssl}modules/agilemultipleseller/ajax_products.php";
	    function toggle_add_new_combination_form()
	    {
	        if(add_new_combination_form_visible)
	        {
	            $("#add_new_combination").hide();
	            $("#brnCancelNewComb").hide();
	            $("#submitCombinations").hide();
	            $("#btnAddNewComb").show();
	        }
	        else
	        {
	            $("#add_new_combination").show();
	            $("#brnCancelNewComb").show();
	            $("#submitCombinations").show();
	            $("#btnAddNewComb").hide();
	        }    
	        add_new_combination_form_visible = !add_new_combination_form_visible;    
	    }
	    
	    function delete_comb(id_product,id_product_attribute)
	    {
	        if(confirm("{l s='Are sure want to delete this combination?' mod='agilemultipleseller' js=1}"))
	        {
				doFrontAjax(ajax_products_url,
				        {
						    "action":"deleteProductAttribute",
						    "id_product":id_product,
						    "id_product_attribute":id_product_attribute,
						    "ajax" : 1 
						}, 
						function(data)
						{
                            data = $.parseJSON(data);
						    alert(data.message);
						    $("#tr_comb_" + id_product_attribute).remove();
						}
				);
	        }
	    }
	    	    
	    function set_default_comb(id_product, id_product_attribute)
	    {
			doFrontAjax(ajax_products_url,
			        {
					    "action":"defaultProductAttribute",
					    "id_product":id_product,
					    "id_product_attribute":id_product_attribute,
					    "ajax" : 1 
					}, 
					function(data)
					{
                        data = $.parseJSON(data);
					    alert(data.message);
                        $("img[id^='icon_default_']").show();
                        $("#icon_default_" + id_product_attribute).hide();
					}
			);
	    
	    }
	    
	    
		function edit_comb (id_product, id_product_attribute)
		{
			$.ajax({
				url: ajax_products_url,
				type: "POST",
				data: {
					"id_product":id_product,
					"id_product_attribute":id_product_attribute,
					ajax: true,
					action: 'editProductAttribute'
				},
				dataType: 'json',
				async: false,
				success: function(data) {
					// color the selected line
					add_new_combination_form_visible = false;
                    toggle_add_new_combination_form();
					$('#attribute_quantity').show();
					var wholesale_price = Math.abs(data[0]['wholesale_price']);
					var price = Math.abs(data[0]['price']);
					var weight = Math.abs(data[0]['weight']);
					var unit_impact = Math.abs(data[0]['unit_price_impact']);
					var reference = data[0]['reference'];
					var ean = data[0]['ean13'];
					var quantity = data[0]['quantity'];
					var image = false;
					var product_att_list = new Array();
					for(i=0;i<data.length;i++)
					{
						product_att_list.push(data[i]['group_name']+' : '+data[i]['attribute_name']);
						product_att_list.push(data[i]['id_attribute']);
					}

					var id_product_attribute = data[0]['id_product_attribute'];
					var default_attribute = data[0]['default_on'];
					var eco_tax = data[0]['ecotax'];
					var upc = data[0]['upc'];
					var minimal_quantity = data[0]['minimal_quantity'];
					var available_date = data[0]['available_date'];

					if (wholesale_price != 0 && wholesale_price > 0)
					{
						$("#attribute_wholesale_price_full").show();
						$("#attribute_wholesale_price_blank").hide();
					}
					else
					{
						$("#attribute_wholesale_price_full").hide();
						$("#attribute_wholesale_price_blank").show();
					}
					fillCombination(
						wholesale_price,
						price,
						weight,
						unit_impact,
						reference,
						ean,
						quantity,
						image,
						product_att_list,
						id_product_attribute,
						default_attribute,
						eco_tax,
						upc,
						minimal_quantity,
						available_date
					);
					calcImpactPriceTI();
				}
			});
		}
	    
	    
	function fillCombination(wholesale_price, price_impact, weight_impact, unit_impact, reference,
	ean, quantity, image, old_attr, id_product_attribute, default_attribute, eco_tax, upc, minimal_quantity, available_date)
	{
		var link = '';
		init_elems();
		$('#stock_mvt_attribute').show();
		$('#initial_stock_attribute').hide();
		$('#attribute_quantity').html(quantity);
		$('#attribute_quantity').show();
		$('#attr_qty_stock').show();

		$('#attribute_minimal_quantity').val(minimal_quantity);

		getE('attribute_reference').value = reference;

		getE('attribute_ean13').value = ean;
		getE('attribute_upc').value = upc;
		getE('attribute_wholesale_price').value = Math.abs(wholesale_price);
		getE('attribute_price').value = ps_round(Math.abs(price_impact), 2);
		getE('attribute_priceTEReal').value = Math.abs(price_impact);
		getE('attribute_weight').value = Math.abs(weight_impact);
		getE('attribute_unity').value = Math.abs(unit_impact);
		if ($('#attribute_ecotax').length != 0)
			getE('attribute_ecotax').value = eco_tax;

		if (default_attribute == 1)
			getE('attribute_default').checked = true;
		else
			getE('attribute_default').checked = false;

		if (price_impact < 0)
		{
			getE('attribute_price_impact').options[getE('attribute_price_impact').selectedIndex].value = -1;
			getE('attribute_price_impact').selectedIndex = 2;
		}
		else if (!price_impact)
		{
			getE('attribute_price_impact').options[getE('attribute_price_impact').selectedIndex].value = 0;
			getE('attribute_price_impact').selectedIndex = 0;
		}
		else if (price_impact > 0)
		{
			getE('attribute_price_impact').options[getE('attribute_price_impact').selectedIndex].value = 1;
			getE('attribute_price_impact').selectedIndex = 1;
		}
		if (weight_impact < 0)
		{
			getE('attribute_weight_impact').options[getE('attribute_weight_impact').selectedIndex].value = -1;
			getE('attribute_weight_impact').selectedIndex = 2;
		}
		else if (!weight_impact)
		{
			getE('attribute_weight_impact').options[getE('attribute_weight_impact').selectedIndex].value = 0;
			getE('attribute_weight_impact').selectedIndex = 0;
		}
		else if (weight_impact > 0)
		{
			getE('attribute_weight_impact').options[getE('attribute_weight_impact').selectedIndex].value = 1;
			getE('attribute_weight_impact').selectedIndex = 1;
		}
		if (unit_impact < 0)
		{
			getE('attribute_unit_impact').options[getE('attribute_unit_impact').selectedIndex].value = -1;
			getE('attribute_unit_impact').selectedIndex = 2;
		}
		else if (!unit_impact)
		{
			getE('attribute_unit_impact').options[getE('attribute_unit_impact').selectedIndex].value = 0;
			getE('attribute_unit_impact').selectedIndex = 0;
		}
		else if (unit_impact > 0)
		{
			getE('attribute_unit_impact').options[getE('attribute_unit_impact').selectedIndex].value = 1;
			getE('attribute_unit_impact').selectedIndex = 1;
		}

		$("#add_new_combination").show();

		/* Reset all combination images */
		combinationImages = $('#id_image_attr').find("input[id^=id_image_attr_]");
		combinationImages.each(function() {
			this.checked = false;
		});

		/* Check combination images */
		
		if (typeof(combination_images[id_product_attribute]) != 'undefined')
			for (i = 0; i < combination_images[id_product_attribute].length; i++)
				$('#id_image_attr_' + combination_images[id_product_attribute][i]).attr('checked', true);
	    
		check_impact();
		check_weight_impact();
		check_unit_impact();

		var elem = getE('product_att_list');

		for (var i = 0; i < old_attr.length; i++)
		{
			var opt = document.createElement('option');
			opt.text = old_attr[i++];
			opt.value = old_attr[i];
			try {
				elem.add(opt, null);
			}
			catch(ex) {
				elem.add(opt);
			}
		}
		getE('id_product_attribute').value = id_product_attribute;

		$('#available_date_attribute').val(available_date);
	}
	
	init_elems = function()
	{
		var impact = getE('attribute_price_impact');
		var impact2 = getE('attribute_weight_impact');
		var elem = getE('product_att_list');

		if (elem.length)
			for (i = elem.length - 1; i >= 0; i--)
				if (elem[i])
					elem.remove(i);

		$('input[name="id_image_attr[]"]').each(function (){
			$(this).attr('checked', false);
		});

		$('#attribute_default').attr('checked', false);

		getE('attribute_price_impact').selectedIndex = 0;
		getE('attribute_weight_impact').selectedIndex = 0;
		getE('attribute_unit_impact').selectedIndex = 0;
		$('#span_unit_impact').hide();
		$('#unity_third').html($('#unity_second').html());

		if ($('#unity').is())
			if ($('#unity').get(0).value.length > 0)
				$('#tr_unit_impact').show();
			else
				$('#tr_unit_impact').hide();
		try
		{
			if (impact.options[impact.selectedIndex].value == 0)
				$('#span_impact').hide();
			if (impact2.options[impact.selectedIndex].value == 0)
				getE('span_weight_impact').style.display = 'none';
		}
		catch (e)
		{
			$('#span_impact').hide();
			getE('span_weight_impact').style.display = 'none';
		}
	}


	function select_all()
	{
		$("#product_att_list option").attr("selected","selected");
	}
	    
	    
	</script>
	<h4>{l s='Add or modify combinations for this product' mod='agilemultipleseller'}</h4>
	<select name="id_tax_rules_group" id="id_tax_rules_group" style="display:none;">
	    <option value="{$id_tax_rules_group}"></option>
	</select>
	<input type="hidden" name="id_product_attribute" id="id_product_attribute" value="0"/>
	
	{if isset($display_multishop_checkboxes) && $display_multishop_checkboxes}
		<br />
        {include file="$agilemultipleseller_views./templates/front/multishop/check_fields.tpl"}
	{/if}
	<div class="separation"></div>
	
	<div id="add_new_combination" style="display:{if count($errors)>0}{else}none;{/if}">
		<table cellpadding="5" style="width:100%">
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">
					<label>{l s='Attribute:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<select name="attribute_group" id="attribute_group" style="width: 200px;" onchange="populate_attrs();">
						{if isset($attributes_groups)}
							{foreach from=$attributes_groups key=k item=attribute_group}
								<option value="{$attribute_group.id_attribute_group}">{$attribute_group.name|escape:'htmlall':'UTF-8'}&nbsp;&nbsp;</option>
							{/foreach}
						{/if}
					</select>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">
					<label>{l s='Value:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<select name="attribute" id="attribute" style="width: 200px;">
						<option value="0">---</option>
					</select>
					<script type="text/javascript">
					$(document).ready(function(){
						populate_attrs();
					});
					</script>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" valign="top">
				<input style="width: 140px; margin-bottom: 10px;" type="button" value="{l s='Add' mod='agilemultipleseller'}" class="button" onclick="add_attr();"/><br />
				<input style="width: 140px;" type="button" value="{l s='Delete' mod='agilemultipleseller'}" class="button" onclick="del_attr()"/></td>
				<td align="left">
					<select id="product_att_list" name="attribute_combination_list[]" multiple="multiple" size="4" style="width: 320px;"></select>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="separation"></div>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;"><label>{l s='Reference:' mod='agilemultipleseller'}</label></td>
				<td style="padding-bottom:5px;">
					<input size="55" type="text" id="attribute_reference" name="attribute_reference" value="" style="width: 130px; margin-right: 44px;" />
					{l s='EAN13:' mod='agilemultipleseller'}<input size="55" maxlength="13" type="text" id="attribute_ean13" name="attribute_ean13" value="" style="width: 110px; margin-left: 10px; margin-right: 44px;" />
					{l s='UPC:' mod='agilemultipleseller'}<input size="55" maxlength="12" type="text" id="attribute_upc" name="attribute_upc" value="" style="width: 110px; margin-left: 10px; margin-right: 44px;" />
					<span class="hint" name="help_box">{l s='Special characters allowed:' mod='agilemultipleseller'} .-_#<span class="hint-pointer">&nbsp;</span></span>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="separation"></div>
				</td>
			</tr>
		</table>
		<table>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl"  field="attribute_wholesale_price" type="default"}
					<label>{l s='Wholesale price:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					{if $currency->format % 2 != 0}{$currency->sign}{/if}
					<input type="text" size="6"  name="attribute_wholesale_price" id="attribute_wholesale_price" value="" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');" />
					{if $currency->format % 2 == 0} {$currency->sign} {/if}<span id="attribute_wholesale_price_blank">({l s='leave blank if the price does not change' mod='agilemultipleseller'})</span>
					<span style="display:none" id="attribute_wholesale_price_full">({l s='Overrides wholesale price on "Information" tab' mod='agilemultipleseller'})</span>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="attribute_price_impact" type="attribute_price_impact"}
					<label>{l s='Impact on price:' mod='agilemultipleseller'}</label>
				</td>
				<td colspan="2" style="padding-bottom:5px;">
					<select name="attribute_price_impact" id="attribute_price_impact" style="width: 140px;" onchange="check_impact(); calcImpactPriceTI();">
						<option value="0">{l s='None' mod='agilemultipleseller'}</option>
						<option value="1">{l s='Increase' mod='agilemultipleseller'}</option>
						<option value="-1">{l s='Reduction' mod='agilemultipleseller'}</option>
					</select>
					<span id="span_impact">&nbsp;&nbsp;{l s='of' mod='agilemultipleseller'}&nbsp;&nbsp;{if $currency->format % 2 != 0}{$currency->sign} {/if}
						<input type="hidden"  id="attribute_priceTEReal" name="attribute_price" value="0.00" />
						<input type="text" size="6" id="attribute_price" value="0.00" onkeyup="$('#attribute_priceTEReal').val(this.value.replace(/,/g, '.')); if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.'); calcImpactPriceTI();"/>{if $currency->format % 2 == 0} {$currency->sign}{/if}
						{if $country_display_tax_label}
							{l s='(tax excl.)' mod='agilemultipleseller'}
							<span {if $tax_exclude_option}style="display:none"{/if}> {l s='or' mod='agilemultipleseller'}
							{if $currency->format % 2 != 0}{$currency->sign} {/if}
							<input type="text" size="6" name="attribute_priceTI" id="attribute_priceTI" value="0.00" onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.'); calcImpactPriceTE();"/>
							{if $currency->format % 2 == 0} {$currency->sign}{/if} {l s='(tax incl.)' mod='agilemultipleseller'}
							</span> {l s='final product price will be set to' mod='agilemultipleseller'}
							{if $currency->format % 2 != 0}{$currency->sign} {/if}
							<span id="attribute_new_total_price">0.00</span>
							{if $currency->format % 2 == 0}{$currency->sign} {/if}
						{/if}
					</span>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl"  field="attribute_weight_impact" type="attribute_weight_impact"}
					<label>{l s='Impact on weight:' mod='agilemultipleseller'}</label>
				</td>
				<td colspan="2" style="padding-bottom:5px;">
					<select name="attribute_weight_impact" id="attribute_weight_impact" style="width: 140px;" onchange="check_weight_impact();">
						<option value="0">{l s='None' mod='agilemultipleseller'}</option>
						<option value="1">{l s='Increase' mod='agilemultipleseller'}</option>
						<option value="-1">{l s='Reduction' mod='agilemultipleseller'}</option>
					</select>
					<span id="span_weight_impact">&nbsp;&nbsp;{l s='of' mod='agilemultipleseller'}&nbsp;&nbsp;
						<input type="text" size="6" name="attribute_weight" id="attribute_weight" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');" />
						{$ps_weight_unit}
					</span>
				</td>
			</tr>
			<tr id="tr_unit_impact">
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="attribute_unit_impact" type="attribute_unit_impact"}
					<label style="width: 100px; float: right">{l s='Impact on unit price :' mod='agilemultipleseller'}</label>
				</td>
				<td colspan="2" style="padding-bottom:5px;">
					<select name="attribute_unit_impact" id="attribute_unit_impact" style="width: 140px;" onchange="check_unit_impact();">
						<option value="0">{l s='None' mod='agilemultipleseller'}</option>
						<option value="1">{l s='Increase' mod='agilemultipleseller'}</option>
						<option value="-1">{l s='Reduction' mod='agilemultipleseller'}</option>
					</select>
					<span id="span_weight_impact">&nbsp;&nbsp;{l s='of' mod='agilemultipleseller'}&nbsp;&nbsp;&nbsp;&nbsp;
						{if $currency->format % 2 != 0} {$currency->sign} {/if}
						<input type="text" size="6" name="attribute_unity" id="attribute_unity" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');" />{if $currency->format % 2 == 0} {$currency->sign}{/if} / <span id="unity_third">{$field_value_unity}</span>
					</span>
				</td>
			</tr>
			{if $ps_use_ecotax}
				<tr>
					<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;">
                        {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="attribute_ecotax" type="default"}
						<label>{l s='Eco-tax:' mod='agilemultipleseller'}</label>
					</td>
					<td style="padding-bottom:5px;">{if $currency->format % 2 != 0}{$currency->sign}{/if}
						<input type="text" size="3" name="attribute_ecotax" id="attribute_ecotax" value="0.00" onKeyUp="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.');" />
						{if $currency->format % 2 == 0} {$currency->sign}{/if} 
						({l s='overrides Eco-tax on "Information" tab' mod='agilemultipleseller'})
					</td>
				</tr>
			{/if}
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" class="col-left">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="attribute_minimal_quantity" type="default"}
					<label>{l s='Minimum quantity:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<input size="3" maxlength="6" name="attribute_minimal_quantity" id="attribute_minimal_quantity" type="text" value="{$minimal_quantity}" />
					<p>{l s='The minimum quantity to buy this product (set to 1 to disable this feature)' mod='agilemultipleseller'}</p>
				</td>
			</tr>
			<tr>
				<td style="width:150px;vertical-align:top;text-align:right;padding-right:10px;font-weight:bold;" class="col-left">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="available_date_attribute" type="default"}
					<label>{l s='Available date:' mod='agilemultipleseller'}</label>
				</td>
				<td style="padding-bottom:5px;">
					<input class="datepicker" id="available_date_attribute" name="available_date_attribute" value="{$available_date}" style="text-align: center;" type="text" />
					<p>{l s='The available date when this product is out of stock' mod='agilemultipleseller'}</p>
					<script type="text/javascript">
						$(document).ready(function(){
							$(".datepicker").datepicker({
								prevText: '',
								nextText: '',
								dateFormat: 'yy-mm-dd'
							});
						});
					</script>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<div class="separation"></div>
				</td>
			</tr>
			<tr>
				<td style="width:150px"><label>{l s='Image:' mod='agilemultipleseller'}</label></td>
				<td style="padding-bottom:5px;">
					<ul id="id_image_attr">
						{foreach from=$images key=k item=image}
							<li style="float: left; width: {$imageWidth}px;">
								<input type="checkbox" name="id_image_attr[]" value="{$image.id_image}" id="id_image_attr_{$image.id_image}" />
								<label for="id_image_attr_{$image.id_image}" style="float: none;">
									<img src="{$smarty.const._THEME_PROD_DIR_}{$image.obj->getExistingImgPath()}-small_default.jpg" alt="{$image.legend|escape:'htmlall':'UTF-8'}" title="{$image.legend|escape:'htmlall':'UTF-8'}" />
								</label>
							</li>
						{/foreach}
					</ul>
					<img id="pic" alt="" title="" style="display: none; width: 100px; height: 100px; float: left; border: 1px dashed #BBB; margin-left: 20px;" />
				</td>
			</tr>
			<tr>
				<td style="width:150px">
                    {include file="$agilemultipleseller_views./templates/front/multishop/checkbox.tpl" field="attribute_default" type="attribute_default"}
					<label>{l s='Default:' mod='agilemultipleseller'}</label><br /><br />
				</td>
				<td style="padding-bottom:5px;">
					<input type="checkbox" name="attribute_default" id="attribute_default" value="1" />
					&nbsp;{l s='Make this the default combination for this product' mod='agilemultipleseller'}<br /><br />
				</td>
			</tr>
			<tr>
				<td style="width:150px">&nbsp;</td>
				<td style="padding-bottom:5px;">
					<span id="ResetSpan" style="float:left;margin-left:8px;display:none;">
						<input type="reset" name="ResetBtn" id="ResetBtn" onclick="getE('id_product_attribute').value = 0;" class="button" value="{l s='Cancel modification' mod='agilemultipleseller'}" />
					</span>
					<span class="clear"></span>
				</td>
			</tr>
		</table>
		<div class="separation"></div>
		<br />
	</div>
	
	<div>
	    <table width="95%" border="0"><tr><td align="right">
  	        <input type="button" class="button" id="btnAddNewComb" style="display:{if count($errors)==0}{else}none;{/if}" name="brnAddComb" onclick="javascript:toggle_add_new_combination_form()" value="{l s=' Add New ' mod='agilemultipleseller'}" />
            <input type="submit" class="button" id="submitCombinations" onclick="select_all()" style="display:{if count($errors)>0}{else}none;{/if};" name="submitCombinations" value="{l s='   Save   ' mod='agilemultipleseller'}" />
  	        <input type="button" class="button" id="brnCancelNewComb" style="display:{if count($errors)>0}{else}none;{/if};" name="btnCancelComb" onclick="javascript:toggle_add_new_combination_form()" value="{l s=' Cancel ' mod='agilemultipleseller'}" />
	    </td></tr></table>
	    <br />
	    <table class="std">
            <thead>
	            <tr>
		            <th class="first_item">{l s='Attrobutes' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='Impact' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='Weight' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='Reference' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='EAN13' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='UPC' mod='agilemultipleseller'}</th>
		            <th class="item">{l s='Actions' mod='agilemultipleseller'}</th>
		            <th class="last_item" style="width:5px">&nbsp;</th>
	            </tr>
            </thead>
            <tbody>
		    {foreach from=$combinationArray key=k item=comb}
	        <tr id="tr_comb_{$k}">
                <td>{$k}-{$comb.attributes}</td>
                <td>{$comb.price}</td>
                <td>{$comb.weight}</td>
                <td>{$comb.reference}</td>
                <td>{$comb.ean13}</td>
                <td>{$comb.upc}</td>
                <td style="white-space:nowrap;">
                    <img src="{$base_dir_ssl}img/admin/edit.gif"  style="cursor:pointer;" onclick="edit_comb({$product->id},{$k})" title="{l s='Edit' mod='agilemultipleseller'}" />
                    <img src="{$base_dir_ssl}img/admin/delete.gif"  style="cursor:pointer;" onclick="delete_comb({$product->id},{$k})" title="{l s='Delete' mod='agilemultipleseller'}"/>
                    <img src="{$base_dir_ssl}img/admin/asterisk.gif" style="cursor:pointer;display:{if $comb.default_on}none;{/if}" id="icon_default_{$k}" onclick="set_default_comb({$product->id},{$k})" title="{l s='Default' mod='agilemultipleseller'}" />
                </td>
	        </tr>
            {/foreach}	
	    </table>
	
	</div>
		

{/if}
