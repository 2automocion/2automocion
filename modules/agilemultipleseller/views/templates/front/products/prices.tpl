{*
*}

{* BEGIN CUSTOMER AUTO-COMPLETE / TO REFACTO *}
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.slider.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/price.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/jquery.typewatch.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.slider.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/i18n/jquery.ui.datepicker-{$iso_code}.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}modules/agilemultipleseller/js/front-products.js"></script>   


<script type="text/javascript">
var ecotax_tax_excl = {$ecotax_tax_excl};
var Customer = {
	"hiddenField": jQuery('#id_customer'),
	"field": jQuery('#customer'),
	"container": jQuery('#customers'),
	"loader": jQuery('#customerLoader'),
	"init": function() {
		jQuery(Customer.field).typeWatch({
			"captureLength": 1,
			"highlight": true,
			"wait": 50,
			"callback": Customer.search
		}).focus(Customer.placeholderIn).blur(Customer.placeholderOut);
	},
	"placeholderIn": function() {
		if (this.value == '{l s='All customers'}') {
			this.value = '';
		}
	},
	"placeholderOut": function() {
		if (this.value == '') {
			this.value = '{l s='All customers'}';
		}
	},
	"search": function()
	{
		Customer.showLoader();
		jQuery.ajax({
			"type": "POST",
			"url": "Customer Search URL",
			"async": true,
			"dataType": "json",
			"data": {
				"ajax": "1",
				"action": "searchCustomers",
				"customer_search": Customer.field.val()
			},
			"success": Customer.success
		});
	},
	"success": function(result)
	{
		if(result.found) {
			var html = '<ul class="clearfix">';
			jQuery.each(result.customers, function() {
				html += '<li><a class="fancybox" href="Admin Customer URL">'+this.firstname+' '+this.lastname+'</a>'+(this.birthday ? ' - '+this.birthday:'')+'<br/>';
				html += '<a href="mailto:'+this.email+'">'+this.email+'</a><br />';
				html += '<a onclick="Customer.select('+this.id_customer+', \''+this.firstname+' '+this.lastname+'\'); return false;" href="#" class="button">{l s='Choose'}</a></li>';
			});
			html += '</ul>';
		}
		else
			html = '<div class="warn">{l s='No customers found'}</div>';
		Customer.hideLoader();
		Customer.container.html(html);
		jQuery('.fancybox', Customer.container).fancybox();
	},
	"select": function(id_customer, fullname)
	{
		Customer.hiddenField.val(id_customer);
		Customer.field.val(fullname);
		Customer.container.empty();
		return false;
	},
	"showLoader": function() {
		Customer.loader.fadeIn();
	},
	"hideLoader": function() {
		Customer.loader.fadeOut();
	}
};
jQuery(document).ready(Customer.init);
</script>

{* END CUSTOMER AUTO-COMPLETE / TO REFACTO *}

<h4>{l s='Product price' mod='agilemultipleseller'}</h4>
<div class="hint" style="display:block;min-height:0;">
	{l s='You must enter either the pre-tax retail price, or the retail price with tax. The input field will be automatically calculated.' mod='agilemultipleseller'}
</div>
<div class="separation"></div>
<table>
	<tr>
		<td class="col-left"><label>{l s='Pre-tax wholesale price:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			{$currency->prefix}<input size="11" maxlength="14" name="wholesale_price" type="text" value="{$product->wholesale_price|string_format:'%.2f'}" onchange="this.value = this.value.replace(/,/g, '.');" />{$currency->suffix}
			{l s='The wholesale price at which you bought this product' mod='agilemultipleseller'}
		</td>
	</tr>

	<tr>
		<td class="col-left"><label>{l s='Pre-tax retail price:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			<input type="hidden"  id="priceTEReal" name="price" value="{$product->price}" />
			{$currency->prefix}<input size="11" maxlength="14" id="priceTE" name="price_displayed" type="text" value="{$product->price|string_format:'%.2f'}" onchange="noComma('priceTE'); $('#priceTEReal').val(this.value);" onkeyup="$('#priceType').val('TE'); $('#priceTEReal').val(this.value.replace(/,/g, '.')); if (isArrowKey(event)) return; calcPriceTI();" />{$currency->suffix}
			{l s='The pre-tax retail price to sell this product' mod='agilemultipleseller'}
		</td>
	</tr>
	<tr>
		<td class="col-left"><label>{l s='Tax rule:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			<script type="text/javascript">
				noTax = {if $tax_exclude_taxe_option}true{else}false{/if};
				taxesArray = new Array ();
				taxesArray[0] = 0;
				{foreach $tax_rules_groups as $tax_rules_group}
					{if isset($taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']])}
					taxesArray[{$tax_rules_group.id_tax_rules_group}] = {$taxesRatesByGroup[$tax_rules_group['id_tax_rules_group']]};
						{else}
					taxesArray[{$tax_rules_group.id_tax_rules_group}] = 0;
					{/if}
				{/foreach}
				ecotaxTaxRate = {$ecotaxTaxRate / 100};
			</script>

			<span {if $tax_exclude_taxe_option}style="display:none;"{/if} >
				 <select onChange="javascript:calcPrice(); unitPriceWithTax('unit');" name="id_tax_rules_group" id="id_tax_rules_group" {if $tax_exclude_taxe_option}disabled="disabled"{/if} >
					<option value="0">{l s='No Tax' mod='agilemultipleseller'}</option>
					{foreach from=$tax_rules_groups item=tax_rules_group}
						<option value="{$tax_rules_group.id_tax_rules_group}" {if $product->getIdTaxRulesGroup() == $tax_rules_group.id_tax_rules_group}selected="selected"{/if} >
							{$tax_rules_group['name']|htmlentitiesUTF8}
						</option>
					{/foreach}
				</select>
			</span>
			{if $tax_exclude_taxe_option}
				<span style="margin-left:10px; color:red;">{l s='Taxes are currently disabled' mod='agilemultipleseller'}</span> 
				<input type="hidden" value="{$product->getIdTaxRulesGroup()}" name="id_tax_rules_group" />
			{/if}
		</td>
	</tr>
	<tr {if !$ps_use_ecotax} style="display:none;"{/if}>
		<td class="col-left">
			<label>{l s='Eco-tax (tax incl.):'}</label>
		</td>
		<td>
			{$currency->prefix}<input size="11" maxlength="14" id="ecotax" name="ecotax" type="text" value="{$product->ecotax|string_format:'%.2f'}" onkeyup="$('#priceType').val('TI');if (isArrowKey(event))return; calcPriceTE(); this.value = this.value.replace(/,/g, '.'); if (parseInt(this.value) > getE('priceTE').value) this.value = getE('priceTE').value; if (isNaN(this.value)) this.value = 0;" />{$currency->suffix}
			<span style="margin-left:10px">({l s='already included in price'})</span>
		</td>
	</tr>
	<tr {if !$country_display_tax_label || $tax_exclude_taxe_option}style="display:none"{/if} >
		<td class="col-left"><label>{l s='Retail price with tax:' mod='agilemultipleseller'}</label></td>
		<td>
			{$currency->prefix}<input size="11" maxlength="14" id="priceTI" type="text" value="" onchange="noComma('priceTI');" onkeyup="$('#priceType').val('TI');if (isArrowKey(event)) return;  calcPriceTE();" />{$currency->suffix}
			<input id="priceType" name="priceType" type="hidden" value="TE" />
		</td>
	</tr>
	<tr id="tr_unit_price">
		<td class="col-left"><label>{l s='Unit price:' mod='agilemultipleseller'}</label></td>
		<td>
			{$currency->prefix} <input size="11" maxlength="14" id="unit_price" name="unit_price" type="text" value="{$unit_price|string_format:'%.2f'}"
				onkeyup="if (isArrowKey(event)) return ;this.value = this.value.replace(/,/g, '.'); unitPriceWithTax('unit');"/>{$currency->suffix}
			{l s='/'} <!--<input size="6" maxlength="10" id="unity" name="unity" type="text" value="{$product->unity|htmlentitiesUTF8}" onkeyup="if (isArrowKey(event)) return ;unitySecond();" onchange="unitySecond();"/> -->
			<select onchange="unitySecond();" name="unity" id="unity">
				{foreach $unities as $unity}
					<option value="{$unity}" {if $unity == $product->unity} selected="selected"{/if}>{$unity}</option>
				{/foreach}
			</select>
			{if $ps_tax && $country_display_tax_label}
				<span style="margin-left:15px">{l s='or' mod='agilemultipleseller'}
					{$currency->prefix}<span id="unit_price_with_tax">0.00</span>{$currency->suffix}
					{l s='/'} <span id="unity_second">{$product->unity}</span> {l s='with tax' mod='agilemultipleseller'}
				</span>
			{/if}
			{l s='e.g.  per lb' mod='agilemultipleseller'}
		</td>
	</tr>
	<tr>
		<td class="col-left"><label>{l s='On sale:' mod='agilemultipleseller'}</label></td>
		<td>
			<input type="checkbox" name="on_sale" id="on_sale" style="padding-top: 5px;" {if $product->on_sale}checked="checked"{/if} value="1" />&nbsp;<label for="on_sale" class="t">{l s='Display "on sale" icon on product page and text on product listing' mod='agilemultipleseller'}</label>
		</td>
	</tr>
	<tr>
		<td class="col-left"><label><b>{l s='Final retail price:' mod='agilemultipleseller'}</b></label></td>
		<td>
			<span {if !$country_display_tax_label}style="display:none"{/if} >
			{$currency->prefix}<span id="finalPrice" style="font-weight: bold;"></span>{$currency->suffix}<span {if $ps_tax}style="display:none;"{/if}> ({l s='tax incl.' mod='agilemultipleseller'})</span>
			</span>
			<span {if $ps_tax}style="display:none;"{/if} >

			{if $country_display_tax_label}
				 /
			{/if}
			{$currency->prefix}<span id="finalPriceWithoutTax" style="font-weight: bold;"></span>{$currency->suffix} {if $country_display_tax_label}({l s='tax excl.' mod='agilemultipleseller'}){/if}</span>
		</td>
	</tr>
</table>
<br /><br />
{if isset($specificPriceModificationForm)}
	<h4>{l s='Specific prices' mod='agilemultipleseller'}</h4>
	{l s='You can set specific prices for clients belonging to different groups, different countries, etc.' mod='agilemultipleseller'}
	<a class="button bt-icon" href="#" id="show_specific_price"><span>{l s='Add a new specific price' mod='agilemultipleseller'}</span></a>
	<a class="button bt-icon" href="#" id="hide_specific_price" style="display:none"><span>{l s='Cancel new specific price' mod='agilemultipleseller'}</span></a>
	<br/>
	<br/>
	<script type="text/javascript">
	var product_prices = new Array();
	{foreach from=$combinations item='combination'}
		product_prices['{$combination.id_product_attribute}'] = '{$combination.price}';
	{/foreach}
	</script>
	<div id="add_specific_price" style="display: none;">
	    <table cellpadding="5" cellspacing="5" width="100%">
	    <tr><td><label>{l s='For:' mod='agilemultipleseller'}</label></td>
	    <td>
		    {if !$multi_shop}
			    <div class="margin-form">
				    <input type="hidden" name="sp_id_shop" value="0" />
		    {else}
			    <div class="margin-form">
				    <select name="sp_id_shop">
					    <option value="0">{l s='All shops' mod='agilemultipleseller'}</option>
					    {foreach from=$shops item=shop}
						    <option value="{$shop.id_shop}">{$shop.name|htmlentitiesUTF8}</option>
					    {/foreach}
				    </select>
							    &gt;
		    {/if}
			    <select name="sp_id_currency" id="spm_currency_0" onchange="changeCurrencySpecificPrice(0);">
				    <option value="0">{l s='All currencies' mod='agilemultipleseller'}</option>
				    {foreach from=$currencies item=curr}
					    <option value="{$curr.id_currency}">{$curr.name|htmlentitiesUTF8}</option>
				    {/foreach}
			    </select>
						    &gt;
			    <select name="sp_id_country">
				    <option value="0">{l s='All countries' mod='agilemultipleseller'}</option>
				    {foreach from=$countries item=country}
					    <option value="{$country.id_country}">{$country.name|htmlentitiesUTF8}</option>
				    {/foreach}
			    </select>
						    &gt;
			    <select name="sp_id_group">
				    <option value="0">{l s='All groups' mod='agilemultipleseller'}</option>
				    {foreach from=$groups item=group}
					    <option value="{$group.id_group}">{$group.name}</option>
				    {/foreach}
			    </select>
	    </td></tr>
	    <tr><td><label>{l s='Customer:' mod='agilemultipleseller'}</label></td><td>	    
		    <div class="margin-form">
			    <input type="hidden" name="sp_id_customer" id="id_customer" value="0" />
			    <input type="text" name="customer" value="{l s='All customers' mod='agilemultipleseller'}" id="customer" autocomplete="off" />
			    <img src="{$base_dir_ssl}img/admin/field-loader.gif" id="customerLoader" alt="{l s='Loading...' mod='agilemultipleseller'}" style="display: none;" />
			    <div id="customers"></div>
		    </div>
		    {if $combinations|@count != 0}
			    <label>{l s='Combination:' mod='agilemultipleseller'}</label>
			    <div class="margin-form">
				    <select id="sp_id_product_attribute" name="sp_id_product_attribute">
					    <option value="0">{l s='Apply to all combinations' mod='agilemultipleseller'}</option>
					    {foreach from=$combinations item='combination'}
						    <option value="{$combination.id_product_attribute}">{$combination.attributes}</option>
					    {/foreach}
				    </select>
			    </div>
		    {/if}
	    </td></tr>
	    <tr><td><label>{l s='Available from:' mod='agilemultipleseller'}</label></td><td>
			<input class="datepicker" type="text" name="sp_from" value="" style="text-align: center" id="sp_from" /><span style="font-weight:bold; color:#000000; font-size:12px"> {l s='to' mod='agilemultipleseller'}</span>
			<input class="datepicker" type="text" name="sp_to" value="" style="text-align: center" id="sp_to" />
	    </td></tr>
	    <tr><td><label>{l s='Starting at:' mod='agilemultipleseller'}</label></td><td>	    
			<input type="text" name="sp_from_quantity" value="1" size="3" /> <span style="font-weight:bold; color:#000000; font-size:12px">{l s='unit' mod='agilemultipleseller'}</span>
		    <script type="text/javascript">
			    $(document).ready(function(){
				    product_prices['0'] = $('#sp_current_ht_price').html();

				    $('#id_product_attribute').change(function() {
					    $('#sp_current_ht_price').html(product_prices[$('#id_product_attribute option:selected').val()]);
				    });

				    $('.datepicker').datetimepicker({
					    prevText: '',
					    nextText: '',
					    dateFormat: 'yy-mm-dd',

					    // Define a custom regional settings in order to use PrestaShop translation tools
					    currentText: '{l s='Now'}',
					    closeText: '{l s='Done'}',
					    ampm: false,
					    amNames: ['AM', 'A'],
					    pmNames: ['PM', 'P'],
					    timeFormat: 'hh:mm:ss tt',
					    timeSuffix: '',
					    timeOnlyTitle: '{l s='Choose Time'}',
					    timeText: '{l s='Time'}',
					    hourText: '{l s='Hour'}',
					    minuteText: '{l s='Minute'}',
				    });
			    });
		    </script>
        </td></tr> 
        <tr><td><label>{l s='Product price'}{if $country_display_tax_label}{l s='(tax excl.):' mod='agilemultipleseller'}{/if}</label></td><td>
			<span id="spm_currency_sign_pre_0" style="font-weight:bold; color:#000000; font-size:12px">
				{$currency->prefix}
			</span>
			<input type="text" name="sp_price" value="0" size="11" />
			<span id="spm_currency_sign_post_0" style="font-weight:bold; color:#000000; font-size:12px">
				{$currency->suffix}
			</span>
			<span>
				(
					{l s='Current:' mod='agilemultipleseller'}
					<span id="sp_current_ht_price">{displayWtPrice p=$product->price}</span>
				)
			</span>
			<span>
				{l s='You can set this value to 0 in order to apply the default price' mod='agilemultipleseller'}
			</span>
        </td></tr>
        <tr><td><label>{l s='Apply a discount of:' mod='agilemultipleseller'}</label></td><td>
			<input type="text" name="sp_reduction" value="0.00" size="11" />
			<select name="sp_reduction_type">
				<option selected="selected">---</option>
				<option value="amount">{l s='Amount' mod='agilemultipleseller'}</option>
				<option value="percentage">{l s='Percentage' mod='agilemultipleseller'}</option>
			</select>
			{l s='(if set to "amount", tax is included)' mod='agilemultipleseller'}        
        </td></tr>
	    </table>
	</div>

	<table style="text-align: center;width:100%" class="table" cellpadding="0" cellspacing="0" id="specific_prices_list">
		<thead>
			<tr bgcolor="#e0e0e0">
				<th class="cell border" style="width: 12%;">{l s='Rule' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 12%;">{l s='Combination' mod='agilemultipleseller'}</th>
				{if $multi_shop}<th class="cell border" style="width: 12%;">{l s='Shop' mod='agilemultipleseller'}</th>{/if}
				<th class="cell border" style="width: 12%;">{l s='Currency' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 11%;">{l s='Country' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 13%;">{l s='Group' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 13%;">{l s='Customer' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 10%;">{l s='Fixed Price' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 10%;">{l s='Impact' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 15%;">{l s='Period' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 8%;">{l s='From' mod='agilemultipleseller'}<br>{l s='(Quantity)' mod='agilemultipleseller'}</th>
				<th class="cell border" style="width: 8%;">{l s='Action' mod='agilemultipleseller'}</th>
			</tr>
		</thead>
		<tbody>
		{$specificPriceModificationForm}
			<script type="text/javascript">
				calcPriceTI();
				unitPriceWithTax('unit');
			</script>
		{/if}
    <p style="float:right;padding:20px;">
        <input type="submit" class="button" name="submitSpecificPrices" value="{l s='   Save   ' mod='agilemultipleseller'}" />
    </p>
