{*
*}
<script type="text/javascript">
	var base_dir_ssl = "{$base_dir_ssl}";
</script>

<div class="Associations">
	<h4>{l s='Associations' mod='agilemultipleseller'}</h4>
	<div class="separation"></div>
	<div id="no_default_category" class="hint">
		{l s='Please select a default category.' mod='agilemultipleseller'}
	</div>
	<table>
		<tr>
			<td>
				<label for="category_block">{l s='Associated categories:' mod='agilemultipleseller'}</label>
			</td>
			<td class="col-right">
				<div id="category_block">{$category_tree}</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="col-left">
				<label for="id_category_default">{l s='Default category:' mod='agilemultipleseller'}</label>
			</td>
			<td class="col-right">
				<select id="id_category_default" name="id_category_default">
					{foreach from=$selected_cat item=cat}
						<option value="{$cat.id_category}" {if $id_category_default == $cat.id_category}selected="selected"{/if} >{$cat.name}</option>
					{/foreach}
				</select>
				{if $agile_ms_edit_category}
				<span> / <input type="text" name="new_category" id="id_new_category">&nbsp;<== &nbsp;{l s='New subcategory under' mod='agilemultipleseller'}:&nbsp;</span>
				{/if}
				<div class="hint" style="display:block;">{l s='The default category is the category which is displayed by default.' mod='agilemultipleseller'}</div>
			</td>
		</tr>
	</table>
	{if $feature_shop_active}
		<div class="separation"></div>
		{* @todo use asso_shop from Helper *}
		<label>{l s='Shop association:' mod='agilemultipleseller'}</label>
		{$displayAssoShop}
	{/if}

<div class="separation"></div>
<br /><br />
	<table>
		<tr>
			<td valign="top" style="width:115px;"><label>{l s='Accessories:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input type="hidden" name="inputAccessories" id="inputAccessories" value="{foreach from=$accessories item=accessory}{$accessory.id_product}-{/foreach}" />
				<input type="hidden" name="nameAccessories" id="nameAccessories" value="{foreach from=$accessories item=accessory}{$accessory.name|htmlentitiesUTF8}¤{/foreach}" />

				<div id="ajax_choose_product">
					<p style="clear:both;margin-top:0;">
						<input type="text" value="" id="product_autocomplete_input" /><br />
						{l s='Begin typing the first letters of the product name, then select the product from the drop-down list' mod='agilemultipleseller'}<br />
               			{l s='(Do not forget to save the product afterward)' mod='agilemultipleseller'}
					</p>
				</div>
				<div id="divAccessories">
					{* @todo : donot use 3 foreach, but assign var *}
					{foreach from=$accessories item=accessory}
						{$accessory.name|htmlentitiesUTF8}{if !empty($accessory.reference)}{$accessory.reference}{/if} 
						<span onclick="delAccessory({$accessory.id_product});" style="cursor: pointer;">
							<img src="{$base_dir_ssl}img/admin/delete.gif" class="middle" alt="" />
						</span><br />
					{/foreach}
				</div>
			</td>
		</tr>
	</table>
</div>
    <p style="float:right;padding:20px;">
        <input type="submit" class="button" name="submitAssociations" value="{l s='   Save   ' mod='agilemultipleseller'}" />
    </p>

<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}modules/agilemultipleseller/js/front-products.js"></script>  
