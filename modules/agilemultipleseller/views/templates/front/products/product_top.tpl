<input type="hidden" name="id_product" value="{$product->id}" />
<p>
	{l s='Product' mod='agilemultipleseller'} <span class="color-myaccount">{l s='#'}{$product->id|string_format:"%06d"}</span> - {$product->name[$id_language]}
	<span style="float:right;padding:0px 30px 0px 0px">
	<a href="{$link->getModuleLink('agilemultipleseller', 'sellerproducts', [], true)}">Back to product list</a>
	</span>
</p>
<h4>
{if $id_product>0}
<span>
    {l s='Steps' mod='agilemultipleseller'}:&nbsp;
    <select name="product_menu" id="product_menu" style="width:150px;" onchange="submit();">
    {foreach from=$product_menus item=menu}
        <option value="{$menu.id}" {if $menu.id==$product_menu}selected{/if}>{$menu.name}</option>
    {/foreach}
    </select>    
</span>
{else}
{l s='Adding a new product - other menus will be available once you save the basic information' mod='agilemultipleseller'}
{/if}
</h4>

<script type="text/javascript">
	var currentmenuid = {$product_menu};
</script>

