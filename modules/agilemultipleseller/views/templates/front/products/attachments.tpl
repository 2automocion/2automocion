<script type="text/javascript" src="{$base_dir_ssl}js/tinymce.inc.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
<script type="text/javascript">
	$('document').ready(function() {
		$("#addAttachment").live('click', function() {
			$("#selectAttachment2 option:selected").each(function(){
				var val = $('#arrayAttachments').val();
				var tab = val.split(',');
				for (var i=0; i < tab.length; i++)
					if (tab[i] == $(this).val())
						return false;
				$('#arrayAttachments').val(val+$(this).val()+',');
			});
			return !$("#selectAttachment2 option:selected").remove().appendTo("#selectAttachment1");
		});
		$("#removeAttachment").live('click', function() {
			$("#selectAttachment1 option:selected").each(function(){
				var val = $('#arrayAttachments').val();
				var tab = val.split(',');
				var tabs = '';
				for (var i=0; i < tab.length; i++)
					if (tab[i] != $(this).val())
					{
						tabs = tabs+','+tab[i];
						$('#arrayAttachments').val(tabs);
					}
			});
			return !$("#selectAttachment1 option:selected").remove().appendTo("#selectAttachment2");
		});
		$("#product").submit(function() {
			$("#selectAttachment1 option").each(function(i) {
				$(this).attr("selected", "selected");
			});
		});
	});
				
	function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
	{
		changeLanguage(field, fieldsString, id_language_new, iso_code);
		$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
	}
</script>
{if isset($product->id)}
	<input type="hidden" name="submitted_tabs[]" value="Attachments" />
	<h4>{l s='Attachment'}</h4>
	<div class="separation"></div>
	<fieldset style="border:none;">
		<label>{l s='Filename:'} </label>
		<div class="margin-form translatable">
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="attachment_name_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								&nbsp;<input type="text" style="width:200px;" id="attachment_name_{$language.id_lang}" name="attachment_name_{$language.id_lang}" value="{if $attachment_name != null and array_key_exists($language.id_lang, $attachment_name)}{$attachment_name[$language.id_lang]}{/if}" /><sup>*&nbsp;</sup>{l s='Maximum 32 characters.'}
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_attachment_name"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_attachment_name" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('attachment_name', 'attachment_name&curren;attachment_description', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td></tr>
				</table>
		</div>
		<div class="clear">&nbsp;</div>
		<label>{l s='Description:'} </label>
		<div class="margin-form translatable">
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="attachment_description_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
									<textarea id="attachment_description_{$language.id_lang}" aria-hidden="true" name="attachment_description_{$language.id_lang}" cols="100" rows="3">{if isset($attachment_description[{$language.id_lang}])}{$attachment_description[{$language.id_lang}]}{else}{if isset($attachment_description[{$language.id_lang}])}{$attachment_description[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
						<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_attachment_description"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_attachment_description" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('attachment_description', 'attachment_name&curren;attachment_description', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td></tr>
				</table>
		</div>
		<div class="clear">&nbsp;</div>
		<label>{l s='File'}</label>
		<div class="margin-form">
			<p><input type="file" name="attachment_file" /></p>
			<p class="preference_description">{l s='Upload a file from your computer'} ({$PS_ATTACHMENT_MAXIMUM_SIZE|string_format:"%.2f"} {l s='MB max.'})</p>
		</div>
		<div class="clear">&nbsp;</div>
		<div class="margin-form">
			<input type="submit" value="{l s='Upload attachment file'}" name="submitAddAttachments" class="button" />
		</div>
		<div class="small"><sup>*</sup> {l s='Required field'}</div>
	</fieldset>
	<div class="separation"></div>
	<div class="clear">&nbsp;</div>
	<table>
		<tr>
			<td>
                <p>{l s='Available attachments:'}</p>
                <select multiple id="selectAttachment2" style="width:300px;height:160px;">
                    {foreach $attach2 as $attach}
                        <option value="{$attach.id_attachment}">{$attach.name}</option>
                    {/foreach}
                </select><br /><br />
                <a href="#" id="addAttachment" style="text-align:center;display:block;border:1px solid #aaa;text-decoration:none;background-color:#fafafa;color:#123456;margin:2px;padding:2px">
                    {l s='Add'} &gt;&gt;
                </a>
            </td>
            <td style="padding-left:20px;">
                <p>{l s='Attachments for this product:'}</p>
                <select multiple id="selectAttachment1" name="attachments[]" style="width:300px;height:160px;">
                    {foreach $attach1 as $attach}
                        <option value="{$attach.id_attachment}">{$attach.name}</option>
                    {/foreach}
                </select><br /><br />
                <a href="#" id="removeAttachment" style="text-align:center;display:block;border:1px solid #aaa;text-decoration:none;background-color:#fafafa;color:#123456;margin:2px;padding:2px">
                    &lt;&lt; {l s='Remove'}
                </a>
			</td>
		</tr>
	</table>
	<div class="clear">&nbsp;</div>
	<input type="hidden" name="arrayAttachments" id="arrayAttachments" value="{foreach $attach1 as $attach}{$attach.id_attachment},{/foreach}" />
	<p style="float:right;padding:20px;">
		<input type="submit" class="button" name="submitAttachments" value="{l s='   Save   ' mod='agilemultipleseller'}" />
	</p>
{/if}
