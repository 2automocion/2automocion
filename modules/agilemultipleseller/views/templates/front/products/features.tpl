<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
<script language="javascript" type="text/javascript">
	function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
	{
		changeLanguage(field, fieldsString, id_language_new, iso_code);
		$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
	}
</script>
{if isset($product->id)}
	
	<h4>{l s='Assign features to this product:' mod='agilemultipleseller'}</h4>
	<div class="separation"></div>
	<ul>
		<li>{l s='You can specify a value for each relevant feature regarding this product, empty fields will not be displayed.' mod='agilemultipleseller'}</li>
		<li>{l s='You can either create a specific value or select among existing pre-defined values you added previously.' mod='agilemultipleseller'}</li>
	</ul>
	<br />
	<table border="5" cellpadding="0" cellspacing="0" class="std">
        <thead>
		<tr>
			<th height="39px">{l s='Feature' mod='agilemultipleseller'}</td>
			<th>{l s='Pre-defined value' mod='agilemultipleseller'}</td>
			<th><u>{l s='or'}</u> {l s='Customized value' mod='agilemultipleseller'}</td>
		</tr>
		</thead>
        <tbody>
	{foreach from=$available_features item=available_feature name=myLoop}
	<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
		<td>{$available_feature.name}</td>
		<td>
		{if sizeof($available_feature.featureValues)}
			<select id="feature_{$available_feature.id_feature}_value" name="feature_{$available_feature.id_feature}_value"
				onchange="$('.custom_{$available_feature.id_feature}_').val('');">
				<option value="0">---&nbsp;</option>
					{foreach from=$available_feature.featureValues item=value}
						<option value="{$value.id_feature_value}"{if $available_feature.current_item == $value.id_feature_value}selected="selected"{/if} >
							{$value.value|truncate:40}&nbsp;
						</option>
					{/foreach}
	
			</select>
		{else}
			<input type="hidden" name="feature_{$available_feature.id_feature}_value" value="0" />
				<span>{l s='N/A' mod='agilemultipleseller'} 
			</span>
		{/if}
		</td>
		<td class="translatable" nowrap width="50%">
			{foreach from=$all_languages key=k item=language}
			<div id="custom_{$available_feature.id_feature}_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float:left;">
				<textarea 
							name="custom_{$available_feature.id_feature}_{$language.id_lang}" 
							id="custom_{$available_feature.id_feature}_{$language.id_lang}" 
							cols="40" rows="1"
							onkeyup="if (isArrowKey(event)) return ;$('#feature_{$available_feature.id_feature}_value').val(0);">{$available_feature.val[$k].value|htmlentitiesUTF8|default:""}</textarea>
			</div>
			{/foreach}
			<div style="display:inline;float:left;">
				<div class="displayed_flag" style="margin: 4px 0 0 4px;" >&nbsp;
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_custom_{$available_feature.id_feature}"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_custom_{$available_feature.id_feature}" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $all_languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('custom_{$available_feature.id_feature}', '{$available_features_all}', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</div>
		 </td>
	</tr>
	
	{foreachelse}
		<tr><td colspan="3" style="text-align:center;">{l s='No features defined' mod='agilemultipleseller'}</td></tr>
	{/foreach}
	
        </tbody>
	</table>
	<p style="float:right;padding:20px;">
        <input type="submit" class="button" name="submitFeatures" value="{l s='   Save   ' mod='agilemultipleseller'}" />
    </p>
{/if}
