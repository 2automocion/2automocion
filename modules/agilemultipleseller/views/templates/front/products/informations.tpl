    <script type="text/javascript">
        var iso = "{$isoTinyMCE}";
        var pathCSS = "{$theme_css_dir}";
        var ad = "{$ad}";
    </script>
    <script type="text/javascript" src="{$base_dir_ssl}js/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript" src="{$base_dir_ssl}js/tinymce.inc.js"></script>
    <script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
	        tinySetup({ elements:"nourlconvert", file_browser_callback: false });
	    });

		function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
		{
			changeLanguage(field, fieldsString, id_language_new, iso_code);
			$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
		}
	</script>
	<div class="separation"></div>
	<table cellpadding="5" style="width: 100%;border:dotted 0px gray;">
		<tr>
			<td style="width:140px;"><label>{l s='Name:' mod='agilemultipleseller'}</label></td>
			<td colspan="3" nowrap> 
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="name_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								&nbsp;<input type="text" style="width:200px;" id="name_{$language.id_lang}" name="name_{$language.id_lang}" value="{if $product->name != null and array_key_exists($language.id_lang, $product->name)}{$product->name[$language.id_lang]}{/if}" /><sup>*</sup>
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_name"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_name" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('name', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td></tr>
				</table>
		    </td>
		</tr>
		<tr style="display:{if empty($categories)}none;{/if}">
			<td style="width:110px;"><label>{l s='Category:' mod='agilemultipleseller'}</label></td>
			<td colspan="3" style="padding-bottom:5px;">
			    <select name="id_category_default" id="id_category_default" style="width:530px;">
			    {foreach $categories AS $category}
			    <option value="{$category['id_category']}" {if $product->id_category_default == $category['id_category']}selected="selected"{/if}>{$category['name']}</option>
			    {/foreach}
			    </select>
			</td>
		</tr>
		<tr style="display:{if $is_agilesellerlistoptions_installed}{else}none{/if};">
			<td style="width:110px;"><label>{l s='List Options' mod='agilemultipleseller'}</label></td>
			<td>
			{$HOOK_PRODYCT_LIST_OPTIONS}
			</td>
		</tr>
	</table>
	<table cellpadding="5" border="0" style="width: 50%; float: left; margin-right: 20px; border-right: 1px solid #CCCCCC;">
	{* global information *}
		<tr>
		    <td><label>{l s='Wholesale Price:' mod='agilemultipleseller'}</label></td>
    		<td><input type="text" name="wholesale_price" id="wholesale_price" value="{$product->wholesale_price}"/>&nbsp;({$currency->sign})</td>
        </tr>
        <tr>
	    	<td><label>{l s='Retail Price:' mod='agilemultipleseller'}</label></td>
    		<td><input type="text" name="price" id="price" value="{$product->price}"/>&nbsp;({$currency->sign})</td>
		</tr>
		<tr>
			<td><label>{l s='Quantity:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="55" type="text" name="quantity" value="{$product->quantity}" style="width: 130px; margin-right: 44px;" />
			</td>
		</tr>
		<tr>
			<td class="col-left"><label>{l s='When out of stock:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input {if $product->out_of_stock == 0} checked="checked" {/if} id="out_of_stock_1" type="radio" checked="checked" value="0" class="out_of_stock" name="out_of_stock">
				<label id="label_out_of_stock_1" class="t" for="out_of_stock_1">{l s='Deny orders' mod='agilemultipleseller'}</label>
                <br />
				<input {if $product->out_of_stock == 1} checked="checked" {/if} id="out_of_stock_2" type="radio" value="1" class="out_of_stock" name="out_of_stock">
				<label id="label_out_of_stock_2" class="t" for="out_of_stock_2">{l s='Allow orders' mod='agilemultipleseller'}</label>
                <br />
				<input {if $product->out_of_stock == 2} checked="checked" {/if} id="out_of_stock_3" type="radio" value="2" class="out_of_stock" name="out_of_stock">
				<label id="label_out_of_stock_3" class="t" for="out_of_stock_3">
					{l s='Default' mod='agilemultipleseller'}:
					{if $order_out_of_stock == 1}
					<i>{l s='Allow orders' mod='agilemultipleseller'}</i>
					{else}
					<i>{l s='Deny orders' mod='agilemultipleseller'}</i>
					{/if} 
					{l s='as set in Preferences' mod='agilemultipleseller'}
				</label>
				<nr></nr>
			</td>
		</tr>
		<tr>
			<td><label>{l s='Additional shipping:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="55" type="text" name="additional_shipping_cost" value="{$product->additional_shipping_cost|htmlentitiesUTF8}" style="width: 130px; margin-right: 44px;" />&nbsp;({$currency->sign})
			</td>
		</tr>
		
		<tr>
			<td><label>{l s='Reference:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="55" type="text" name="reference" value="{$product->reference|htmlentitiesUTF8}" style="width: 130px; margin-right: 44px;" />
			</td>
		</tr>
		<tr>
			<td><label>{l s='EAN13 or JAN:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="55" maxlength="13" type="text" name="ean13" value="{$product->ean13|htmlentitiesUTF8}" style="width: 130px; margin-right: 5px;" /> <span class="small">{l s='(Europe, Japan)' mod='agilemultipleseller'}</span>
			</td>
		</tr>
		<tr>
			<td><label>{l s='UPC:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="55" maxlength="12" type="text" name="upc" value="{$product->upc}" style="width: 130px; margin-right: 5px;" /> <span class="small">{l s='(US, Canada)' mod='agilemultipleseller'}</span>
			</td>
		</tr>

	</table>
	{* status informations *}
	<table cellpadding="5" style="width: 40%; float: left; margin-left: 10px;">
	<tr>
		<td>
			<label>{l s='Status:' mod='agilemultipleseller'}</label>
		</td>
		<td style="padding-bottom:5px;">
					<input type="radio" name="active" id="active_on" value="1" {if $product->active}checked="checked" {/if} />
					<label for="active_on" class="radioCheck">{l s='Enabled' mod='agilemultipleseller'}</label><br />
					<input type="radio" name="active" id="active_off" value="0" {if !$product->active}checked="checked"{/if} />
					<label for="active_off" class="radioCheck">{l s='Disabled' mod='agilemultipleseller'}</label>
		</td>
	</tr>
	<tr>
		<td class="col-left"><label>{l s='Visibility:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			<select name="visibility" id="visibility">
				<option value="both" {if $product->visibility == 'both'}selected="selected"{/if} >{l s='Everywhere' mod='agilemultipleseller'}</option>
				<option value="catalog" {if $product->visibility == 'catalog'}selected="selected"{/if} >{l s='Catalog only' mod='agilemultipleseller'}</option>
				<option value="search" {if $product->visibility == 'search'}selected="selected"{/if} >{l s='Search only' mod='agilemultipleseller'}</option>
				<option value="none" {if $product->visibility == 'none'}selected="selected"{/if}>{l s='Nowhere' mod='agilemultipleseller'}</option>
			</select>
		</td>
	</tr>
	<tr id="product_options" {if !$product->active}style="display:none"{/if} >
		<td class="col-left"><label>{l s='Options:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
    		<input  type="checkbox" name="available_for_order" id="available_for_order" value="1" {if $product->available_for_order}checked="checked"{/if}  />
			<script type="text/javascript">
			$(document).ready(function()
			{
				$("#available_for_order").click(function(){
					if ($(this).is(':checked'))
					{
						$('#show_price').attr('checked', 'checked');
						$('#show_price').attr('disabled', 'disabled');
					}
					else
					{
						$('#show_price').removeAttr('disabled');
					}
				});
			});
			</script>
			<label for="available_for_order" class="t">{l s='available for order' mod='agilemultipleseller'}</label>
			<br />
			<input type="checkbox" name="show_price" id="show_price" value="1" {if $product->show_price}checked="checked"{/if} {if $product->available_for_order}disabled="disabled"{/if}/>
			<label for="show_price" class="t">{l s='show price' mod='agilemultipleseller'}</label>
            <br />
			<input type="checkbox" name="online_only" id="online_only" value="1" {if $product->online_only}checked="checked"{/if} />
			<label for="online_only" class="t">{l s='online only (not sold in store)' mod='agilemultipleseller'}</label>
		</td>
	</tr>
	<tr>
		<td class="col-left"><label>{l s='Condition:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			<select name="condition" id="condition">
				<option value="new" {if $product->condition == 'new'}selected="selected"{/if} >{l s='New' mod='agilemultipleseller'}</option>
				<option value="used" {if $product->condition == 'used'}selected="selected"{/if} >{l s='Used' mod='agilemultipleseller'}</option>
				<option value="refurbished" {if $product->condition == 'refurbished'}selected="selected"{/if}>{l s='Refurbished' mod='agilemultipleseller'}</option>
			</select>
		</td>
	</tr>
		<tr>
			<td><label>{l s='Supllier:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
			    <select name="id_supplier" id="id_supplier">
			    <option value="0"> -- </option>
			    {foreach $suppliers AS $supplier}
			    <option value="{$supplier['id_supplier']}" {if $product->id_supplier == $supplier['id_supplier']}selected="selected"{/if}>{$supplier['name']}</option>
			    {/foreach}
			    </select>
			</td>
		</tr>

		<tr>
			<td><label>{l s='Manufacturer:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
			    <select name="id_manufacturer" id="id_manufacturer">
			    <option value="0"> -- </option>
			    {foreach $manufacturers AS $manufacturer}
			    <option value="{$manufacturer['id_manufacturer']}" {if $product->id_manufacturer == $manufacturer['id_manufacturer']}selected="selected"{/if}>{$manufacturer['name']}</option>
			    {/foreach}
			    </select>
			</td>
		</tr>
    </table>
    <table cellpadding="5" cellspacing="0" border="0" style="width: 100%;"><tr><td><div class="separation"></div></td></tr></table>
	<table cellspacing="0" cellpadding="5" border="0">
		<tr>
			<td valign="top" style="width:140px;"><label>{l s='Short description:' mod='agilemultipleseller'}<br /></label><p class="product_description">({l s='appears in the product lists and on the top of the product page' mod='agilemultipleseller'})</p></td>
			<td colspan="3" style="padding-bottom:5px;" class="translatable">
				{foreach $all_languages AS $language}
					<div id="description_short_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'};">
							<textarea class="rte" id="mce_description_short_{$language.id_lang}" aria-hidden="true" name="description_short_{$language.id_lang}" cols="26" rows="3">{if isset($smarty.post.description_short[{$language.id_lang}])}{$smarty.post.description_short[{$language.id_lang}]}{else}{if isset($product->description_short[{$language.id_lang}])}{$product->description_short[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
					</div>
				{/foreach}
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_description_short"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_description_short" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $all_languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('description_short', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td>
		</tr>
		<tr>
			<td valign="top" style="width:140px;"><label>{l s='Description:' mod='agilemultipleseller'}<br /></label><p class="product_description">({l s='appears in the body of the product page' mod='agilemultipleseller'})</p></td>
			<td colspan="3" style="padding-bottom:5px;" class="translatable">
				{foreach $all_languages AS $language}
					<div id="description_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'};">
							<textarea class="rte" id="mce_description_{$language.id_lang}" aria-hidden="true" name="description_{$language.id_lang}" cols="26" rows="3">{if isset($smarty.post.description[{$language.id_lang}])}{$smarty.post.description[{$language.id_lang}]}{else}{if isset($product->description[{$language.id_lang}])}{$product->description[{$language.id_lang}]|escape:'htmlall':'UTF-8'}{/if}{/if}</textarea>
					</div>
				{/foreach}
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_description"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_description" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $all_languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('description', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</td>
		</tr>
	    <tr>
		    <td class="col-left" valign="top"><label>{l s='Meta Tags:' mod='agilemultipleseller'}</label></td>
		    <td style="padding-bottom:5px;" class="translatable">		        
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="meta_keywords_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								<input type="text" size="100" id="meta_keywords_{$language.id_lang}" name="meta_keywords_{$language.id_lang}" value="{if $product->meta_keywords != null and array_key_exists($language.id_lang, $product->meta_keywords)}{$product->meta_keywords[$language.id_lang]}{/if}" />
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_meta_keywords"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_meta_keywords" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('meta_keywords', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td>
				  </tr>
				</table>
		    </td>
	    </tr>

	    <tr>
		    <td class="col-left"><label>{l s='Meta Description:' mod='agilemultipleseller'}</label></td>
		    <td style="padding-bottom:5px;" class="translatable">
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="meta_description_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								<input size="100" type="text" id="meta_description_{$language.id_lang}" name="meta_description_{$language.id_lang}" value="{if $product->meta_description != null and array_key_exists($language.id_lang, $product->meta_description)}{$product->meta_description[$language.id_lang]}{/if}" />
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_meta_description"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_meta_description" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('meta_description', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td>
				  </tr>
				</table>
		    </td>
	    </tr>

	    <tr>
		    <td class="col-left"><label>{l s='Meta Title:' mod='agilemultipleseller'}</label></td>
		    <td style="padding-bottom:5px;" class="translatable">
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="meta_title_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								<input size="100" type="text" id="meta_title_{$language.id_lang}" name="meta_title_{$language.id_lang}" value="{if $product->meta_title != null and array_key_exists($language.id_lang, $product->meta_title)}{$product->meta_title[$language.id_lang]}{/if}" />
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_meta_title"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_meta_title" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('meta_title', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td>
				  </tr>
				</table>
		    </td>
	    </tr>

	    <tr>
		    <td class="col-left" valign="top"><label>{l s='Friendly URL:' mod='agilemultipleseller'}</label></td>
		    <td style="padding-bottom:5px;" class="translatable">
				<table>
					<tr valign="top"><td>
						{foreach $all_languages AS $language}
							<div id="link_rewrite_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float: left;">
								<input size="100" type="text" id="link_rewrite_{$language.id_lang}" name="link_rewrite_{$language.id_lang}" value="{if $product->link_rewrite != null and array_key_exists($language.id_lang, $product->link_rewrite)}{$product->link_rewrite[$language.id_lang]}{/if}" /><sup>*</sup>
							</div>
						{/foreach}
					</td>
					<td>
						<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
							<img src="{$base_dir}img/l/{$id_language}.jpg"
								class="pointer"
								id="language_current_link_rewrite"
								onclick="toggleLanguageFlags(this);" />
						</div>
						<div id="languages_link_rewrite" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
							{l s='Choose language:'}<br /><br />
							{foreach $all_languages as $language}
									<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
										alt="{$language.name}"
										class="pointer"
										title="{$language.name}"
										onclick="changeMyLanguage('link_rewrite', 'name&curren;description&curren;description_short&curren;meta_title&curren;meta_description&curren;link_rewrite&curren;meta_keywords', {$language.id_lang}, '{$language.iso_code}');" />
							{/foreach}
						</div>
					</td>
				  </tr>
				</table>
                <p>{l s='Leave it empty if you want the system to generate one for you' mod='agilemultipleseller' mod='agilemultipleseller'}</p>
		    </td>
	    </tr>
	</table>
    <p style="float:right;padding:20px;">
        <input type="submit" class="button" name="submitProduct" value="{l s='   Save   ' mod='agilemultipleseller'}" />
    </p>
