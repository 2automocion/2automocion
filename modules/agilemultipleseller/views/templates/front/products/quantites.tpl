{*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @version  Release: $Revision: 11069 $
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if isset($product->id)}

<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.theme.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.slider.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/ui/themes/base/jquery.ui.datepicker.css" rel="stylesheet" type="text/css" media="all" />
<link href="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.css" rel="stylesheet" type="text/css" media="all" />

<script type="text/javascript" src="{$base_dir_ssl}js/admin.js"></script>

<script type="text/javascript" src="{$base_dir_ssl}js/price.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/jquery.typewatch.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.core.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.mouse.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.slider.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.widget.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/jquery.ui.datepicker.min.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}modules/agilemultipleseller/js/front-products.js"></script>   
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/ui/i18n/jquery.ui.datepicker-{$iso_code}.js"></script>
<script type="text/javascript" src="{$base_dir_ssl}js/jquery/plugins/timepicker/jquery-ui-timepicker-addon.js"></script>   
<script language="javascript" type="text/javascript">
	function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
	{
		changeLanguage(field, fieldsString, id_language_new, iso_code);
		$("img[id^='language_current_']").attr("src","{$base_dir}img/l/" + id_language_new + ".jpg");
	}
</script>
	<h4>{l s='Available quantities for sale' mod='agilemultipleseller'}</h4>
	<div class="separation"></div>
	<div class="hint" style="display:none; position:'auto';">
		<p>{l s='This interface allows you to manage the available quantities for sale of the current product and its combinations on the current shop.' mod='agilemultipleseller'}</p>
		<p>{l s='You can choose whether or not to use the advanced stock management system for this product.' mod='agilemultipleseller'}</p>
		<p>{l s='You can manually specify the quantities for the product/each product combination, or choose to automatically determine these quantities based on your stock (if advanced stock management is activated).' mod='agilemultipleseller'}</p>
		<p>{l s='In this case, the quantities correspond to the quantitites of the real stock in the warehouses associated to the current shop or current group of shops.' mod='agilemultipleseller'}</p>
		<br/>
		<p>{l s='For packs, if it has products that use the advanced stock management, you have to specify a common warehouse for these products in the pack.' mod='agilemultipleseller'}</p>
		<p>{l s='Also, please note that when a product has combinations, its default combination will be used in stock movements.' mod='agilemultipleseller'}</p>
	</div>
	<br />

	{if $show_quantities == true}
		<div style="background:beige;color:yellow;width:100%;padding:5px;display: none;" id="available_quantity_ajax_msg"></div>
		<div style="background:beige;color:red;width:100%;padding:5px;display: none;" id="available_quantity_ajax_error_msg" ></div>
		<div style="background:beige;color:blue;width:100%;padding:5px;display: none;" id="available_quantity_ajax_success_msg"></div>

		<table cellpadding="5" style="width:100%">
			<tbody>
				<tr style="display:none;" {if $product->is_virtual || $product->cache_is_pack}style="display:none;"{/if} class="stockForVirtualProduct">
					<td valign="top" style="vertical-align:top;">
						<input 
							{if $product->advanced_stock_management == 1 && $stock_management_active == 1}
								value="1" checked="checked"
							{else}
								value="0"
							{/if} 
							{if $stock_management_active == 0 || $product->cache_is_pack}
								disabled="disabled" 
							{/if} 
							type="checkbox" name="advanced_stock_management" class="advanced_stock_management" id="advanced_stock_management" />
						<label style="float:none;font-weight:normal" for="advanced_stock_management">
							{l s='I want to use the advanced stock management system for this product' mod='agilemultipleseller'} 
							{if $stock_management_active == 0 && !$product->cache_is_pack}
							&nbsp;-&nbsp;<b>{l s='This requires you to enable advanced stock management.' mod='agilemultipleseller'}</b>
							{else if $product->cache_is_pack}
							&nbsp;-&nbsp;<b>{l s='This parameter depends on the product(s) in the pack.' mod='agilemultipleseller'}</b>
							{/if}
						</label>
						<br /><br />
					</td>
				</tr>
				<tr  {if 1==1 || $product->is_virtual || $product->cache_is_pack}style="display:none;"{/if} class="stockForVirtualProduct">
					<td valign="top" style="vertical-align:top;">
						<input 
							{if $product->depends_on_stock == 1 && $stock_management_active == 1}
								checked="checked" 
							{/if} 
							{if $stock_management_active == 0 || $product->advanced_stock_management == 0 || $product->cache_is_pack}
								disabled="disabled" 
							{/if} 
							type="radio" name="depends_on_stock" class="depends_on_stock" id="depends_on_stock_1" value="1"/>
						<label style="float:none;font-weight:normal" for="depends_on_stock_1">
							{l s='Available quantities for current product and its combinations are based on stock in the warehouses' mod='agilemultipleseller'} 
							{if ($stock_management_active == 0 || $product->advanced_stock_management == 0) && !$product->cache_is_pack}
							&nbsp;-&nbsp;<b>{l s='This requires you to enable the advanced stock management globally or for this product.' mod='agilemultipleseller'}</b>
							{else if $product->cache_is_pack}
							&nbsp;-&nbsp;<b>{l s='This parameter depends on the product(s) in the pack.' mod='agilemultipleseller'}</b>
							{/if}
						</label>
						<br /><br />
					</td>
				</tr>
				
				<tr {if 1==1 || $product->is_virtual || $product->cache_is_pack}style="display:none;"{/if} class="stockForVirtualProduct">
					<td valign="top" style="vertical-align:top;">
						<input style=""
							{if $product->depends_on_stock == 0 || $stock_management_active == 0}
								checked="checked" 
							{/if} 
							type="radio" name="depends_on_stock" class="depends_on_stock" id="depends_on_stock_0" value="0"/>
						<label style="float:none;font-weight:normal" for="depends_on_stock_0">
							{l s='I want to specify available quantities manually' mod='agilemultipleseller'}
						</label>
						<br /><br />
					</td>
				</tr>
				{if isset($pack_quantity)}
				<tr>
					<td valign="top" style="text-align:left;vertical-align:top;">
						<p><b>{l s='When a product has combinations, quantities will be based on the default combination.' mod='agilemultipleseller'}</b></p>
						<p><b>{l s='Given the quantities of the products in this pack, the maximum quantity should be:' mod='agilemultipleseller'} {$pack_quantity}</b></p>
					</td>
				</tr>
				{/if}
				<tr>
					<td valign="top" style="text-align:left;vertical-align:top;">
						<table class="std" >
								<colgroup>
									<col width="50"></col>
									<col></col>
								</colgroup>
							<thead>
								<tr>
									<th>{l s='Quantity' mod='agilemultipleseller'}</th>
									<th>{l s='Designation' mod='agilemultipleseller'}</th>
								</tr>
							</thead>
							<tbody>
							{foreach from=$attributes item=attribute}
								<tr>
									<td  class="available_quantity" id="qty_{$attribute['id_product_attribute']}">
										<span>{$available_quantity[$attribute['id_product_attribute']]}</span>
										<input type="text" value="{$available_quantity[$attribute['id_product_attribute']]|htmlentities}"/>
									</td>
									<td>{$product_designation[$attribute['id_product_attribute']]}</td>
								</tr>
							{/foreach}
							</tbody>
						</table>
					</td>
				</tr>
				<tr id="when_out_of_stock">
					<td>
						<table style="margin-top: 15px;">
							<tbody>
								<tr>
									<td class="col-left"><label>{l s='When out of stock:' mod='agilemultipleseller'}</label></td>
									<td style="padding-bottom:5px;">
										<input {if $product->out_of_stock == 0} checked="checked" {/if} id="out_of_stock_1" type="radio" checked="checked" value="0" class="out_of_stock" name="out_of_stock">
										<label id="label_out_of_stock_1" class="t" for="out_of_stock_1">{l s='Deny orders' mod='agilemultipleseller'}</label>
                                        <br />
										<input {if $product->out_of_stock == 1} checked="checked" {/if} id="out_of_stock_2" type="radio" value="1" class="out_of_stock" name="out_of_stock">
										<label id="label_out_of_stock_2" class="t" for="out_of_stock_2">{l s='Allow orders' mod='agilemultipleseller'}</label>
                                        <br />
										<input {if $product->out_of_stock == 2} checked="checked" {/if} id="out_of_stock_3" type="radio" value="2" class="out_of_stock" name="out_of_stock">
										<label id="label_out_of_stock_3" class="t" for="out_of_stock_3">
											{l s='Default' mod='agilemultipleseller'}:
											{if $order_out_of_stock == 1}
											<i>{l s='Allow orders' mod='agilemultipleseller'}</i>
											{else}
											<i>{l s='Deny orders' mod='agilemultipleseller'}</i>
											{/if} 
											{l s='as set in Preferences' mod='agilemultipleseller'}
										</label>
									</td>
								</tr>
							</tbody>
						</table>
					</td>
				</tr>
			</tbody>
		</table>
	{else}
		<div class="warn">
			<p>{l s='It is not possible to manage quantities when:' mod='agilemultipleseller'}</p>
			<ul>
				<li>{l s='You are managing all shops.' mod='agilemultipleseller'}</li>
				<li>{l s='You are managing a group of shops where quantities are not shared between all shops of this group.' mod='agilemultipleseller'}</li>
				<li>{l s='You are managing a shop which is in a group where quantities are shared between all shops of this group.' mod='agilemultipleseller'}</li>
			</ul>
		</div>
	{/if}
<div class="separation"></div>
<h4>{l s='Availability settings' mod='agilemultipleseller'}</h4>
<table cellpadding="5">
	{if !$ps_stock_management}
			<tr>
				<td colspan="2">{l s='The stock management is disabled' mod='agilemultipleseller'}</td>
			</tr>
		{/if}
		{if !$has_attribute}
		<tr>
			<td class="col-left"><label>{l s='Minimum quantity:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input size="3" maxlength="6" name="minimal_quantity" id="minimal_quantity" type="text" value="{$product->minimal_quantity|default:1}" />
				<p class="preference_description">{l s='The minimum quantity to buy this product (set to 1 to disable this feature)' mod='agilemultipleseller'}</p>
			</td>
		</tr>
		{/if}
	<tr>
		<td class="col-left"><label>{l s='Displayed text when in-stock:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			{foreach from=$all_languages key=k item=language}
				<div id="available_now_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float:left;">
					<input size="30" type="text" 
						   id="available_now_{$language.id_lang}"
						   name="available_now_{$language.id_lang}"
						   value="{$product->available_now[$language.id_lang]|htmlentitiesUTF8|default:''}"
							/><span class="hint" name="help_box">{l s='Forbidden characters:' mod='agilemultipleseller'} <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>
			{/foreach}
			<div style="display:inline;float:left;">
				<div class="displayed_flag" style="margin: 4px 0 0 4px;" >&nbsp;
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_available_now"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_available_now" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $all_languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('available_now', 'available_now&curren;available_later', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</div>
    	</td>
	</tr>
	<tr>
		<td class="col-left"><label>{l s='Displayed text when allowed to be back-ordered:' mod='agilemultipleseller'}</label></td>
		<td style="padding-bottom:5px;">
			{foreach from=$all_languages key=k item=language}
				<div id="available_later_{$language.id_lang}" style="display: {($language.id_lang == $id_language)? 'block' : 'none'}; float:left;">
					<input size="30" type="text" 
							id="available_later_{$language.id_lang}"
							name="available_later_{$language.id_lang}"
							value="{$product->available_later[$language.id_lang]|htmlentitiesUTF8|default:''}"
							/><span class="hint" name="help_box">{l s='Forbidden characters:' mod='agilemultipleseller'} <>;=#{}<span class="hint-pointer">&nbsp;</span></span>
				</div>
			{/foreach}
			<div style="display:inline;float:left;">
				<div class="displayed_flag" style="margin: 4px 0 0 4px;" >&nbsp;
					<img src="{$base_dir}img/l/{$id_language}.jpg"
						class="pointer"
						id="language_current_available_later"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_available_later" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					{l s='Choose language:'}<br /><br />
					{foreach $all_languages as $language}
							<img src="{$base_dir}img/l/{$language.id_lang}.jpg"
								alt="{$language.name}"
								class="pointer"
								title="{$language.name}"
								onclick="changeMyLanguage('available_later', 'available_now&curren;available_later', {$language.id_lang}, '{$language.iso_code}');" />
					{/foreach}
				</div>
			</div>
		</td>
	</tr>
	{if !$countAttributes}
		<tr>
			<td class="col-left"><label>{l s='Available date:' mod='agilemultipleseller'}</label></td>
			<td style="padding-bottom:5px;">
				<input id="available_date" name="available_date" value="{$product->available_date}" class="datepicker"
					style="text-align: center;" type="text" />
				<p>{l s='The available date when this product is out of stock' mod='agilemultipleseller'}</p>
			</td>
		</tr>
	{/if}
</table>

	<script type="text/javascript">
		$('.datepicker').datepicker({
			prevText: '',
			nextText: '',
			dateFormat: 'yy-mm-dd'
		});

		var showAjaxError = function(msg)
		{
			$('#available_quantity_ajax_error_msg').html(msg);
			$('#available_quantity_ajax_error_msg').show();
			$('#available_quantity_ajax_msg').hide();
			$('#available_quantity_ajax_success_msg').hide();
		};
	
		var showAjaxSuccess = function(msg)
		{
			$('#available_quantity_ajax_success_msg').html(msg);
			$('#available_quantity_ajax_error_msg').hide();
			$('#available_quantity_ajax_msg').hide();
			$('#available_quantity_ajax_success_msg').show();
		};
	
		var showAjaxMsg = function(msg)
		{
			$('#available_quantity_ajax_msg').html(msg);
			$('#available_quantity_ajax_error_msg').hide();
			$('#available_quantity_ajax_msg').show();
			$('#available_quantity_ajax_success_msg').hide();
		};
	
		var ajaxCall = function(data)
		{
			data.ajaxProductQuantity = 1;
			data.id_product = '{$product->id}';
			data.ajax = 1;
			data.action = "productQuantity";
			showAjaxMsg('{l s='Saving data...'}');
			$.ajax({
				type: "POST",
				url: "{$base_dir_ssl}modules/agilemultipleseller/ajax_products.php",
				data: data,
				dataType: 'json',
				async : true,
				success: function(msg)
				{
					if (msg.error)
					{
						showAjaxError(msg.error);
						return;
					}
					showAjaxSuccess('{l s='Data saved'}');
				},
				error: function(msg)
				{
					showAjaxError(msg.error);
				}
			});
		};
	
		var refreshQtyAvaibilityForm = function()
		{
			if ($('#depends_on_stock_0').attr('checked'))
			{
				$('.available_quantity').find('input').show();
				$('.available_quantity').find('span').hide();
			}
			else
			{
				$('.available_quantity').find('input').hide();
				$('.available_quantity').find('span').show();
			}
		};
	
		$('.depends_on_stock').click(function(e)
		{
			refreshQtyAvaibilityForm();
			ajaxCall( { actionQty: 'depends_on_stock', value: $(this).val() } );
			if($(this).val() == 0)
				$('.available_quantity input').trigger('change');
		});

		$('.advanced_stock_management').click(function(e)
		{
			var val = 0;
			if ($(this).attr('checked'))
				val = 1;
			
			ajaxCall( { actionQty: 'advanced_stock_management', value: val } );
			if (val == 1)
			{
				$(this).val(1);
				$('#depends_on_stock_1').attr('disabled', false);
			}
			else
			{
				$(this).val(0);
				$('#depends_on_stock_1').attr('disabled', true);
				$('#depends_on_stock_0').attr('checked', true);
				ajaxCall( { actionQty: 'depends_on_stock', value: 0} );
				refreshQtyAvaibilityForm();
			}
			refreshQtyAvaibilityForm();
		});
	
		// bind enter key event on search field
		$('.available_quantity').find('input').bind('keypress', function(e) {
			var code = (e.keyCode ? e.keyCode : e.which);
			if(code == 13) { //Enter keycode
				e.stopPropagation();//Stop event propagation
				return false;
			}
		});
	
		$('.available_quantity').find('input').change(function(e, init_val)
		{
			ajaxCall( { actionQty: 'set_qty', id_product_attribute: $(this).parent().attr('id').split('_')[1], value: $(this).val() } );
		});
	
		$('.out_of_stock').click(function(e)
		{
			refreshQtyAvaibilityForm();
			ajaxCall( { actionQty: 'out_of_stock', value: $(this).val() } );
		});
	
		refreshQtyAvaibilityForm();
	</script>
    <p style="float:right;padding:20px;">
        <input type="submit" class="button" name="submitQuantities" value="{l s='   Save   ' mod='agilemultipleseller'}" />
    </p>

{/if}