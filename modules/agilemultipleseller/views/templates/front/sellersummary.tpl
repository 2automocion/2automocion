{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilemultipleseller'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilemultipleseller'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='My Seller Account' mod='agilemultipleseller'}</h1>
{include file="$tpl_dir./errors.tpl"}

{include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}
<script type="text/javascript" src="{$base_dir_ssl}/modules/agilemultipleseller/js/sellersummary.js"></script>

<script type="text/javascript">
	var membership_module_integrated = {$membership_module_integrated};
    var msg = "{l s='You must agree on Seller Terms & Conditions' mod='agilemultipleseller'}";
	var mymembership_url = "{$link->getModuleLink('agilemembership', 'mymembership', [content_only=>1], true)}";

	$('document').ready(function() {		 $("a#seller_terms").fancybox({
	            'type' : 'iframe',
	            'width':600,
	            'height':600
	        });	
	});

</script>

{if isset($seller_exists) AND $seller_exists}
    <h3>{l s='Your account summary' mod='agilemultipleseller'}</h3>
	<form action="{$link->getModuleLink('agilemultipleseller', 'sellersummary', [], true)}" method="post" class="std" id="frmConvertingPayment">
	<table width="100%" border="0">
		<tr><td width="25%">
			<label  for="seller_status">{l s='Seller Account Status' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td colspan="2">
			<span id="seller_status">{if $seller->active}<img src="{$base_dir_ssl}img/admin/enabled.gif" />&nbsp;&nbsp;{l s='Active' mod='agilemultipleseller'}{else}<img src="{$base_dir_ssl}img/admin/disabled.gif" />&nbsp;{l s='Inactive' mod='agilemultipleseller'}{/if}</span>
		</td></tr>
		<tr><td style="height:10px;"></td></tr>
		<tr><td>
			<label for="products_stat">{l s='Products Listed' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td colspan="2">
			<span id="products_stat">{$num_products}</span>
		</td></tr>
		<tr><td style="height:10px;"></td></tr>
		<tr><td>
			<label for="orders_stat">{l s='Orders Received' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td colspan="2">
			<span id="orders_stat">{$num_orders}</span>
		</td></tr>
		<tr><td style="height:10px;"></td></tr>
		<tr><td>
			<label for="sales_stat">{l s='Total Amount Sold' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td colspan="2">
			<span id="sales_stat">{$currency->sign}&nbsp;{$total_amount_sold}</span>
		</td></tr>
		{if $is_seller_commission_installed}
		<tr><td style="height:10px;"></td></tr>
		<tr><td>
			<label for="acct_baance">{l s='Account Balance' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td width="10%">
			<span id="acct_baance">{$currency->sign}&nbsp;{$account_balance}</span>&nbsp;&nbsp;
		</td><td>
		{if isset($isSeller) && $isSeller}			
			{if $account_balance > 0}
				{if $is_agileprepaidcredit_installed}
				<a id="show_messagebox" onclick="validate_message('B2T','{$request_B2T}')" class="button" href="#confirm_submit">{$request_B2T}</a>
				{*
				<a id="show_messagebox" onclick="validate_message('MPR','{$request_MPR}')" class="button" href="#confirm_submit">{$request_MPR}</a>
				*}
				<input type="text" name="amount_to_convert" id="amount_to_convert" size="5" value="">
				{/if}
			{/if}
			{if $account_balance < 0}
				<span>{l s='You owe store this amount.' mod='agilemultipleseller'}&nbsp;</span>
				<span><a href="{$paycommission_url}">{l s='pay account balance now' mod='agilemultipleseller'}&nbsp;<img src="{$base_dir_ssl}modules/agilesellercommission/img/pay.png"></a></span>
			{/if}
		{/if}
		</td></tr>
		{/if}

		{if $is_agileprepaidcredit_installed}
		<tr><td style="height:10px;"></td></tr>
		<tr><td>
			<label for="acct_baance">{l s='Token Balance' mod='agilemultipleseller'}:&nbsp;</label>
		</td><td>
			<span id="acct_baance">{$token_balance}</span>&nbsp;&nbsp;
		</td><td>
		{if isset($isSeller) && $isSeller}			
			{if $token_balance > 0}
				{if $is_seller_commission_installed}
				<a id="show_messagebox" onclick="validate_message('T2B','{$request_T2B}')" class="button" href="#confirm_submit">{$request_T2B}</a>
				<input type="text" name="tokens_to_convert" id="tokens_to_convert" size="5" value="">
				{/if}
			{/if}
		{/if}
			<div style="display:none">
				<input type="hidden" name="submitRequest" id="submitRequest" value="">
				<div id="confirm_submit">
					<span id="msg_comfirm"><img width="20" height="20" src="{$base_dir_ssl}modules/agilemultipleseller/images/icon-info.png">&nbsp;{l s='Are you sure want to perform ' mod='agilemultipleseller'}<span id="msg_request_1">??2</span></span>
					<span id="msg_error"><img width="20" height="20" src="{$base_dir_ssl}modules/agilemultipleseller/images/icon-error.png">&nbsp;{l s='Please enter correct amount for ' mod='agilemultipleseller'}<span id="msg_request_2">??1</span></span>
					<br><br><br>
					<center>
						<input type="button" class="button" name="btnYes" id="btnYes" onclick="fb_yesclick()" value="{l s='Yes' mod='agilemultipleseller'}">&nbsp;
						<input type="button" class="button" name="btnNo" id="btnNo"  onclick="fb_noclick()" value="  {l s='No' mod='agilemultipleseller'} ">
						<input type="button" class="button" name="btnOK" id="btnOK" onclick="fb_okclick()" value="{l s='OK' mod='agilemultipleseller'}">&nbsp;
					</center>
				</div>
			</div>
		</td></tr>
		{/if}
	</table>
	</form>
	{if isset($ams_custom_selllersummarybag) && $ams_custom_selllersummarybag==1}{include file="$agilemultipleseller_custom./selllersummarybag/sellersummarybag.tpl"}{/if}

{else}
	<form action="{$link->getModuleLink('agilemultipleseller', 'sellersummary', [], true)}" method="post" class="std" id="frmSellerSummary">
	<p>
    <span>{l s='You do not yet have a seller account.' mod='agilemultipleseller'}</span><br />
    {l s='Once you register for a seller account, you will be able to list your products in this store upon approval.' mod='agilemultipleseller'}<br />
    </p>
    <p class="required"  style="margin:10px;">
        {l s='Do you want create a seller account now so that you can list your products for sale?' mod='agilemultipleseller'}<br />
    </p>
    {if isset($id_cms_seller_terms) AND $id_cms_seller_terms >0}
		<input type="checkbox" name="iagree" id="iagree">&nbsp;&nbsp;{l s='I have read and agree on the ' mod='agilemultipleseller'}
		<a href="{$link_terms}" id="seller_terms" name="seller_terms">{l s='Seller Terms & conditions(read)' mod='agilemultipleseller'}</a><br>
	{/if}
    <center>
		<input type="hidden" name="submitSellerAccount" id="submitSellerAccount" value="1">
        <input type="button" class="button" onclick="check_terms({$id_cms_seller_terms})" name="submitSellerAccount" value="{l s='Yes, Sign me up' mod='agilemultipleseller'}" />
    </center>
	<br><br>
	</form>
{/if}

{* ============= Membership integration ============ *}
<fieldset id="fsMymembershipInfo" style="display:none;">
	<br><br>
	<div id="divMymembershipInfo"></div>
</fieldset>

{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}
