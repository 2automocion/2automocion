{capture name=path}<a href="{$link->getPageLink('my-account', true)}">{l s='My Account' mod='agilemultipleseller'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='My Seller Account'  mod='agilemultipleseller'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h1>{l s='My Seller Account' mod='agilemultipleseller'}</h1>
{include file="$agilemultipleseller_views./templates/front/seller_tabs.tpl"}
<br />
{include file="$tpl_dir./errors.tpl"}
<script type="text/javascript">
	var base_dir = "{$base_dir_ssl}";
	var id_product = {$id_product};
</script>
{if isset($isSeller) AND $isSeller AND ($id_product>0 OR !$is_list_limited)}
    <form action="{$link->getModuleLink('agilemultipleseller', 'sellerproductdetail', ['id_product'=>$id_product], true)}" enctype="multipart/form-data" method="post" class="std">

	<div style="display:{if $hasOwnerShip}{else}none;{/if}">
        {include file="$agilemultipleseller_views./templates/front/products/product_top.tpl"}
        {if $product_menu == 1}
        {include file="$agilemultipleseller_views./templates/front/products/informations.tpl"}
        {else if $product_menu == 2}
        {include file="$agilemultipleseller_views./templates/front/products/images.tpl"}
        {else if $product_menu == 3}
        {include file="$agilemultipleseller_views./templates/front/products/features.tpl"}
        {else if $product_menu == 4}
        {include file="$agilemultipleseller_views./templates/front/products/associations.tpl"}
        {else if $product_menu == 5}
        {include file="$agilemultipleseller_views./templates/front/products/prices.tpl"}
        {else if $product_menu == 6}
        {include file="$agilemultipleseller_views./templates/front/products/quantites.tpl"}
        {else if $product_menu == 7}
        {include file="$agilemultipleseller_views./templates/front/products/combinations.tpl"}
        {else if $product_menu == 8}
        {include file="$agilemultipleseller_views./templates/front/products/virtualproduct.tpl"}
        {else if $product_menu == 9}
        {include file="$agilemultipleseller_views./templates/front/products/shipping.tpl"}
        {else if $product_menu == 10}
        {include file="$agilemultipleseller_views./templates/front/products/attachments.tpl"}
		{/if}
	</div>
    </form>
    <br />
{/if}

{include file="$agilemultipleseller_views./templates/front/seller_footer.tpl"}

