<form action="{$base_dir_ssl}{$admin_folder_name}/index.php?controller=adminlogin" id="seller_login_form" method="post">
<input type="hidden" name="ams_seller_email" value="{$selleremail}" />
<input type="hidden" name="ams_seller_token" value="{$sellertoken}" />
<input type="hidden" name="seller_login" value="1" />
<input type="hidden" name="submitLogin" value="1" />
</form>

<script type="text/javascript" language="javascript">
	function seller_login() 
	{ldelim}
		$("#seller_login_form").submit();
	{rdelim}
</script>

<ul class="footer_links">
    <li><a href="{$link->getPageLink('my-account', true)}"><img src="{$img_dir}icon/my-account.gif" alt="" class="icon" /></a><a href="{$link->getPageLink('my-account', true)}">{l s='Back to your account' mod='agilemultipleseller'}</a></li>
    <li><a href="{$base_dir_ssl}"><img src="{$img_dir}icon/home.gif" alt="" class="icon" /></a><a href="{$base_dir_ssl}">{l s='Home' mod='agilemultipleseller' mod='agilemultipleseller'}</a></li>
    {if $isSeller}
	<li><a href="#" onclick="seller_login()"><img src="{$img_dir}icon/my-account.gif" alt="" class="icon" /></a><a href="#" onclick="seller_login()">{l s='Please click here to access your seller account at back office.' mod='agilemultipleseller'}</a></li>
	{/if}
</ul>

