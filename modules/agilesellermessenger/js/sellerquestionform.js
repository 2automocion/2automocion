$(document).ready(function () {
    //set timer for fixxing Google map issues at multiple seller module - Sellr Info tab 
    if (ispostbacksellerquestion) window.setTimeout("gosellerquestiontab()", 1000);

    $('div#idTab12 div[id=pagination] a').click(function () {
        var url = $(this).attr("href");
        var p = getQuerystringParam(url, "p", "");
        if (p == "") p = 1;
        newurl = creatSellerQustionUrl(p, request_sellerquestion_n);
///alert("Click2:" + newurl);
        window.location.href = newurl;
        return false;
    });

    //hook page size form submit event (override existing one)
    $('div#idTab12 div[id=pagination] form').submit(function () {
        var n = $('select#nb_item').val();
        url = creatSellerQustionUrl(1, n);
        window.location.href = url;
        return false;
    });

});


function gosellerquestiontab() {
    $("#more_info_block ul").idTabs("idTab12");
}

function creatSellerQustionUrl(p, n) {
    return request_sellerquestion_uri + '&p=' + p + '&n=' + n + '&ispostbacksellerquestion=1';
}
