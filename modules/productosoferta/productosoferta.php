<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
class Productosoferta extends Module
{
	public function __construct()
	{
		
		$this->name = 'productosoferta';
		$this->tab = 'front_office_features';
		$this->version = '1.0';

		parent::__construct();

		$this->displayName = $this->l('productosoferta');
		$this->description = $this->l('Show information productosoferta');
	}
	
	public function install()
	{
		return parent::install() || $this->registerHook('displayHeader') || $this->registerHook('displayHome');
	}
	
	
	public function hookDisplayHeader()
	{
		
		$pos1 = stripos($_SERVER["REQUEST_URI"], 'category');
		
		if ($pos1 === false){
		
			$this->context->controller->addJS(($this->_path).'js/productosoferta.js');
			$this->context->controller->addCSS(($this->_path).'css/productosoferta.css', 'all');
		
		}
		
	}
	
	
	public function consulta(){
		
		$productos = Product::getSimpleProductsOferta((int)Context::getContext()->language->id,null);
		$contador   = 0;
	
		foreach($productos as $product){
		
			$productos[$contador]['link'] = _PS_BASE_URL_.__PS_BASE_URI__.'index.php?id_product='.$product['id_product'].'&controller=product&id_lang='.(int)Context::getContext()->language->id;
			//$productos[$contador]['id_image'] = $product['id_product'];
			$contador++;
		
		}
		
		
		
		$this->smarty->assign('productos',$productos);
		
	}
	
	public function hookHome($params){
		
		$this->consulta();
		
		$id_lang = (int)$this->context->language->id;
		
		$path = $this->_path;
		$this->smarty->assign('path',$path);
		$base_url = _PS_BASE_URL_.__PS_BASE_URI__;
		$this->smarty->assign('base_url',$base_url);
		$this->smarty->assign('id_lang'.(int)$this->context->language->id);
		
		return $this->display(__FILE__, 'productosoferta.tpl');
		
		
	}
	
	
}

?>
