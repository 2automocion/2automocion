<?php
/*
* 2007-2012 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2012 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;


class Loginar extends Module
{
	
	public $contador;
	public $page_name = '';
	
	public $coches 				   = array();
	public $modelos				   = array();
	public $versiones			   = array();
	public $carrocerias			   = array();
	public $fechas_inicio		   = array();
	public $fechas_fin			   = array();
	public $productos 			   = array();
	
	public $contadorCoches 		   = 0;
	public $contadorModelos 	   = 0;
	public $contadorVersiones      = 0;
	public $contadorCarrocerias    = 0;
	public $contadorFechas_inicio  = 0;
	public $contadorFechas_fin     = 0;
	public $contadorProductos      = 0;
	

    public function __construct()
    {
        
    	$this->page_name = Dispatcher::getInstance()->getController();
		$this->page_name = (preg_match('/^[0-9]/', $this->page_name)) ? 'page_'.$this->page_name : $this->page_name;
		
    	$this->name = 'loginar';
        $this->tab = 'front_office_features';
        $this->version = 1.0;
		$this->author = 'PrestaShop';
		$this->need_instance = 0;

        parent::__construct();

		$this->displayName = $this->l('Loginar block');
        $this->description = $this->l('Display a login block');
       
        
    }
	 
    
	public function install()
	{
		
		Configuration::updateValue('MANUFACTURER_DISPLAY_TEXT', true);
		Configuration::updateValue('MANUFACTURER_DISPLAY_TEXT_NB', 5);
		Configuration::updateValue('MANUFACTURER_DISPLAY_FORM', true);
		
        return parent::install() || $this->registerHook('header') || $this->registerHook('top') || $this->registerHook('left');
        
    }

	
	
	public function hookHeader($params)
	{
		
		global $smarty;
		
		$pos3 = strpos($_SERVER["REQUEST_URI"], "controller=authentication");
		
		if ($pos3 === false){
			
			$this->context->controller->addCSS(($this->_path).'loginar.css', 'all');
			
		}
		
		
	}
	
	public function hookTop($params){
		
		
		
			
	}
	
	
	public function hookLeftColumn($params){
		
		$pos3 = strpos($_SERVER["REQUEST_URI"], "controller=authentication");
		
		if ($pos3 === false){

			return $this->display(__FILE__, 'loginar.tpl');
			
		}
		
		
		
	}
	
	
	
	
	
	
	
}
