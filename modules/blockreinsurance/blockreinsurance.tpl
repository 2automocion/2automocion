{*
* 2007-2013 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2013 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $infos|@count > 0}
<!-- MODULE Block reinsurance -->
<div id="reinsurance_block" class="clearfix">
	<ul class="width{$nbblocks}">	
		<li><img src="{$module_dir}img/1.png" alt="piezas originales" title="piezas originales" /><span id="span1">Piezas adaptables</span><span id="span2">nuevas y con garant&iacute;ia</span></li>
		<li><img src="{$module_dir}img/2.png" alt="todas las piezas" title="todas las piezas" /><span id="span1">Todas los productos</span><span id="span2">y al mejor precio</span></li>
		<li><img src="{$module_dir}img/3.png" alt="le enviamos su pieza" title="le enviamos su pieza" /><span id="span1">Le enviamos su pieza</span><span id="span2">a su empresa en 24h</span></li>
		<li><img src="{$module_dir}img/4.png" alt="compre en tres" title="compre en tres" /><span id="span1">Compre en tres</span><span id="span2">sencillos pasos</span></li>
		<li><img src="{$module_dir}img/5.png" alt="abierto los" title="abierto los" /><span id="span1">Abierto los</span><span id="span2">365 d&iacute;as del a&ntilde;o</span></li>
		<!--
		{*foreach from=$infos item=info*}
			<li><img src="{$link->getMediaLink("`$module_dir`img/`$info.file_name|escape:'htmlall':'UTF-8'`")}" alt="{$info.text|escape:html:'UTF-8'}" title="{$info.text|escape:html:'UTF-8'}" /><span>{$info.text|escape:html:'UTF-8'}</span></li>
		{*/foreach*}
		-->
	</ul>
</div>
<!-- /MODULE Block reinsurance -->
{/if}