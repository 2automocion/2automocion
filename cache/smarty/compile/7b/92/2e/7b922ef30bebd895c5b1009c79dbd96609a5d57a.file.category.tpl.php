<?php /* Smarty version Smarty-3.1.14, created on 2015-11-16 11:40:06
         compiled from "C:\online\prestashop1562\themes\default\category.tpl" */ ?>
<?php /*%%SmartyHeaderCode:74965649b2865dea01-44862452%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7b922ef30bebd895c5b1009c79dbd96609a5d57a' => 
    array (
      0 => 'C:\\online\\prestashop1562\\themes\\default\\category.tpl',
      1 => 1447546148,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '74965649b2865dea01-44862452',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'categoryNameComplement' => 0,
    'scenes' => 0,
    'link' => 0,
    'categorySize' => 0,
    'description_short' => 0,
    'subcategories' => 0,
    'products' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5649b286961359_85586388',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5649b286961359_85586388')) {function content_5649b286961359_85586388($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include 'C:\\online\\prestashop1562\\tools\\smarty\\plugins\\modifier.escape.php';
?>

<script>

	$(document).ready(function(){

		$("#center_column").css('margin-right','10px');
		$("#center_column").css('width','76%');	
		$(".breadcrumb").css('margin-bottom','10px');
		$(".breadcrumb").css('font-size','12px');
		$(".breadcrumb").css('border','1px solid #82bc00');
		$(".breadcrumb").css('border-radius','5px');
		$(".breadcrumb").css('padding-bottom','10px');
	
	});

</script>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./errors.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<?php if (isset($_smarty_tpl->tpl_vars['category']->value)){?>
	<?php if ($_smarty_tpl->tpl_vars['category']->value->id&&$_smarty_tpl->tpl_vars['category']->value->active){?>
		<h1>
			<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['category']->value->name, 'htmlall', 'UTF-8');?>
<?php if (isset($_smarty_tpl->tpl_vars['categoryNameComplement']->value)){?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['categoryNameComplement']->value, 'htmlall', 'UTF-8');?>
<?php }?>
		</h1>
		
		<div class="resumecat category-product-count">
			<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./category-count.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

		</div>
		
		<?php if ($_smarty_tpl->tpl_vars['scenes']->value||$_smarty_tpl->tpl_vars['category']->value->description||$_smarty_tpl->tpl_vars['category']->value->id_image){?>
		
		<div class="content_scene_cat">
			<?php if ($_smarty_tpl->tpl_vars['scenes']->value){?>
				<!-- Scenes -->
				<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./scenes.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('scenes'=>$_smarty_tpl->tpl_vars['scenes']->value), 0);?>

			<?php }else{ ?>
				<!-- Category image -->
				<?php if ($_smarty_tpl->tpl_vars['category']->value->id_image){?>
				<div class="align_center">
					<img src="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getCatImageLink($_smarty_tpl->tpl_vars['category']->value->link_rewrite,$_smarty_tpl->tpl_vars['category']->value->id_image,'category_default'), ENT_QUOTES, 'UTF-8', true);?>
" alt="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['category']->value->name, 'htmlall', 'UTF-8');?>
" title="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['category']->value->name, 'htmlall', 'UTF-8');?>
" id="categoryImage" width="<?php echo $_smarty_tpl->tpl_vars['categorySize']->value['width'];?>
" height="<?php echo $_smarty_tpl->tpl_vars['categorySize']->value['height'];?>
" />
				</div>
				<?php }?>
			<?php }?>

			<?php if ($_smarty_tpl->tpl_vars['category']->value->description){?>
				<div class="cat_desc">
				<?php if (strlen($_smarty_tpl->tpl_vars['category']->value->description)>120){?>
					<div id="category_description_short"><?php echo $_smarty_tpl->tpl_vars['description_short']->value;?>
</div>
					<div id="category_description_full" style="display:none;"><?php echo $_smarty_tpl->tpl_vars['category']->value->description;?>
</div>
					<a href="#" onclick="$('#category_description_short').hide(); $('#category_description_full').show(); $(this).hide(); return false;" class="lnk_more"><?php echo smartyTranslate(array('s'=>'More'),$_smarty_tpl);?>
</a>
				<?php }else{ ?>
					<div><?php echo $_smarty_tpl->tpl_vars['category']->value->description;?>
</div>
				<?php }?>
				</div>
			<?php }?>
		</div>
		
		<?php }?>
		
		<?php if (isset($_smarty_tpl->tpl_vars['subcategories']->value)){?>
		
		<!-- Subcategories -->
		<!--
		<div id="subcategories">
			
			<h3></h3>
			<ul class="inline_list">
			
			
			
				<li class="clearfix">
					
					<a href="" title="" class="img">
						
							<img src="" alt="" width="" height="" />
						
							<img src="default-medium_default.jpg" alt="" width="" height="" />
						
					</a>
					
					<a href="" class="cat_name"></a>
					
					
						<p class="cat_desc"></p>
					
					
				</li>
				
			
			
			</ul>
			<br class="clear"/>
		</div>
		-->
		
		<?php }?>

		<?php if ($_smarty_tpl->tpl_vars['products']->value){?>
			
			<div class="content_sortPagiBar">
				
				
				
				
				<div class="sortPagiBar clearfix">
				
					
					
					<?php echo $_smarty_tpl->getSubTemplate ("./nbr-product-page.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				
				</div>
				
				
			</div>
			
			<?php echo $_smarty_tpl->getSubTemplate ("./product-list.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('products'=>$_smarty_tpl->tpl_vars['products']->value), 0);?>

			
			
			
			<div class="content_sortPagiBar">
				
				
				<div class="sortPagiBar clearfix">
					
					
					
				</div>
				
				<?php echo $_smarty_tpl->getSubTemplate ("./pagination.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array('paginationId'=>'bottom'), 0);?>

			
			</div>
			
		<?php }?>
		
	<?php }elseif($_smarty_tpl->tpl_vars['category']->value->id){?>
		<p class="warning"><?php echo smartyTranslate(array('s'=>'This category is currently unavailable.'),$_smarty_tpl);?>
</p>
	<?php }?>
	
<?php }?>
<?php }} ?>