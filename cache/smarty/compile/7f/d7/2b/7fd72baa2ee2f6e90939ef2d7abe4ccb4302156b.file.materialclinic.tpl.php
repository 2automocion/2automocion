<?php /* Smarty version Smarty-3.1.14, created on 2015-11-22 09:58:46
         compiled from "/home/automoci/public_html/themes/default/materialclinic.tpl" */ ?>
<?php /*%%SmartyHeaderCode:1482337687565183c6183b28-98642934%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '7fd72baa2ee2f6e90939ef2d7abe4ccb4302156b' => 
    array (
      0 => '/home/automoci/public_html/themes/default/materialclinic.tpl',
      1 => 1447764739,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '1482337687565183c6183b28-98642934',
  'function' => 
  array (
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_565183c61abc71_24938924',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_565183c61abc71_24938924')) {function content_565183c61abc71_24938924($_smarty_tpl) {?>


<script>

	$(document).ready(function(){
		
		$("#center_column").css("width","77%");
		$("#center_column").css("marginRight","0px");
		$("#center_column").css("height","1050px");
		$("#center_column").css('border','1px solid #82bc00');
		$("#center_column").css('backgroundColor','#ebf4d6');
	
	});

</script>

<?php $_smarty_tpl->_capture_stack[0][] = array('path', null, null); ob_start(); ?>Dosa<?php list($_capture_buffer, $_capture_assign, $_capture_append) = array_pop($_smarty_tpl->_capture_stack[0]);
if (!empty($_capture_buffer)) {
 if (isset($_capture_assign)) $_smarty_tpl->assign($_capture_assign, ob_get_contents());
 if (isset( $_capture_append)) $_smarty_tpl->append( $_capture_append, ob_get_contents());
 Smarty::$_smarty_vars['capture'][$_capture_buffer]=ob_get_clean();
} else $_smarty_tpl->capture_error();?>
<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['tpl_dir']->value)."./breadcrumb.tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>


<h1>Quienes somos</h1>

<div id="contenedorReciclinic">
	
	<h3>Distribuciones Dosa Automoci&oacute;n, S.L., es una empresa de nueva creaci&oacute;n, fundada por profesionales con una larga experiencia en el sector del recambio del autom&oacute;vil.</h3>
	
	<p>Nuestros clientes son los principales <span id="business">recambistas espa&ntilde;oles.</span></p>
	
	<p>Nuestra filosof&iacute;a se divide en dos simples puntos:</p>
	
	<ol>
	
		<li>Voluntad de dar el mejor servicio al cliente.</li>
		<li>Constante mejora de nuestra gesti&oacute;n para ofrecer los precios m&aacute;s competitivos.</li>
	
	</ol>
	
	
	
	<div id="left">
		<div id="map"></div>
	</div>
	
	<div id="right">
		
		<h3>DOSA AUTOMOCI&Oacute;N, S.L.</h3>
		<p>Avda. Mare de D&eacute;u de N&uacute;ria, 23B , Pol. Ind. Can Calder&oacute;n</p> 
		<p>08830 Sant Boi de Llobregat, Barcelona, Espa&ntilde;a</p> 
		<p>CIF B66276460</p>
		<!--<p>&nbsp;</p>-->
		<p>Tel. +34 936.309.223 / +34 936.400.129</p>
		<p>Fax. +34 936 409 964</p> 
		<!--<p>&nbsp;</p>-->
		<p>email:&nbsp;<a href="mailto:info@dosa.com">info@dosa.com</a></p>
		<p>website:&nbsp;<a href="http://www.2automocion.com">http://www.2automocion.com</a></p>
		<!--<p>&nbsp;</p>-->
		<!--
		<p>Inscrita en el Registro Mercantil de Barcelona, T. 44266, F.451448, S. 8, Fecha de inscripción  2014-04-14, fechaPublicacion 2014-04-22 
		<p>213515. Fecha inscripci&oacute;n 18/04/2000.</p>
		-->
		
		
		
	</div>
	
	<br id="clear">
	
	
	
	
</div>

<script type="text/javascript">
		
		var roadStyle = {
			strokeColor: "#FFFF00",
			strokeWeight: 7,
			strokeOpacity: 0.75
		};
		
		var addressStyle = {
			icon: "img/marker-house.png"
		};
		
		var parcelStyle = {
			strokeColor: "#FF7800",
			strokeOpacity: 1,
			strokeWeight: 2,
			fillColor: "#46461F",
			fillOpacity: 0.25
		};
		
		var map;
		currentFeature_or_Features = null;
		
		var geojson_Point = {
			"type": "Point",
			"coordinates": [41.361832, 2.103009]
		};
		
		var infowindow = new google.maps.InfoWindow();
		
		function init(){
			
			map = new google.maps.Map(document.getElementById('map'),{
				zoom: 17,
				center: new google.maps.LatLng(41.503761, 2.344225),
				mapTypeId: google.maps.MapTypeId.ROADMAP
			});
			
			
		}
		
		function clearMap(){
			
			if (!currentFeature_or_Features)
				return;
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(null);
						}
					}
					else{
						currentFeature_or_Features[i].setMap(null);
					}
				}
			}else{
				currentFeature_or_Features.setMap(null);
			}
			if (infowindow.getMap()){
				infowindow.close();
			}
		}
		
		function showFeature(geojson, style){
			
			clearMap();
			
			currentFeature_or_Features = new GeoJSON(geojson, style || null);
			
			if (currentFeature_or_Features.type && currentFeature_or_Features.type == "Error"){
				document.getElementById("put_geojson_string_here").value = currentFeature_or_Features.message;
				return;
			}
			
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(map);
							if(currentFeature_or_Features[i][j].geojsonProperties) {
								setInfoWindow(currentFeature_or_Features[i][j]);
							}
						}
					}
					else{
						currentFeature_or_Features[i].setMap(map);
					}
					if (currentFeature_or_Features[i].geojsonProperties) {
						setInfoWindow(currentFeature_or_Features[i]);
					}
				}
			}else{
				currentFeature_or_Features.setMap(map)
				if (currentFeature_or_Features.geojsonProperties) {
					setInfoWindow(currentFeature_or_Features);
				}
			}
			
			document.getElementById("put_geojson_string_here").value = JSON.stringify(geojson);
		}
		
		function rawGeoJSON(){
			clearMap();
			currentFeature_or_Features = new GeoJSON(JSON.parse(document.getElementById("put_geojson_string_here").value));
			if (currentFeature_or_Features.length){
				for (var i = 0; i < currentFeature_or_Features.length; i++){
					if(currentFeature_or_Features[i].length){
						for(var j = 0; j < currentFeature_or_Features[i].length; j++){
							currentFeature_or_Features[i][j].setMap(map);
							if(currentFeature_or_Features[i][j].geojsonProperties) {
								setInfoWindow(currentFeature_or_Features[i][j]);
							}
						}
					}
					else{
						currentFeature_or_Features[i].setMap(map);
					}
					if (currentFeature_or_Features[i].geojsonProperties) {
						setInfoWindow(currentFeature_or_Features[i]);
					}
				}
			}else{
				currentFeature_or_Features.setMap(map);
				if (currentFeature_or_Features.geojsonProperties) {
					setInfoWindow(currentFeature_or_Features);
				}
			}
		}
		
		function setInfoWindow (feature) {
			google.maps.event.addListener(feature, "click", function(event) {
				var content = "<div id='infoBox'><strong>GeoJSON Feature Properties</strong><br />";
				for (var j in this.geojsonProperties) {
					content += j + ": " + this.geojsonProperties[j] + "<br />";
				}
				content += "</div>";
				infowindow.setContent(content);
				infowindow.setPosition(event.latLng);
				infowindow.open(map);
			});
		}
		
		init();
		showFeature(geojson_Point);
		
</script>
<?php }} ?>