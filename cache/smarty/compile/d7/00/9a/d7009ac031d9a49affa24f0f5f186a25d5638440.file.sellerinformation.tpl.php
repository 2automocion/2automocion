<?php /* Smarty version Smarty-3.1.14, created on 2015-11-22 12:19:50
         compiled from "/home/automoci/public_html/modules/agilemultipleseller/views/templates/front/sellerinformation.tpl" */ ?>
<?php /*%%SmartyHeaderCode:5843213975651a4d6356162-05393343%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'd7009ac031d9a49affa24f0f5f186a25d5638440' => 
    array (
      0 => '/home/automoci/public_html/modules/agilemultipleseller/views/templates/front/sellerinformation.tpl',
      1 => 1447765642,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '5843213975651a4d6356162-05393343',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'base_dir_ssl' => 0,
    'base_dir' => 0,
    'display_fields' => 0,
    'languages' => 0,
    'language' => 0,
    'id_language' => 0,
    'parameter' => 0,
    'sellerinfo' => 0,
    'countries' => 0,
    'country' => 0,
    'addtional_fields_html' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5651a4d656b383_65920688',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5651a4d656b383_65920688')) {function content_5651a4d656b383_65920688($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/home/automoci/public_html/tools/smarty/plugins/modifier.escape.php';
?><script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
js/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
js/tinymce.inc.js"></script>
<script type="text/javascript" src="<?php echo $_smarty_tpl->tpl_vars['base_dir_ssl']->value;?>
js/admin.js"></script>
<script type="text/javascript">
	function changeMyLanguage(field, fieldsString, id_language_new, iso_code)
	{
		changeLanguage(field, fieldsString, id_language_new, iso_code);
		$("img[id^='language_current_']").attr("src","<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/" + id_language_new + ".jpg");
	}
</script>
<table id="sellerinformation" name="sellerinformation" cellpadding="15" style="display: none;width: 80%;border:dotted 0px gray;"align="center">
<?php if (in_array('company',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px" align="right"><p><label><?php echo smartyTranslate(array('s'=>'Company:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td nowrap> 
		<table>
			<tr>
			<td>
				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					<?php $_smarty_tpl->tpl_vars["parameter"] = new Smarty_variable(("company_").($_smarty_tpl->tpl_vars['language']->value['id_lang']), null, 0);?>
					<div id="company_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" style="display: <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang']==$_smarty_tpl->tpl_vars['id_language']->value ? 'block' : 'none';?>
; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="company_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="company_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" value="<?php if (isset($_POST[$_smarty_tpl->tpl_vars['parameter']->value])){?><?php echo $_POST[$_smarty_tpl->tpl_vars['parameter']->value];?>
<?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['sellerinfo']->value->company!=null&&array_key_exists($_smarty_tpl->tpl_vars['language']->value['id_lang'],$_smarty_tpl->tpl_vars['sellerinfo']->value->company)){?><?php echo $_smarty_tpl->tpl_vars['sellerinfo']->value->company[$_smarty_tpl->tpl_vars['language']->value['id_lang']];?>
<?php }?><?php }?>" /><sup>*</sup>
					</div>
				<?php } ?>
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['id_language']->value;?>
.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_company" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					<?php echo smartyTranslate(array('s'=>'Choose language:'),$_smarty_tpl);?>
<br /><br />
					<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
.jpg"
								alt="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								class="pointer"
								title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								onclick="changeMyLanguage('company', 'company&curren;description&curren;address1&curren;address2&curren;city', <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
, '<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>
');" />
					<?php } ?>
				</div>
			</td></tr>
		</table>
	</td>
</tr>
<?php }?>
<?php if (in_array('address1',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label><?php echo smartyTranslate(array('s'=>'Address1:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					<?php $_smarty_tpl->tpl_vars["parameter"] = new Smarty_variable(("address1_").($_smarty_tpl->tpl_vars['language']->value['id_lang']), null, 0);?>
					<div id="address1_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" style="display: <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang']==$_smarty_tpl->tpl_vars['id_language']->value ? 'block' : 'none';?>
; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="address1_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="address1_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" value="<?php if (isset($_POST[$_smarty_tpl->tpl_vars['parameter']->value])){?><?php echo $_POST[$_smarty_tpl->tpl_vars['parameter']->value];?>
<?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['sellerinfo']->value->address1!=null&&array_key_exists($_smarty_tpl->tpl_vars['language']->value['id_lang'],$_smarty_tpl->tpl_vars['sellerinfo']->value->address1)){?><?php echo $_smarty_tpl->tpl_vars['sellerinfo']->value->address1[$_smarty_tpl->tpl_vars['language']->value['id_lang']];?>
<?php }?><?php }?>" />
					</div>
				<?php } ?>
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['id_language']->value;?>
.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address1" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					<?php echo smartyTranslate(array('s'=>'Choose language:'),$_smarty_tpl);?>
<br /><br />
					<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
.jpg"
								alt="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								class="pointer"
								title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								onclick="changeMyLanguage('address1', 'company&curren;description&curren;address1&curren;address2&curren;city', <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
, '<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>
');" />
					<?php } ?>
				</div>
			</td></tr>
		</table>
	</td>
</tr>
<?php }?>
<?php if (in_array('address2',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label><?php echo smartyTranslate(array('s'=>'Address2:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					<?php $_smarty_tpl->tpl_vars["parameter"] = new Smarty_variable(("address2_").($_smarty_tpl->tpl_vars['language']->value['id_lang']), null, 0);?>
					<div id="address2_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" style="display: <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang']==$_smarty_tpl->tpl_vars['id_language']->value ? 'block' : 'none';?>
; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="address2_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="address2_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" value="<?php if (isset($_POST[$_smarty_tpl->tpl_vars['parameter']->value])){?><?php echo $_POST[$_smarty_tpl->tpl_vars['parameter']->value];?>
<?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['sellerinfo']->value->address2!=null&&array_key_exists($_smarty_tpl->tpl_vars['language']->value['id_lang'],$_smarty_tpl->tpl_vars['sellerinfo']->value->address2)){?><?php echo $_smarty_tpl->tpl_vars['sellerinfo']->value->address2[$_smarty_tpl->tpl_vars['language']->value['id_lang']];?>
<?php }?><?php }?>" />
					</div>
				<?php } ?>
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['id_language']->value;?>
.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_address2" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					<?php echo smartyTranslate(array('s'=>'Choose language:'),$_smarty_tpl);?>
<br /><br />
					<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
						<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
.jpg"
							alt="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
							class="pointer"
							title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
							onclick="changeMyLanguage('address2', 'company&curren;description&curren;address1&curren;address2&curren;city', <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
, '<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>
');" />
					<?php } ?>
				</div>
			</td></tr>
		</table>
	</td>
</tr>
<?php }?>
<?php if (in_array('city',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label><?php echo smartyTranslate(array('s'=>'City:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td nowrap> 
		<table>
			<tr valign="top"><td>
				<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
					<?php $_smarty_tpl->tpl_vars["parameter"] = new Smarty_variable(("city_").($_smarty_tpl->tpl_vars['language']->value['id_lang']), null, 0);?>
					<div id="city_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" style="display: <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang']==$_smarty_tpl->tpl_vars['id_language']->value ? 'block' : 'none';?>
; float: left;">
						&nbsp;<input type="text" style="width:400px;" id="city_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" name="city_<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
" value="<?php if (isset($_POST[$_smarty_tpl->tpl_vars['parameter']->value])){?><?php echo $_POST[$_smarty_tpl->tpl_vars['parameter']->value];?>
<?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['sellerinfo']->value->city!=null&&array_key_exists($_smarty_tpl->tpl_vars['language']->value['id_lang'],$_smarty_tpl->tpl_vars['sellerinfo']->value->city)){?><?php echo $_smarty_tpl->tpl_vars['sellerinfo']->value->city[$_smarty_tpl->tpl_vars['language']->value['id_lang']];?>
<?php }?><?php }?>" />
					</div>
				<?php } ?>
			</td>
			<td>
				<div class="displayed_flag" style="float:left;margin: 4px 0 0 4px;" >
					<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['id_language']->value;?>
.jpg"
						class="pointer"
						id="language_current_name"
						onclick="toggleLanguageFlags(this);" />
				</div>
				<div id="languages_city" class="language_flags" style="display:none; background-color: lightgray; margin: 4px; width: 120px;	border: 1px solid #555;">
					<?php echo smartyTranslate(array('s'=>'Choose language:'),$_smarty_tpl);?>
<br /><br />
					<?php  $_smarty_tpl->tpl_vars['language'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['language']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['languages']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['language']->key => $_smarty_tpl->tpl_vars['language']->value){
$_smarty_tpl->tpl_vars['language']->_loop = true;
?>
							<img src="<?php echo $_smarty_tpl->tpl_vars['base_dir']->value;?>
img/l/<?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
.jpg"
								alt="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								class="pointer"
								title="<?php echo $_smarty_tpl->tpl_vars['language']->value['name'];?>
"
								onclick="changeMyLanguage('city', 'company&curren;description&curren;address1&curren;address2&curren;city', <?php echo $_smarty_tpl->tpl_vars['language']->value['id_lang'];?>
, '<?php echo $_smarty_tpl->tpl_vars['language']->value['iso_code'];?>
');" />
					<?php } ?>
				</div>
			</td></tr>
		</table>
	</td>
</tr>
<?php }?>
<?php if (in_array('postcode',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label><?php echo smartyTranslate(array('s'=>'Zip/Postal Code:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td>  
		&nbsp;<input type="text" style="width:200px;" id="postcode" name="postcode" value="<?php if (isset($_POST['postcode'])){?><?php echo $_POST['postcode'];?>
<?php }else{ ?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['sellerinfo']->value->postcode, 'htmlall', 'UTF-8');?>
<?php }?>" />
	</td>
</tr>
<?php }?>
<?php if (in_array('id_country',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label for="id_country"><?php echo smartyTranslate(array('s'=>'Country:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td>
		<select name="id_country" id="id_country">
			<?php  $_smarty_tpl->tpl_vars['country'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['country']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['countries']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['country']->key => $_smarty_tpl->tpl_vars['country']->value){
$_smarty_tpl->tpl_vars['country']->_loop = true;
?>
				<option value="<?php echo $_smarty_tpl->tpl_vars['country']->value['id_country'];?>
" <?php if (isset($_POST['id_country'])){?><?php if ($_POST['id_country']==$_smarty_tpl->tpl_vars['country']->value['id_country']){?>selected="selected"<?php }?><?php }else{ ?><?php if ($_smarty_tpl->tpl_vars['sellerinfo']->value->id_country==$_smarty_tpl->tpl_vars['country']->value['id_country']){?>selected="selected"<?php }?><?php }?>><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['country']->value['name'], 'htmlall', 'UTF-8');?>
</option>
			<?php } ?>
		</select><sup>*</sup>
	</td>
</tr>
<?php }?>
<?php if (in_array("id_state",$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p class="id_state"><label for="id_state"><?php echo smartyTranslate(array('s'=>'State:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</label></p></td>
	<td nowrap>
		<p class="id_state">
			<select name="id_state" id="id_state">
			</select>
		</p>
	</td>
</tr>
<?php }?>
<?php if (in_array('phone',$_smarty_tpl->tpl_vars['display_fields']->value)){?>
<tr>
	<td style="width:150px;"><p><label for="phone"><?php echo smartyTranslate(array('s'=>'Phone:','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</p></label></td>
	<td>&nbsp;<input type="text" name="phone" id="phone" value="<?php if (isset($_POST['phone'])){?><?php echo $_POST['phone'];?>
<?php }else{ ?><?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['sellerinfo']->value->phone, 'htmlall', 'UTF-8');?>
<?php }?>"/>
	</td>
</tr>
<?php }?>
<?php echo $_smarty_tpl->tpl_vars['addtional_fields_html']->value;?>

<input type="hidden" name="signin" id="signin" value="1"/>
</table><?php }} ?>