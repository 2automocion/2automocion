<?php /* Smarty version Smarty-3.1.14, created on 2015-11-18 15:28:05
         compiled from "/home/automoci/public_html/themes/default/order-opc-new-account.tpl" */ ?>
<?php /*%%SmartyHeaderCode:728266141564c8af54cd022-41251831%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '0c2337c8b0abcb46b9beaa53e993331a49ac30b3' => 
    array (
      0 => '/home/automoci/public_html/themes/default/order-opc-new-account.tpl',
      1 => 1447764742,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '728266141564c8af54cd022-41251831',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'link' => 0,
    'back' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_564c8af54f7156_59171263',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_564c8af54f7156_59171263')) {function content_564c8af54f7156_59171263($_smarty_tpl) {?><?php if (!is_callable('smarty_modifier_escape')) include '/home/automoci/public_html/tools/smarty/plugins/modifier.escape.php';
?><div id="opc_new_account" class="opc-main-block">
	<div id="opc_new_account-overlay" class="opc-overlay" style="display: none;"></div>
	<h2><span>1</span> <?php echo smartyTranslate(array('s'=>'Account'),$_smarty_tpl);?>
</h2>
	<form action="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('authentication',true,null,"back=order-opc"), ENT_QUOTES, 'UTF-8', true);?>
" method="post" id="login_form" class="std">
		<fieldset>
			<h3><?php echo smartyTranslate(array('s'=>'Already registered?'),$_smarty_tpl);?>
</h3>
			<p><a href="#" id="openLoginFormBlock">&raquo; <?php echo smartyTranslate(array('s'=>'Click here'),$_smarty_tpl);?>
</a></p>
			<div id="login_form_content" style="display:none;">
				<!-- Error return block -->
				<div id="opc_login_errors" class="error" style="display:none;"></div>
				<!-- END Error return block -->
				<div style="margin-left:40px;margin-bottom:5px;float:left;width:40%;">
					<label for="login_email"><?php echo smartyTranslate(array('s'=>'Email address'),$_smarty_tpl);?>
</label>
					<span><input type="text" id="login_email" name="email" /></span>
				</div>
				<div style="margin-left:40px;margin-bottom:5px;float:left;width:40%;">
					<label for="login_passwd"><?php echo smartyTranslate(array('s'=>'Password'),$_smarty_tpl);?>
</label>
					<span><input type="password" id="login_passwd" name="login_passwd" /></span>
					<a href="<?php echo htmlspecialchars($_smarty_tpl->tpl_vars['link']->value->getPageLink('password',true), ENT_QUOTES, 'UTF-8', true);?>
" class="lost_password"><?php echo smartyTranslate(array('s'=>'Forgot your password?'),$_smarty_tpl);?>
</a>
				</div>
				<p class="submit">
					<?php if (isset($_smarty_tpl->tpl_vars['back']->value)){?><input type="hidden" class="hidden" name="back" value="<?php echo smarty_modifier_escape($_smarty_tpl->tpl_vars['back']->value, 'htmlall', 'UTF-8');?>
" /><?php }?>
					<input type="submit" id="SubmitLogin" name="SubmitLogin" class="button" value="Acceso" />
				</p>
			</div>
		</fieldset>
	</form>

	<div class="clear"></div>
</div>
<?php }} ?>