<?php /* Smarty version Smarty-3.1.14, created on 2015-11-22 12:19:50
         compiled from "/home/automoci/public_html/modules/agilemultipleseller/views/templates/hook/displaycustomeraccountform.tpl" */ ?>
<?php /*%%SmartyHeaderCode:19454545355651a4d662e8a8-38763744%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'afb275e71b293c1fda03b3111a267dbb710f763b' => 
    array (
      0 => '/home/automoci/public_html/modules/agilemultipleseller/views/templates/hook/displaycustomeraccountform.tpl',
      1 => 1447765644,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '19454545355651a4d662e8a8-38763744',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'id_cms_seller_terms' => 0,
    'link_terms' => 0,
    'seller_sign_up_fields' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5651a4d6666a97_55035248',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5651a4d6666a97_55035248')) {function content_5651a4d6666a97_55035248($_smarty_tpl) {?><script type="text/javascript">
	$(document).ready(function() {
		selleraccountsignup();

		 $("a#seller_terms").fancybox({
	            'type' : 'iframe',
	            'width':600,
	            'height':600
	        });	
	});

	function selleraccountsignup()
	{
		if($("input[id='seller_account_signup']").attr('checked') == 'checked')
		{
			$("table[id='sellerinformation']").show();
		} else
		{
			$("table[id='sellerinformation']").hide();
		}
	}
</script>
<fieldset class="account_creation">
	<h3><?php echo smartyTranslate(array('s'=>'Seller Account','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</h3>
	<p style="padding:5px 20px 10px 50px;">
	    <?php echo smartyTranslate(array('s'=>'If you register for a seller account, you will be able to list your products for sale on this website.','mod'=>'agilemultipleseller'),$_smarty_tpl);?>

	    <?php echo smartyTranslate(array('s'=>'You can also choose to create your seller account at a later time. You can register for your seller account at any time from My Account - My Seller Account page.','mod'=>'agilemultipleseller'),$_smarty_tpl);?>

		<br><br>
	    <input id="seller_account_signup" type="checkbox" name="seller_account_signup" value="1" <?php if (isset($_POST['seller_account_signup'])){?><?php if ($_POST['seller_account_signup']==1){?>checked<?php }?><?php }?> />&nbsp;
		<script type="text/javascript">
			$("input[name='seller_account_signup']").change(function(){
				selleraccountsignup();
			});
		</script>

	    <?php if (isset($_smarty_tpl->tpl_vars['id_cms_seller_terms']->value)&&$_smarty_tpl->tpl_vars['id_cms_seller_terms']->value>0){?>
			<?php echo smartyTranslate(array('s'=>'Yes, I have read and I agree on the Seller Terms & conditions','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
,
	    <?php }?>
	    <?php echo smartyTranslate(array('s'=>'Please create a seller account for me','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
<br>
			<?php if (isset($_smarty_tpl->tpl_vars['id_cms_seller_terms']->value)&&$_smarty_tpl->tpl_vars['id_cms_seller_terms']->value>0){?>
			</br>
			<span style="padding:5px 20px 10px 20px;"><a href="<?php echo $_smarty_tpl->tpl_vars['link_terms']->value;?>
" id="seller_terms"><?php echo smartyTranslate(array('s'=>'Seller Terms & conditions(read)','mod'=>'agilemultipleseller'),$_smarty_tpl);?>
</a></span>
 	    	<?php }?>
	</p>
    <p>&nbsp;</p>

	<?php echo $_smarty_tpl->tpl_vars['seller_sign_up_fields']->value;?>

</fieldset>
 <script type="text/javascript">$('a.iframe').fancybox();</script>
<?php }} ?>