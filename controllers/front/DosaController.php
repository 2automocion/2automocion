<?php
class DosaControllerCore extends FrontController{
	
	
	public $php_self = 'dosa';
	
	
	/**
	 * Initialize search controller
	 * @see FrontController::init()
	 */
	public function init()
	{

		parent::init();

	}

	public function setMedia(){

		parent::setMedia();
		 
		$this->addCSS(_THEME_CSS_DIR_.'reciclinic.css');
		$this->addJs(_THEME_JS_DIR_.'GeoJSON.js');
		$this->addJS('http://maps.google.com/maps/api/js?sensor=false');
		
		//$default_country = new Country((int)Configuration::get('PS_COUNTRY_DEFAULT'));
		//$this->addJS('http://maps.google.com/maps/api/js?sensor=true&amp;region='.substr($default_country->iso_code, 0, 2));

	}


	/**
	 * Assign template vars related to page content
	 * @see FrontController::initContent()
	 */
	public function initContent()
	{
		parent::initContent();

		
		$this->setTemplate(_PS_THEME_DIR_.'materialclinic.tpl');
		
		/*
		if(isset($_POST['demandaTexto'])){

			$this->tratarCategorias();
			$this->setTemplate(_PS_THEME_DIR_.'salidaDemanda.tpl');
				
		}else{

			$this->tratarCategorias();
			$this->setTemplate(_PS_THEME_DIR_.'demanda.tpl');
				
		}
		*/
		
		
	}


}